## [6.2.4](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.2.3...v6.2.4) (2024-05-24)


### Bug Fixes

* fix comp failures ([25040a7](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/25040a780f01111be36d5f5ca8e51149ee098d4e))
* set defaults for restok lifetime and result size ([0d0dcfa](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/0d0dcfa35af4254b8339bcd8df06e892649db3e2))
* set restok lifetime to 300 secs for now ([a5e56c0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/a5e56c0e847aacc9554092628829693151310059))
* set restokLifetime as config value and use this everywhere ([299b192](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/299b192e3d8ff08b6f1184804b5dc82052b8c5bc))

## [6.2.3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.2.2...v6.2.3) (2024-05-23)


### Bug Fixes

* add new common version ([3bc5dba](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/3bc5dbab5c5e1e45f56005a4a3bcc448d741f1d3))
* fix some source issues with totalHits ([e423a07](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e423a0798c2416947d3edb00972180244bbf20de))
* fix test ([94ec286](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/94ec2867833560559eb5e6a4e588d5ff2b5a38ee))
* increase common to 5.0.0 release (es7) ([a643f5d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/a643f5dd62362d75b9d8b899f9bfe42773bd239c))
* increase mets mods mapping version ([c3c15ca](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c3c15ca7e835ca8fc7532cd42aa1d0620d5d35aa))
* remove dep to ltputils ([bcc455c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/bcc455ce84926ad008b9d158e63ea50795b4bac5))
* remove type from es client and configuration ([24a8ee0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/24a8ee01811d1017648eb112c057e31a1d26b6a0))
* remove unused type getters and setters ([bc3704a](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/bc3704a4ef6d65904f1595fd3da1337e925e1d12))
* upgrade to increased es version ([77ecf94](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/77ecf94001faa78d57d046100d7593fb9c1324aa))

## [6.2.2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.2.1...v6.2.2) (2023-03-14)


### Bug Fixes

* divide commons and tgclient versions ([ab9b401](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ab9b401c9abb3b09a8eac1343f4b6f0c4d43dd12))
* remove deb build and  debn deployment from pom files ([785496d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/785496df1074c0b44525ec04c7f6360f5aa16b4e))

## [6.2.1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.2.0...v6.2.1) (2023-03-14)


### Bug Fixes

* increase common version ([e2ba3e5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e2ba3e5c87a619cd1e53ef5288e489c688efa21c))

# [6.2.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.1.0...v6.2.0) (2023-03-13)


### Bug Fixes

* add new metsmods release version 2.4.1 ([cc82593](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/cc82593abf68ce20b3cfb7b423a6ba75515ae156))
* add sandbox (and edition) filter to tg listidentifier method ([0968cda](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/0968cda37043c8168157966cfb68ebf21cd03065))
* add sandbox filter to datacite metadata format, too ([4967d5c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/4967d5c90469b464fe1c22833c81e37eba61912c))
* update metsmods version to new snapshot ([3989111](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/398911140b9b37c1e4b3882d7fc1fe130ddb0055))


### Features

* add 1970-12-10 date format again ([7a31028](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/7a31028426ac33d8a7a665c48289bd5dab161c4d))

# [6.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v6.0.0...v6.1.0) (2023-03-07)


### Bug Fixes

* add new queries and metsmods modules ([98f4344](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/98f43444b91e04ad0d617c04aa4ee51da406026c))


### Features

* add new service method for updating idiom data to textgrid ([2f52b36](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/2f52b36b6758078971c63eb21bfc5a6eced8d52c))

# [6.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.10...v6.0.0) (2023-02-17)


### Bug Fixes

* add utc dates again to mods and mets header is current creation date now ([f1a2689](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f1a2689d4cfd8d42210a3c96b20a49f79f530a7c))
* fix mods record dates for ClaccisMayanMetsMods ([b2a4175](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b2a41755b826b3b5982d7af78edb53211e74ec2c))
* fix utc date tests ([d80f8ae](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d80f8ae2328f3029088abdbb5542c37b6c1a9ece))


### Features

* use new metsmods mudule v2.3.3 ([8dbae98](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/8dbae982928dd47f8951fb43b2cbf89bcdd297fc)), closes [#103](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/103)


### BREAKING CHANGES

* maybe ClassicMayanMetsMods and/or IdiomImage constructors must be adapted

## [5.1.10](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.9...v5.1.10) (2023-02-16)


### Bug Fixes

* add date time workaround for non-timezone dates in dhrep administrative metadata ([f34c351](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f34c351e1cf365c795fabe30007b6a0e20056e09))
* add localtime with server default time zone ([ab29402](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ab29402b99dbab92a0361b4474dccd9f458ae876))
* implement comments from mr ([6e96f8e](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/6e96f8e7390bebcfdd32d2e4779e61edda1401f5))

## [5.1.9](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.8...v5.1.9) (2023-02-10)


### Bug Fixes

* fix all the spec errors! at least TG API is COMPLIANT now ([dea5879](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/dea587977490cb1d3f86ae1d7b869813c4cac381))
* fix dates: UTC format for OAI things, ISO format for TG and DH  things ([ca97a8c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ca97a8c74eabbf79b1cbc2a791dbde732dcb2205))
* fix false tests ([c93d5c6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c93d5c6cd491ee1fe23a01d625cdf1fbef051c1c))
* fix OAI error with non existent works mentioned in isEditionOf fields ([1063209](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/10632093ad88db24fb7641008d6a5b818bb686b7))
* increase metsmods module version ([a150a4a](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/a150a4af5e97cd2c5c1cfbb7ad3e30c3c8d04584))
* increase metsmods module version ([a298268](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/a29826852688334cd8c4d0ba4395818d7a6e570a))
* increase metsmods module version, set to next release ([77538b8](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/77538b8a42d7200b928f3ab11af697b9f1b9477a))
* refactor some of the calendar methods, add tests ([e54a468](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e54a468e7d4977e14d56c077d43ea5e9281ff9b4))
* remove sandbox items from dc record list ([173cbca](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/173cbca5973d7bc2081e1db7ed92ff022f08e925))

## [5.1.8](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.7...v5.1.8) (2023-01-27)


### Bug Fixes

* increase maven-jaxb-plugin version ([e9156b3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e9156b3b24728488fd6279672bbf5b0e66cc4f93))
* increase mets mods lib version ([1675575](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/16755751977806aa7362244aab3969be8fe3e5c4))
* increase metsmods version ([68efad8](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/68efad868e6d64bd8264de663afc613129a2e0fa))
* remove openaire namespace and location, we are using datacite here ([c143076](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c1430766aa1c4cc7e5cd54ef52e2ba45be8be5ba))
* remove unused schema files ([b76cbb6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b76cbb633e30a0497186294e1e3ae92e4c2b3118))

## [5.1.7](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.6...v5.1.7) (2023-01-18)


### Bug Fixes

* include new mets mods mapping version 2.1.2 ([f3039ed](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f3039ed4da88aff7e14d3109126289ce1f8211c6))
* increase mets mods module version ([42afcbb](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/42afcbb27267447f9ac1ee4cdb71c2bfcc3050bd))

## [5.1.6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.5...v5.1.6) (2023-01-16)


### Bug Fixes

* add configurability for classicmayan element selection ([b0cc701](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b0cc701adf1c3f20f9d49404fd57d20171b68609))
* add more and correct logging ([588a3b2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/588a3b29e1500f9b8e521883f877bdb725eafcfe))
* add more error handling to oaipmhimpl ([c539a3e](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c539a3e2e72e1cf0856dd397eb2acb14fd8f8b4a))
* add tests for textgrid no edition queries ([c37e26f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c37e26f0ee4474f3eaac6994c1c7e496f8c88685))
* check and remove/change FIXME comments to TODO ([d4f3250](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d4f3250721dc8906d86baff63c406c7bb751e37c))
* fix double project id prefix ([e3af362](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e3af3627bbc48350ff029c2aa62efa242bcf70dc))
* fix misconfiguration in beans file ([1f48af4](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/1f48af4febb72f8fca66c7d816c7fb6d483f3e01))
* gregorian calendars refactored, fixes datestamp in oaipmh headers ([3e15d61](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/3e15d6128a08f8ab08c6cab42da089bf01861281))
* refactor some timestamps in test classes, too ([61533c3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/61533c389332fcbeb490eb3ba27e562599697f4c))
* rep identification set completely by config file now ([4f3fca2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/4f3fca28337b464e743623f7716cfce6ce28d3ce))

## [5.1.5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.4...v5.1.5) (2023-01-06)


### Bug Fixes

* really fix npe now! ([87c0e18](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/87c0e181b4b34a16f90cae5b727a1ba200fb1829))

## [5.1.4](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.3...v5.1.4) (2023-01-05)


### Bug Fixes

* fix npe in oaipmh datacite list records ([47eb43c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/47eb43cddad06f4457cf041546f651b357234fd6))

## [5.1.3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.2...v5.1.3) (2023-01-05)


### Bug Fixes

* add geolocation for dariah openaire ([b0ec128](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b0ec128b9125b50be0147d48e71d814b4aec5293))
* add MA openaire rights tag ([22feac7](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/22feac77d60f33aafd813d684f9d66ac15ba902f))
* add reducing singleField again.... had to understand it first ([9a9257c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/9a9257c0b0cd9af949f97ef599707acdd97a58b5))
* add title again to fieldLoader ([e39331e](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e39331e1d14e38099df9d41529de7109ed2c2b97))
* fix and refactor some general openaire issues for textgrid ([601ef94](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/601ef948b80ac63fb668e4fa904e14f81c611e21))
* fix npe and add more name identifier scheme uris ([92f4544](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/92f4544c0f8c6b66033349f4584393f20678a71f))
* fix Property Contributor & contributorName ([8981148](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/8981148a8a6b41985c8296b0e951e30cbd9e510a))
* refactor some strings ([06adbe1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/06adbe1d2ca4bcac13262cbb8f1c913bb23c9c82))
* remove empty subject tags in openaire response ([e0fa01f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e0fa01f7375ff1c00552d00ccddfb9f9df979c8f))

## [5.1.2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.1...v5.1.2) (2022-12-20)


### Bug Fixes

* increase metsmods version ([9a33b66](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/9a33b666a75fef88219650661b7971ec776db24a))

## [5.1.1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.1.0...v5.1.1) (2022-12-07)


### Bug Fixes

* add json error to logging ([765112a](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/765112a1b710ef6a38c47af54c0cfc730f641471))

# [5.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.0.3...v5.1.0) (2022-12-07)


### Bug Fixes

* increase mets-mods-mapping version ([6b89614](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/6b896146eff723249f1298fcc65907304098aab1))
* remove idiomimage from oaipmh impl ([4856a68](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/4856a68eb5c25be293463a528777fbb93824c112))


### Features

* increase metsmodsmapping version, add README for JUNIT testing ([9285238](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/9285238d1153b62034f71875d1a532380cd5d656))

## [5.0.3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.0.2...v5.0.3) (2022-10-10)


### Bug Fixes

* correct camel case for OAIPMH and DATACITE classes :-) ([d50892e](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d50892e1c1bda060e72d30810e6352d6f547e87b))
* correct header date information for oai_idiom_mets ([414e7e5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/414e7e55c953de6187dc317299d364a533902bc0))

## [5.0.2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.0.1...v5.0.2) (2022-10-06)


### Bug Fixes

* get ListIdentifiers for IDIOM from ElasticSearch (fixes [#77](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/77)) ([7dac11f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/7dac11fda45e5ba5de479db4ea5d1fe5606d5f6b))
* re-ignore testing (have to improve this eventually... :-) ([28f6b63](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/28f6b63f722f5b4ab8a53bb0b79fc20c2faf8fad))
* refactor method for ES request (fixes [#80](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/80)) ([eb874ac](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/eb874ac38551b740b2c518eede16a2ab2deca053))
* repair IDIOM image issues (fixes [#74](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/74) and fixes [#79](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/79)) ([ad908e6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ad908e6828b15465592f77a5f826ba233dad3239))

## [5.0.1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v5.0.0...v5.0.1) (2022-10-04)


### Bug Fixes

* repair revision URI deliverance with IDIOM objects ([1c933c5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/1c933c531024c06a10c463a30dd7b7463fa9262a)), closes [#73](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/73)

# [5.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.11...v5.0.0) (2022-09-28)


### Bug Fixes

* add endpoint for tgcrud for correct mets-mods usage ([8272f62](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/8272f62a088b52b77ee106c637fb06f947210e39))
* add singleton tgcrud client ([2a1d842](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/2a1d842417bb46eb7ff67cf9ddf998d3dfc835d1))
* add TEST_ALL_PAGES trigger for online testing ([01c4975](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/01c4975ec470cfa42dde73969c4f8bcd256035f5))
* correct oaiEsClient name ([d772dec](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d772dec85f7ffee7b52f03517512abecd3b0241c))
* fix beans es client instantiation ([71a9573](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/71a957372a26c78d88fb6c6cf2faa0aba64386a2))
* fix dhrep online tests and config ([f97801f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f97801fabadce6006b86304cb0dfb6a17ca486a4))
* fix id counting method ([2c2b144](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/2c2b144da7f8d811a5858e4d1ca67b6c0621489e))
* ignore tests again ([d23e99d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d23e99d72749c4b90ad94e2b1122ef31e7e58337))
* refactor beans.xml and a little more in IDIOM ListRecords ([62aad73](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/62aad7349929523e1d04f1a58319fd62f2c2dfe4))
* refactor dhrep tests a little bit, correct test settings ([983f443](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/983f443d74ee9c5b06a4ed1f223c1917751a0964))
* refactor oaiEsClient handling, we have a new client instance now for every class ([9693691](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/9693691f4c8ff6d29936f27861eae8ca57628cab))
* refactor some spring configurations and IDIOM image settings ([d0541d9](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d0541d9d67bb813adbe7e5721fd646d12a39aef4))
* refactor tests a lot :-) ([d8997f3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/d8997f3be99fcedd44811558e284ea4e70eeb729))
* refactor textgrid URI usage (URI <--> base URI) ([e2834dd](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e2834dd2a07125f5789fa1c386b1a5ab58976162))
* refactore oaipmh client handling ([0e96ef6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/0e96ef6045c41e5d30799b15013869d4889dd303))
* remove more idiom image metadata format things ([f73bf6f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f73bf6fa50975917103e9122ec98ef5225c1cdb3))
* removed all oai_idiom_image_mets occurances, fixes [#56](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/56) ([80fde0b](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/80fde0bff57119fd0357da72391176f2d3d703ec))
* set tests right ([1e77386](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/1e773861d6690879cf787b6a4a199856ecf0dd47))
* update spring dependency ([bb52b05](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/bb52b056405056371c5fc6eb34e2ae5fc4a66479))


### Features

* re-factor idiom image handling, use new idiom metsmods lib 2.0.0 ([689ca1d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/689ca1d47acd72d88ee0cd38bd45cf48d5c31645))
* refactor ElasticSearchBean a little bit --> use a second for nonpublic queries ([7fdec8b](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/7fdec8b05567bd0967db65a5a999388ee8b9e212))


### BREAKING CHANGES

* OAI-PMH configuration needs to be updated

## [4.4.11](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.10...v4.4.11) (2022-09-01)


### Bug Fixes

* add more and finer logs ([de828a0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/de828a0147a27ea10a169cd9d2070a0d2c92082d))
* remove log4jcontextlistener ([10beddf](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/10beddf5a737f88e6aa414ed8f2a030a48da4d62))
* remove some dead code, update Spring deps ([dc25e34](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/dc25e34d76d59f39cd24ba2e543b78ed11631482))

## [4.4.10](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.9...v4.4.10) (2022-08-30)


### Bug Fixes

* if image incomplete, add normal object data ([6e74754](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/6e74754420b2b0ea8aef56d04390ec6b83b81254))
* jump to next entry if no record available (still [#64](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/64)) ([bfea775](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/bfea7758ddfac18a4f1cba86dadf750b3a1e851e))

## [4.4.9](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.8...v4.4.9) (2022-08-30)


### Bug Fixes

* fix failure in gitlab-ci ([ff80672](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ff806729d5ac1a47db043a7c2f7f65732ac39925))
* fixes [#65](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/65) for GetRecord, too ([961c0c3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/961c0c313f3008019ed4a98f05631401f6ef5078))

## [4.4.8](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.7...v4.4.8) (2022-08-29)


### Bug Fixes

* catching NoSuchElementException in metsmodsmapping module ([f47c283](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/f47c2834a4bae52d5d58be0bddb1a3a0bf463cb8))

## [4.4.7](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.6...v4.4.7) (2022-08-29)


### Bug Fixes

* add more logging ([c446fe0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/c446fe0f04e42068452baea2c43de975cf10166f))

## [4.4.6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.5...v4.4.6) (2022-08-29)


### Bug Fixes

* change some identifier assignments ([574ed1a](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/574ed1aff30cd8f9b415ea061cf4e2ffda364b18))
* remove sysout, add FINE logging ([78ec0dd](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/78ec0dd589537430ab17fc3c8c91204f39fdb603))

## [4.4.5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.4...v4.4.5) (2022-08-19)


### Bug Fixes

* add new classicmayan mets-mods-mapping release ([6479754](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/64797545dd3775f2c2fcadb9854b60fe5f1e4eec))
* fix https link in simpledc schema ([0b52c44](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/0b52c4470af6d1756e0a8119d88257d88b418352))
* increase mets-mods-mapping version ([b7a362f](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b7a362f309f793fde9e6566e506d2996510ca1ee))

## [4.4.4](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.3...v4.4.4) (2022-08-18)


### Bug Fixes

* looking for NPE ([35fe2e1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/35fe2e1b98c683dba866260f220710a28ae2dd4d))
* still looking for NPE... :-D ([854ded6](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/854ded619af3bc74be6b4c780825ef55a1d93c1c))
* store latest changes ([25a43df](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/25a43df0b63ccd666126ac25c3f6d6d62b8a2ce4))

## [4.4.3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.2...v4.4.3) (2022-08-17)


### Bug Fixes

* add more logging ([7d0f637](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/7d0f637e8591bbe6313facea9c12612f7d59a4e4))

## [4.4.2](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.1...v4.4.2) (2022-08-17)


### Bug Fixes

* add bom creation in mvn profile sbom ([16e8f63](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/16e8f63110742377284fccbc56bcc700657ba239))
* add method doc, fix some final issues ([5cee54e](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/5cee54ef1c9f387a5d576e5f587bb79e9432fbbf))
* add more ignores ([03ca80b](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/03ca80bd52424283a4694b1f1929ce18a2a76cb7))
* add some logging ([b2a5e69](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b2a5e691a99421157527ac05887f70c98daf3ded))
* remove sysouts, do some formatting ([93e0da1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/93e0da1809e5f335bdb08d02bfd047137d662064))

## [4.4.1](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.4.0...v4.4.1) (2022-07-19)


### Bug Fixes

* add "hdl" instead of "hdl:" from config file ([884eb83](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/884eb830cd67ccd97310a56938a5b465dcc5e405))
* add boost for collection in set response ([5f321fd](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/5f321fd7a46d23e0cdb27964eb43814edae3c7bd))
* add contrib file ([11ad08d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/11ad08d1a6f575064b7249d1c462498cf2a81a8e))
* add EVEN MORE logging ([b05afae](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/b05afaed4809aa6115aea41e4c6075d2e7f38670))
* add more logging ([9a282b3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/9a282b3146ddefbc863d2801043e6b9f719f9fe8))
* arg! ([3e1e3f8](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/3e1e3f8d3594a6b037988b47f5713dfab87f13c5))
* arg! ([24e378b](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/24e378bc9ffdbc811fdb06e501954ae38b60ec1a))
* fix listsets bug, using admimistrativeMetadata.dcterms:format now ([ce57635](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ce57635a02762210de8357da668e32df4b04b57c))
* more list sets logging ([47ed8d3](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/47ed8d33a25c0245a149febb2043b33bb05abac2))
* remove log4j deps, using util logging for now ([e2ae91c](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/e2ae91c81dfa3f2a7e8b48813bf55478296afcf5))
* remove out commented ignore for testing ([fead011](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/fead011b598c6376bc5dd08824edf9cdc9eb0a7e))
* try more logging ([ee439c5](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/ee439c5457320b2948877d9a2192b3b1b46f0a3f))
* use administrativeMetadata for ListSets request ([551f771](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/551f771b0f19fc979598e75289fc9fc146dcf74e))
* use more listsets getters and setters ([2156f34](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/2156f34304d3424dec3aeaece0a185bd7db7e7e4))
* yes, fix bug [#30](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/issues/30), set included in listidentifiers, too ([18f0270](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/18f027072145c87a7a50622a10b3c6ce9b645fb3))

# [4.4.0](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/compare/v4.3.0...v4.4.0) (2022-07-06)


### Bug Fixes

* increase SNAPSHOT version ([a4bf840](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/a4bf8401bff21c032caa40a1fa084def4adbdbaf))


### Features

* remove set-in-collection to fix bug ([7578c7d](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/commit/7578c7dcfb9fa4194e512e49a785b4bd39cbe228))

# 4.2.10-SNAPSHOT

* Implement new CI workflow

# 4.2.8

* Fix #61

# 4.0.15-SNAPSHOT

* Move repository, issues, and ci/cd from projects.gwdg.de to gitlab.gwdg.de
* Implement Gitlab CI/CD workflow

.. oai-pmh documentation master file, created by
   sphinx-quickstart on Thu May 21 11:06:44 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


DH-oaipmh Service
=================

The DARIAH-DE OAI-PMH Service is the service to harvest all metadata from the collections stored in the DARIAH-DE Repository. So the `Repository Search <https://repository.de.dariah.eu/search>`_ and the `DARIAH-DE Generic Search <https://search.de.dariah.eu>`_ can index all the data that is entered into the `Repository Collection Registry <https://repository.de.dariah.eu/colreg-ui>`_ or the `DARIAH-DE Collection Registry <https://colreg.de.dariah.eu>`_.

All objects stored in the DARIAH-DE Repository are indexed within and are delivered by the DH-oaiopmh Service.


API Documentation
-----------------

DARIAH-DE Repository OAI-PMH base URL: https://repository.de.dariah.eu/1.0/oaipmh/oai

All requests are implemented to be consistent with the OAI-PMH `The Open Archives Initiative Protocol for Metadata Harvesting <https://www.openarchives.org/OAI/openarchivesprotocol.html>`_

Two metadata formats are provided for the DARIAH-DE Repository: The mandatory DC metadata format (*oai_dc*) and the enhanced DataCite format according to `OpenAIRE Guidelines for Data Archives <https://guidelines.openaire.eu/en/latest/data/index.html>`_ (*oai_datacite*).


Query Examples
--------------

Get the OAI-PMH service's version: https://repository.de.dariah.eu/1.0/oaipmh/oai/version

Identify
""""""""

* Get basic information: https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=Identify

ListMetadataFormats
"""""""""""""""""""

* Get provided metadata formats: https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListMetadataFormats

ListSets
""""""""

* List all DARIAH-DE Repository collections as OAI-POMH sets: https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListSets

ListRecords
"""""""""""

* Get all records (*oai_dc*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListRecords&metadataPrefix=oai_dc
* Get all records (*oai_datacite*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListRecords&metadataPrefix=oai_datacite
* Get all records of a set (*oai_dc*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListRecords&set=hdl:21.11113/0000-000B-C8F2-2&metadataPrefix=oai_dc

ListIdentifiers
"""""""""""""""

* Get all record identifiers (*oai_dc*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_dc
* Get all record identifiers (*oai_datacite*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_datacite
* Get all record identifiers of a set (*oai_datacite*): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=ListIdentifiers&set=hdl:21.11113/0000-000B-C8F2-2&metadataPrefix=oai_dc

GetRecord
"""""""""

* Get a single record (*oai_dc): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=GetRecord&identifier=hdl:21.11113/0000-000B-C9ED-8&metadataPrefix=oai_dc
* Get a single Edition record (*oai_datacite): https://repository.de.dariah.eu/1.0/oaipmh/oai?verb=GetRecord&identifier=hdl:21.11113/0000-000B-C9ED-8&metadataPrefix=oai_datacite


Sources
-------
See oaipmh_sources_


Bugtracking
-----------
See oaipmh_bugtracking_


License
-------
See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/-/blob/main/LICENSE.txt
.. _oaipmh_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/
.. _oaipmh_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/-/issues

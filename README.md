# The DARIAH-DE OAI-PMH Service

## Overview

The OAI-PMH Data Provider Service is used for both TextGrid and the DARIAH-DE Repository, one instance for every installation.

## Technical Information

**TODO**

## Documentation

For service and API documentation please have a look at the [TG-oaipmh Documentation](https://textgridlab.org/doc/services/submodules/oai-pmh/docs_tgrep/index.html), and the [DH-oaipmh Documentation](https://repository.de.dariah.eu/doc/services/submodules/oai-pmh/docs_dhrep/index.html).

## Installation

The service is provided with a Gitlab CI workflow to build .DEB files to be deployed and installed on TextGrid and DARIAH-DE servers.

### Building from GIT using Maven

You can check out the PID Service from our [GIT Repository](https://gitlab.gwdg.de/dariah-de/dariah-de-oaipmh-services), develop or main branch, or get yourself a certain tag, and then use Maven to build the PID Service WAR file:

    git clone https://gitlab.gwdg.de/dariah-de/dariah-de-oaipmh-services.git

... build the WAR packages via:

    mvn clean package

... build the DEB packagea using:

    mvn clean package -Pdhrep.deb

You will get a PID Service WAR and/or DEB file in the folders ./oaipmh-webapp/target.

### Using DEB File

Or you can use a DEB file from the DARIAH-DE [APTLY Repository](https://ci.de.dariah.eu/packages/)


## Deploying the OAI-PMH Service

The service is deployed just by installing the appropriate DEB package.


## Releasing a new version

For releasing a new version of DARIAH-DE OAI-PMH, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).


## JUNIT Online Testing

* Choose one of the existing properties files from */oaipmh-core/src/test/resources* to test TextGrid or DARIAH-DE prod, test, or dev OAI-PMH service.
* Put the filename and path in class  */oaipmh-core/src/test/java/info/textgrid/middleware/test/online/OaipmhUtilitiesOnline.java* or */oaipmh-core/src/test/java/info/textgrid/middleware/test/online/dh/TestDHOaipmhOnline.java*.
* **Un-ignore** all the JUNIT test classes in all the test class header in *info.textgrid.middleware.test.online.dh* or *info.textgrid.middleware.test.online.tg*.
* Store the files and run the tests via Eclipse, or mvn command line.

## Links and References

* [OAI-PMH Service Gitlab page](https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services)

.. oai-pmh documentation master file, created by
   sphinx-quickstart on Thu May 21 11:06:44 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TextGrid OAI-PMH
================

The TextGrid OAI-PMH Service is the service to harvest all metadata from the editions stored in the TextGrid Repository (public editions only). For example the `DARIAH-DE Generic Search <https://search.de.dariah.eu>`_ can index all the data that is entered into the `DARIAH-DE Collection Registry <https://colreg.de.dariah.eu>`_.

The TextGrid OAI-PMH Service is delivering all public projects as OAI-PMH sets (no objects from the sandbox at the moment), and all Edition objects within the projects. You can get to the project's data by using the TextGrid Search service, please see `TG-search <https://textgridlab.org/doc/services/submodules/tg-search/docs/index.html>`_, or the `TextGrid Aggregator <https://textgridlab.org/doc/services/submodules/aggregator/docs/index.html>`_.


API Documentation
-----------------

TextGrid Repository OAI-PMH base URL: https://textgridlab.org/1.0/tgoaipmh/oai

All requests are implemented to be consistent with the OAI-PMH `The Open Archives Initiative Protocol for Metadata Harvesting <https://www.openarchives.org/OAI/openarchivesprotocol.html>`_

Two metadata formats are provided for the TextGrid Repository: The mandatory DC metadata format (*oai_dc*) and the enhanced DataCite format according to `OpenAIRE Guidelines for Data Archives <https://guidelines.openaire.eu/en/latest/data/index.html>`_ (*oai_datacite*).


Query Examples
--------------

Get the OAI-PMH service's version: https://textgridlab.org/1.0/tgoaipmh/oai/version

Identify
""""""""

* Get basic information: https://textgridlab.org/1.0/tgoaipmh/oai?verb=Identify

ListMetadataFormats
"""""""""""""""""""
* Get provided metadata formats: https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListMetadataFormats

ListSets
""""""""
* List all public TextGrid projects as OAI-POMH sets: https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListSets

ListRecords
"""""""""""

* Get all Edition records (*oai_dc*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&metadataPrefix=oai_dc
* Get all Edition records (*oai_datacite*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&metadataPrefix=oai_datacite
* Get all Edition records of a set (*oai_dc*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListRecords&set=project:TGPR-8b44ca41-6fa1-9b49-67b7-6374d97e29eb&metadataPrefix=oai_dc

ListIdentifiers
"""""""""""""""

* Get all Edition records (*oai_dc*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_dc
* Get all Edition record identifiers (*oai_datacite*):https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListIdentifiers&metadataPrefix=oai_datacite
* Get all Edition record identifiers of a set (*oai_datacite*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=ListIdentifiers&set=project:TGPR-8b44ca41-6fa1-9b49-67b7-6374d97e29eb&metadataPrefix=oai_dc

GetRecord
"""""""""

* Get a single Edition record (*oai_dc*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=GetRecord&identifier=textgrid:40wr9.0&metadataPrefix=oai_dc
* Get a single Edition record (*oai_datacite*): https://textgridlab.org/1.0/tgoaipmh/oai?verb=GetRecord&identifier=textgrid:40wr9.0&metadataPrefix=oai_datacite


Sources
-------
See oaipmh_sources_


Bugtracking
-----------
See oaipmh_bugtracking_


License
-------
See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/-/blob/main/LICENSE.txt
.. _oaipmh_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/
.. _oaipmh_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-oai-pmh-services/-/issues

package info.textgrid.middleware.test.online;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.http.HttpStatus;
import info.textgrid.middleware.OaipmhConstants;
import info.textgrid.middleware.OaipmhProducer;
import info.textgrid.middleware.OaipmhUtilities;
import info.textgrid.middleware.test.online.dh.TestDHOaipmhOnline;

/**
 * <p>
 * Some online tests for the TextGrid OAIMPH service.
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES <b>BELOW</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
public class OaipmhUtilitiesOnline {

  // **
  // FINALS
  // **

  // ## CHANGE SETTINGS BELOW FOR SETTING TEST SCOPE ---------------------------------------------

  public static final String PROPERTIES_FILE = "oaipmh.test.textgridlab-org.properties";
  // public static final String PROPERTIES_FILE = "oaipmh.test.dev-textgridlab-org.properties";
  // public static final String PROPERTIES_FILE = "oaipmh.test.test-textgridlab-org.properties";

  public static final boolean TEST_ALL_PAGES = false;
  // public static final boolean TEST_ALL_PAGES = true;

  // ## CHANGE SETTINGS ABOVE FOR SETTING TEST SCOPE ---------------------------------------------

  public static final String OAI_DC_PREFIX = "oai_dc";
  public static final String OAI_DATACITE_PREFIX = "oai_datacite";
  public static final String OAI_IDIOMMETS_PREFIX = "oai_idiom_mets";

  public static final String VERB_LIST_IDENTIFIERS = "ListIdentifiers";
  public static final String VERB_LIST_RECORDS = "ListRecords";
  public static final String VERB_GET_RECORD = "GetRecord";
  public static final String VERB_IDENTIFY = "Identify";
  public static final String VERB_LIST_METADATA_FORMATS = "ListMetadataFormats";
  public static final String VERB_LIST_SETS = "ListSets";

  public static final String HEADER_ID_START_TAG = "<identifier>";
  public static final String HEADER_ID_END_TAG = "</identifier>";

  public static final String EXPECTED_OAIDC_FORMAT_CONTENT = "<oai_dc:dc>";
  public static final String EXPECTED_IDIOMMETS_FORMAT_CONTENT = "<mets ";
  public static final String EXPECTED_DATACITE_FORMAT_CONTENT = "<datacite:resource>";

  public static final String OK = ">>> OKIDOKI";
  public static final String TESTING = "\n >>> TESTING ";
  public static final String NOT_TESTED = " >>> NOT_TESTED";
  public static final String NO_TOKEN = "-1";

  public static final boolean NO_METADATA_FORMAT_WITH_RESTOK = false;
  public static final boolean METADATA_FORMAT_WITH_RESTOK = true;
  public static final boolean NO_ERROR_EXPECTED = false;
  public static final boolean ERROR_EXPECTED = true;
  public static final String NO_SET = null;
  public static final String NO_THREAD_NAME = "MAIN";
  public static final String NO_FROM = null;
  public static final String NO_UNTIL = null;
  public static final String NO_QUERY = "";

  // Time to wait between the single queries using resumption tokens in milliseconds.
  public static final Long TIME = 275l;

  public static final String HDL_PREFIX = "hdl:";
  public static final String STAR_PREFIX = "***";
  public static final String OAI_PATH = "oai";
  public static final String VERSION_PATH = "oai/version";

  public static final long OAIPMH_CLIENT_TIMEOUT = 120000;

  /**
   * @param theClient
   * @param theQuery
   * @return
   * @throws IOException
   */
  public static Response getOAIHttpResponse(Client theClient, String theQuery)
      throws IOException {
    return getHttpResponse(theClient, "[" + NO_THREAD_NAME + "] ", OAI_PATH, theQuery);
  }

  /**
   * @param theClient
   * @param theThreadName
   * @param theQuery
   * @return
   * @throws IOException
   */
  public static Response getOAIHttpResponse(Client theClient, String theThreadName,
      String theQuery) throws IOException {
    return getHttpResponse(theClient, theThreadName, OAI_PATH, theQuery);
  }

  /**
   * @param theClient
   * @return
   * @throws IOException
   */
  public static Response getVersionHttpResponse(Client theClient) throws IOException {
    return getHttpResponse(theClient, NO_THREAD_NAME, VERSION_PATH, NO_QUERY);
  }

  /**
   * @param theClient
   * @param theThreadName
   * @param thePath
   * @param theQuery
   * @return
   * @throws IOException
   */
  protected static Response getHttpResponse(Client theClient, String theThreadName, String thePath,
      String theQuery) throws IOException {

    Response result;

    System.out.println("\t" + theThreadName + "waiting: " + TIME + " millies");

    try {
      Thread.sleep(TIME);
    } catch (InterruptedException e) {
      // Nothing interrupts here!
    }

    WebClient w = WebClient.fromClient(theClient).path(thePath).replaceQuery(theQuery);

    System.out.println("\t" + theThreadName + "HTTP GET " + w.getCurrentURI());

    result = w.get();

    // Check HTTP status.
    int status = result.getStatus();

    if (status != HttpStatus.SC_OK) {
      String responseString = IOUtils.readStringFromStream((InputStream) result.getEntity());
      System.out.println("RESPONSE: " + responseString);
      String message = theThreadName + "status: " + status;
      assertTrue(message, false);
    } else {
      System.out.println("\t" + theThreadName + "status: " + status);
    }

    return result;
  }

  /**
   * @param theMillis
   * @return
   */
  public static String getDurationInSecs(long theMillis) {

    int SECS_IN_MILLIS = 1000;
    int secs = SECS_IN_MILLIS;

    return theMillis / secs + " second" + ((theMillis / secs) != 1 ? "s" : "");
  }

  /**
   * <p>
   * Loads a resource.
   * </p>
   * 
   * TODO Put together with the method in TGCrudServiceUtilities! Maybe create a first build maven
   * module for utility things.
   * 
   * @param {@link String} The resource to search for.
   * @return {@link File} The resource.
   * @throws IOException
   */
  public static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

  /**
   * @param theProperty
   * @return
   */
  public static List<String> getListFromProperties(String theProperty) {

    List<String> result = new ArrayList<String>();

    String parts[] = theProperty.split(",");
    for (String part : parts) {
      result.add(part.trim());
    }

    return result;
  }

  /**
   * @return
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static Properties getPropertiesFromFile(String theConfigFile)
      throws FileNotFoundException, IOException {

    Properties result = null;

    // Load properties file.
    result = new Properties();
    result.load(new FileInputStream(getResource(theConfigFile)));

    System.out.println("Properties file: " + theConfigFile);
    result.list(System.out);

    return result;
  }

  /**
   * @param theEndpoint
   * @return
   */
  public static Client getOAIPMHWEebClient(String theEndpoint) {

    Client result = null;

    // Get OAI-PMH REST endpoint and HTTP client.
    System.out.println("Getting OAI-PMH HTTP client --> " + theEndpoint + " <--");

    // Get proxy first, set policy.
    OaipmhProducer JAXRSClient = JAXRSClientFactory.create(theEndpoint, OaipmhProducer.class);
    HTTPConduit conduit = WebClient.getConfig(JAXRSClient).getHttpConduit();
    HTTPClientPolicy policy = new HTTPClientPolicy();
    policy.setReceiveTimeout(OaipmhUtilitiesOnline.OAIPMH_CLIENT_TIMEOUT);
    conduit.setClient(policy);

    // Create Web Client from Web Proxy.
    result = WebClient.client(JAXRSClient);

    return result;
  }

  // **
  // TG used methods
  // **

  /**
   * @param theClient
   * @param theVerb
   * @param theSet
   * @param theMetadataPrefix
   * @param maxNumberOfPagesToTest
   * @param recordsExpectedPerRequest
   * @param theThreadName
   * @param from
   * @param until
   * @param resumptionTokenANDMetadataPrefix
   * @throws IOException
   */
  public static void examineTGList(Client theClient, final String theVerb, final String theSet,
      final String theMetadataPrefix, final int maxNumberOfPagesToTest,
      final int recordsExpectedPerRequest, final String theThreadName, final String from,
      final String until, final boolean resumptionTokenANDMetadataPrefix,
      final boolean errorExpected) throws IOException {

    String threadName = tn(theThreadName);

    long startTime = System.currentTimeMillis();

    String testOccurance = "header";
    if (theVerb.equals(VERB_LIST_RECORDS)) {
      testOccurance = "record";
    }

    String query = "verb=" + theVerb + "&metadataPrefix=" + theMetadataPrefix;

    if (theSet != null) {
      query += "&set=" + theSet;
    }

    if (from != null && until != null) {
      query += "&from=" + from + "&until=" + until;
    }

    Response httpResponse = getOAIHttpResponse(theClient, threadName, query);
    int status = httpResponse.getStatus();

    int loopCount = 1;
    long timeRunning = System.currentTimeMillis() - startTime;
    System.out.println("\t" + threadName + "time: " + getDurationInSecs(timeRunning) + " (loop "
        + loopCount + (maxNumberOfPagesToTest != 0 ? "/" + maxNumberOfPagesToTest : "") + ")");

    String responseString = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    // Test resumption token tags.
    String restok = examineTGResumptionTokenTag(responseString, testOccurance, NO_TOKEN,
        recordsExpectedPerRequest, loopCount, threadName);

    // Test if the header identifiers are sound.
    if (theVerb.equals(VERB_LIST_RECORDS) || theVerb.equals(VERB_LIST_IDENTIFIERS)) {
      examineTGHeaderIDs(responseString, theMetadataPrefix, theThreadName);
    }

    // Test if the header dates and record (modification) dates are equal.
    if (theVerb.equals(VERB_LIST_RECORDS)
        && theMetadataPrefix.equals(OaipmhConstants.METADATA_IDIOM_PREFIX)) {
      try {
        examineTGModificationDates(responseString);
      } catch (ParseException | DatatypeConfigurationException e) {
        System.out.println(responseString);
        throw new IOException(e);
      }
    }

    // Test general metadata content (if verb is listRecords!), must go conform with the metadata
    // prefix setting.
    if (theVerb.equals(VERB_LIST_RECORDS)) {
      examineTGContent(responseString, theMetadataPrefix);
    }

    while (status == HttpStatus.SC_OK && !restok.equals(NO_TOKEN)) {
      loopCount += 1;
      synchronized (threadName) {
        if (maxNumberOfPagesToTest > 0 && loopCount > maxNumberOfPagesToTest) {
          System.out.println(
              "\t" + threadName + ">>> TESTING ONLY " + maxNumberOfPagesToTest + " pages!");
          break;
        }
        query = "verb=" + theVerb + "&resumptionToken=" + restok;
        if (resumptionTokenANDMetadataPrefix) {
          query += "&metadataPrefix=" + theMetadataPrefix;
        }
        httpResponse = getOAIHttpResponse(theClient, threadName, query);
        timeRunning = System.currentTimeMillis() - startTime;
        System.out.println("\t" + threadName + "time: " + getDurationInSecs(timeRunning) + " (loop "
            + loopCount + (maxNumberOfPagesToTest != 0 ? "/" + maxNumberOfPagesToTest : "") + ")");

        // Test resumption token tags.
        responseString = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());
        restok = examineTGResumptionTokenTag(responseString, testOccurance, restok,
            recordsExpectedPerRequest, loopCount, threadName);

        // Test if the header identifiers are sound.
        if (theVerb.equals(VERB_LIST_RECORDS)) {
          examineTGHeaderIDs(responseString, theMetadataPrefix, theThreadName);
        }

        // Test general metadata content (if verb is listRecords!), must go conform with the
        // metadata prefix setting.
        if (theVerb.equals(VERB_LIST_RECORDS)) {
          examineTGContent(responseString, theMetadataPrefix);
        }
      }
    }

    // Check for errors in response.
    if (responseString.contains("error code")) {
      String message = "ERROR in response!";
      System.out.println(responseString);
      if (!errorExpected) {
        assertTrue(message, false);
      }
    }

    // Only check for max loops, if we do have less loops and no resumption token, it will be fine,
    // too!
    if (!restok.equals(NO_TOKEN) && loopCount < maxNumberOfPagesToTest) {
      String message = threadName + ": Must have done " + maxNumberOfPagesToTest
          + " loops, but did only " + loopCount;
      System.out.println(responseString);
      assertTrue(message, false);
    }

    // Check for single loop, we need MORE!
    if (loopCount == 1 && recordsExpectedPerRequest > 0) {
      System.out.println(responseString);
      String message = threadName + ": Must have more than one loop!";
      assertTrue(message, false);
    }

    System.out.println("\t" + threadName + OK);
  }

  /**
   * @param theResponse
   * @param recordOrHeader
   * @param oldtok
   * @param recordsExpectedPerRequest
   * @param loopCount
   * @param theThreadName
   * @return Resumption token string, if existing, -1 if either no <resumptionToken> tag is
   *         existing, tag is existing and has no token value.
   * @throws IOException
   */
  private static String examineTGResumptionTokenTag(final String theResponseString,
      final String recordOrHeader, final String oldtok, final int recordsExpectedPerRequest,
      final int loopCount, final String theThreadName) throws IOException {

    // Test for OAIPMH errors.
    if (theResponseString.contains("<error code=\"badArgument\">")) {
      String message = theThreadName + ": ERROR IN OAIPMH RESPONSE: " + theResponseString;
      System.out.println(message);
      throw new IOException(message);
    }

    // Count response objects at first.
    int recordCount = 0;
    int i = theResponseString.indexOf("<" + recordOrHeader + ">", 0);
    while (i != -1) {
      recordCount++;
      i++;
      i = theResponseString.indexOf("<" + recordOrHeader + ">", i);
    }

    System.out.println("\t" + theThreadName + recordOrHeader + "s: " + recordCount);

    // Check if token tag is existing.
    int tokStart = theResponseString.indexOf("<resumptionToken");
    int tokEnd = theResponseString.indexOf("</resumptionToken");

    if (tokStart == -1 && tokEnd == -1) {
      System.out.println("\t" + theThreadName + "token: no token");

      return NO_TOKEN;
    }

    String restokTmp = theResponseString.substring(tokStart, tokEnd);
    // Get token tag.
    String toktag = restokTmp.substring(0, restokTmp.indexOf(">") + 1).trim();
    System.out.println("\t" + theThreadName + "tokentag: " + toktag);

    // Get token.
    String restok = restokTmp.substring(restokTmp.indexOf(">") + 1).trim();
    System.out.println("\t" + theThreadName + "token: " + restok);

    // Check if old and new token are equal or not.
    boolean tokchanged = !oldtok.equals(restok);
    System.out.println("\t" + theThreadName + "tokchngd: " + tokchanged);

    // Get completeListSize and cursor.
    String sizeStr = toktag.substring(toktag.indexOf("completeListSize=\"") + 18);
    int size = Integer.parseInt(sizeStr.substring(0, sizeStr.indexOf("\"")));
    String cursorStr = toktag.substring(toktag.indexOf("cursor=\"") + 8);
    int cursor = Integer.parseInt(cursorStr.substring(0, cursorStr.indexOf("\"")));
    System.out.println("\t" + theThreadName + "complete list size: " + size);
    System.out.println("\t" + theThreadName + "delivered with last response: " + cursor);

    // If token is provided, test cursor and element count!
    if (!restok.isEmpty()) {
      synchronized (OaipmhUtilitiesOnline.class) {
        // Check <record> or <header> count, must be recordsExpectedPerRequest!
        if (recordCount != recordsExpectedPerRequest) {
          String message = recordOrHeader + " count mismatch, must be "
              + recordsExpectedPerRequest + " if token is provided, but is " + recordCount;
          assertTrue(message, false);
        }
        if (size <= recordsExpectedPerRequest) {
          String message = "completeListSize count mismatch, must be > "
              + recordsExpectedPerRequest + " if token is provided, but is " + size;
          assertTrue(message, false);
        }
        if (cursor != recordsExpectedPerRequest * (loopCount - 1)) {
          String message = "cursor must be " + (loopCount * recordsExpectedPerRequest)
              + " in loop " + loopCount + ", but is " + cursor;
          assertTrue(message, false);
        }
      }
    }

    // If no token is provided, stop querying.
    else {
      // Check <record> count, must be completeListSize % recordsExpectedPerRequest.
      // NOTE In case we have exactly recordsExpectedPerRequest records, we have to use moduloCount!
      int moduloCount = recordCount % recordsExpectedPerRequest;
      int moduloExpected = size % recordsExpectedPerRequest;
      if (moduloCount != moduloExpected) {
        String message = theThreadName + ": " + recordOrHeader + " count mismatch, should be "
            + moduloExpected + ", but is " + recordCount;
        assertTrue(message, false);
      }

      // No resumption token available in response.
      return NO_TOKEN;
    }

    return restok;
  }

  /**
   * @param theResponseString
   * @param theMetadataFormat
   */
  private static void examineTGContent(String theResponseString, String theMetadataFormat) {

    // Check for correct metadata content according to metadata prefix.
    if (theMetadataFormat.equals(OAI_DC_PREFIX)
        && !theResponseString.contains("metadataPrefix=\"" + OAI_DC_PREFIX + "\"")
        && !theResponseString.contains(EXPECTED_OAIDC_FORMAT_CONTENT)) {

      System.out.println("\tRESPONSE: " + theResponseString);

      String message = OAI_DC_PREFIX + " needs to deliver content with schema: "
          + EXPECTED_OAIDC_FORMAT_CONTENT + "!";
      assertTrue(message, false);

    } else if (theMetadataFormat.equals(OAI_IDIOMMETS_PREFIX)
        && !theResponseString
            .contains("metadataPrefix=\"" + EXPECTED_IDIOMMETS_FORMAT_CONTENT + "\"")
        && !theResponseString.contains(EXPECTED_IDIOMMETS_FORMAT_CONTENT)) {

      System.out.println("\tRESPONSE: " + theResponseString);

      String message = OAI_IDIOMMETS_PREFIX + " needs to deliver content with schema: "
          + EXPECTED_IDIOMMETS_FORMAT_CONTENT + "!";
      assertTrue(message, false);

    } else if (theMetadataFormat.equals(OAI_DATACITE_PREFIX)
        && !theResponseString.contains("metadataPrefix=\"" + OAI_DATACITE_PREFIX + "\"")
        && !theResponseString.contains(EXPECTED_DATACITE_FORMAT_CONTENT)) {

      System.out.println("\tRESPONSE: " + theResponseString);

      String message = OAI_DATACITE_PREFIX + " needs to deliver content with schema: "
          + EXPECTED_DATACITE_FORMAT_CONTENT + "!";
      assertTrue(message, false);
    }

    // DEBUG OUTPUT Trace empty record tags without metadata.
    // try {
    // File f = new File("kaputt.log");
    // if (!f.exists()) {
    // f.createNewFile();
    // }
    // FileWriter kaputt = new FileWriter(f, true);
    // String mustNOTcontain = "</header>\n" + " </record>";
    // if (theResponseString.contains(mustNOTcontain)) {
    // int loc = theResponseString.indexOf(mustNOTcontain);
    // kaputt.append("#########################################\n");
    // kaputt.append(loc + "\n");
    // kaputt.append("#########################################\n");
    // kaputt.append(theResponseString.substring(loc - 200, loc + 200) + "\n");
    // kaputt.append("#########################################\n");
    // }
    // kaputt.close();
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
  }

  /**
   * <p>
   * Creates a hash set containing all the ID values from a certain tag. Used to count elements such
   * as <identifier> in OAI-PMH header list responses. We want to know here, if we have ID
   * duplicates in a response.
   * </p>
   * 
   * @param theResponseString
   * @param theMetadataPrefix
   * @param theThreadName
   */
  public static void examineTGHeaderIDs(String theResponseString, String theMetadataPrefix,
      String theThreadName) {

    HashSet<String> idHash = new HashSet<String>();

    int startID = 0;
    int endID = 0;
    String id = "";
    int count = 0;
    // Look for all the header ID tags.
    while (startID != -1) {
      startID = theResponseString.indexOf(HEADER_ID_START_TAG, endID);
      endID = theResponseString.indexOf(HEADER_ID_END_TAG, startID);
      if (startID != -1) {
        count++;
        id = theResponseString.substring(startID + HEADER_ID_START_TAG.length(), endID);
        idHash.add(id);
      }
    }

    System.out.println("\t[" + theThreadName + "] ID hash (" + idHash.size() + "): " + idHash);

    // Check for ID count: Hash size (unique IDs!) must be equal to the count of ID headers!
    if (count != idHash.size()) {
      String message =
          "We must have " + count + " unique IDs in the headers, but we got only " + idHash.size();
      assertTrue(message, false);
    }

    // Check for revision URIs in IDIOM responses.
    for (String u : idHash) {
      examineTGIdentifiers(u, theMetadataPrefix);
    }
  }

  /**
   * <p>
   * Looks for both dates and compares them. <datestamp> must be equal to <recordChangeDate>!
   * </p>
   * 
   * @param theResponseString
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  public static void examineTGModificationDates(String theResponseString)
      throws ParseException, DatatypeConfigurationException {

    String datestampTag = "<datestamp>";
    String recordChangeTag = "<recordChangeDate encoding=\"iso8601\">";

    String datidate = theResponseString
        .substring(theResponseString.indexOf(datestampTag) + datestampTag.length());

    System.out.println(datidate);

    // Check occurrence of modification date, must be in GetRecord.record.header.datestamp, and in
    // mets.recordInfo.recordChangeDate!
    String headerDate =
        theResponseString.substring(theResponseString.indexOf(datestampTag) + datestampTag.length(),
            theResponseString.indexOf("</datestamp>")).trim();
    String recordChangeDate = OaipmhUtilities.getUTCDateAsString(theResponseString
        .substring(theResponseString.indexOf(recordChangeTag) + recordChangeTag.length(),
            theResponseString.indexOf("</recordChangeDate>"))
        .trim());

    if (!headerDate.equals(recordChangeDate)) {
      assertTrue(headerDate + " != " + recordChangeDate, false);
    }
  }

  // **
  // DH used methods
  // **

  /**
   * @param theResponse
   * @return Resumption token string, if existing, -1 if either no <resumptionToken> tag is
   *         existing, tag is existing and has no token value.
   * @throws IOException
   */
  public static String examineDHResumptionTokenTag(String theResponseString, String recordOrHeader,
      String oldtok) throws IOException {

    // Test for OAIPMH errors.
    if (theResponseString.contains("<error code=\"badArgument\">")) {
      assertTrue("ERROR IN OAIPMH RESPONSE: " + theResponseString, false);
    }

    // Count response objects at first.
    int recordCount = 0;
    int i = theResponseString.indexOf("<" + recordOrHeader + ">", 0);
    while (i != -1) {
      recordCount++;
      i++;
      i = theResponseString.indexOf("<" + recordOrHeader + ">", i);
    }

    System.out.println("\t" + recordOrHeader + "s: " + recordCount);

    // Check if token tag is existing.
    int tokStart = theResponseString.indexOf("<resumptionToken");
    int tokEnd = theResponseString.indexOf("</resumptionToken");

    if (tokStart == -1 && tokEnd == -1) {
      System.out.println("\ttoken: no token");

      return "-1";
    }

    String restokTmp = theResponseString.substring(tokStart, tokEnd);
    // Get token tag.
    String toktag = restokTmp.substring(0, restokTmp.indexOf(">") + 1).trim();
    System.out.println("\ttokentag: " + toktag);

    // Get token.
    String restok = restokTmp.substring(restokTmp.indexOf(">") + 1).trim();
    System.out.println("\ttoken: " + restok);

    // Check if old and new token are equal or not.
    boolean tokchanged = !oldtok.equals(restok);
    System.out.println("\ttokchngd: " + tokchanged);

    // Get completeListSize and cursor.
    String sizeStr = toktag.substring(toktag.indexOf("completeListSize=\"") + 18);
    int size = Integer.parseInt(sizeStr.substring(0, sizeStr.indexOf("\"")));
    String cursorStr = toktag.substring(toktag.indexOf("cursor=\"") + 8);
    int cursor = Integer.parseInt(cursorStr.substring(0, cursorStr.indexOf("\"")));
    System.out.println("\tsize: " + size + " / " + cursor);

    // If token is provided, and we have less than 100 elements: mekkern!
    if (!restok.isEmpty()) {
      synchronized (TestDHOaipmhOnline.class) {
        // Check <record> or <header> count, must be 100!
        String message = "";
        if (recordCount > 100) {
          message = recordOrHeader + " count mismatch, must be 100 if token is provided, but is "
              + recordCount;
        } else {
          message =
              "completeListSize count mismatch, must be > 100 if token is provided, but is " + size;
        }
        if (recordCount != 100 || size <= 100) {
          assertTrue(message, false);
        }
      }
    }

    // If no token is provided, stop querying.
    else {
      // Check <record> count, must be completeListSize % 100.
      if (recordCount != size % 100) {
        String message =
            recordOrHeader + " count mismatch, should be " + size % 100 + ", but is " + recordCount;
        assertTrue(message, false);
      }

      // No resumption token available in response.
      return "-1";
    }

    return restok;
  }

  /**
   * @param theClient
   * @param theVerb
   * @param theMetadataPrefix
   * @param theSet
   * @return
   * @throws IOException
   */
  public static int testDHList(Client theClient, String theVerb, String theMetadataPrefix,
      String theSet) throws IOException {
    return testDHList(theClient, theVerb, theMetadataPrefix, theSet, NO_FROM, NO_UNTIL);
  }

  /**
   * @param theClient
   * @param theVerb
   * @param theMetadataPrefix
   * @param theSet
   * @param from
   * @param until
   * @return How many pages were delivered
   * @throws IOException
   */
  public static int testDHList(Client theClient, String theVerb, String theMetadataPrefix,
      String theSet, String from, String until) throws IOException {

    int result;

    long startTime = System.currentTimeMillis();

    String testOccurance = "header";
    if (theVerb.equals(VERB_LIST_RECORDS)) {
      testOccurance = "record";
    }

    String query = "verb=" + theVerb + "&metadataPrefix=" + theMetadataPrefix;

    if (theSet != null && !theSet.isEmpty()) {
      query += "&set=" + theSet;
    }

    if (from != null && until != null) {
      query += "&from=" + from + "&until=" + until;
    }

    Response response = getOAIHttpResponse(theClient, query);

    String responseString = IOUtils.readStringFromStream((InputStream) response.getEntity());
    int status = response.getStatus();

    long timeRunning = System.currentTimeMillis() - startTime;
    System.out.println("\ttime: " + getDurationInSecs(timeRunning));

    String restok = examineDHResumptionTokenTag(responseString, testOccurance, "");
    examineDHIdentifiers(responseString);

    result = 1;
    while (status == HttpStatus.SC_OK && !restok.equals("-1")) {
      query = "verb=" + theVerb + "&resumptionToken=" + restok;
      response = getOAIHttpResponse(theClient, query);
      responseString = IOUtils.readStringFromStream((InputStream) response.getEntity());
      timeRunning = System.currentTimeMillis() - startTime;
      System.out.println("\ttime: " + getDurationInSecs(timeRunning));
      restok = examineDHResumptionTokenTag(responseString, testOccurance, restok);
      examineDHIdentifiers(responseString);
      result++;
    }

    System.out.println("\tpage amount: " + result);
    System.out.println(OK);

    return result;
  }

  /**
   * @param theClient
   * @param theSet
   * @param theExpectedResponse
   * @throws IOException
   */
  public static void testDHListSet(Client theClient, String theSet, String theExpectedResponse)
      throws IOException {

    System.out.println(TESTING + "#LISTSETS");

    Response response = getOAIHttpResponse(theClient, "verb=" + VERB_LIST_SETS);
    int status = response.getStatus();

    String responseString = IOUtils.readStringFromStream((InputStream) response.getEntity());

    if (status != HttpStatus.SC_OK || !responseString.contains(theExpectedResponse)) {
      String message = "[" + status + "] response should contain '" + theExpectedResponse + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + responseString);
    System.out.println(OK);
  }

  /**
   * @param theClient
   * @param theIdentifier
   * @param theVerb
   * @param theMetadataPrefix
   * @param theExpected
   * @throws IOException
   */
  public static void testDHRecord(Client theClient, String theIdentifier, String theVerb,
      String theMetadataPrefix, String theExpected) throws IOException {

    long startTime = System.currentTimeMillis();

    String query =
        "verb=" + theVerb + "&identifier=" + theIdentifier + "&metadataPrefix=" + theMetadataPrefix;

    Response response = getOAIHttpResponse(theClient, query);

    String responseString = IOUtils.readStringFromStream((InputStream) response.getEntity());
    int status = response.getStatus();

    System.out.println(status);
    System.out.println(responseString);

    long timeRunning = System.currentTimeMillis() - startTime;
    System.out.println("\ttime: " + getDurationInSecs(timeRunning));

    examineDHIdentifiers(responseString);

    if (!responseString.contains(theExpected)) {
      System.out.println("NOT CONTAINED: " + theExpected);
      assertTrue(false);
    } else {
      System.out.println(OaipmhUtilitiesOnline.OK);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theThreadName
   * @return
   */
  private static String tn(String theThreadName) {

    String result = theThreadName;

    if (theThreadName != null && !theThreadName.equals("") && !theThreadName.endsWith(" ")) {
      result = "[" + theThreadName + "] ";
    }

    return result;
  }

  /**
   * @param theResponseString
   * @return
   * @throws IOException
   */
  private static void examineDHIdentifiers(String theResponseString) throws IOException {
    if (theResponseString.contains("<identifier>hdl:21</identifier>")) {
      String message = "ERROR IN OAIPMH RESPONSE: identifier tag is corrupt!" + theResponseString;
      assertTrue(message, false);
    }
  }

  /**
   * @param theIdentifier
   * @param theMetadataPrefix
   */
  private static void examineTGIdentifiers(String theIdentifier, String theMetadataPrefix) {
    if (theMetadataPrefix.equals(OAI_IDIOMMETS_PREFIX)) {
      if (!OaipmhUtilities.getTextGridBaseURI(theIdentifier).equals(theIdentifier)) {
        String message =
            "ERROR IN OAIPMH RESPONSE: identifier " + theIdentifier + " is NOT a base URI!";
        assertTrue(message, false);
      }
    } else {
      if (OaipmhUtilities.getTextGridBaseURI(theIdentifier).equals(theIdentifier)) {
        String message =
            "ERROR IN OAIPMH RESPONSE: identifier " + theIdentifier + " MUST NOT BE a base URI!";
        assertTrue(message, false);
      }
    }
  }

}

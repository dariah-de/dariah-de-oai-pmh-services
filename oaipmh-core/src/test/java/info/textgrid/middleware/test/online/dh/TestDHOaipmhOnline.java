package info.textgrid.middleware.test.online.dh;

import static org.junit.Assert.assertTrue;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.ws.rs.core.Response;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Some online tests for the DARIAH-DE Repository OAIMPH service.
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES <b>HERE</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen //
 */
@Ignore
public class TestDHOaipmhOnline {

  // **
  // FINALS
  // **

  private static final String PROPERTIES_FILE = "oaipmh.test.repository-de-dariah-eu.properties";
  // private static final String PROPERTIES_FILE = "oaipmh.test.trep-de-dariah-eu.properties";
  // private static final String PROPERTIES_FILE =
  // "oaipmh.test.dhrepworkshop-de-dariah-eu.properties";

  // **
  // STATICS
  // **

  // Some JAXRS things.
  private static String oaipmhEndpoint;
  private static Client oaipmhWebClient;

  // Properties
  private static String checkGetRecordDC;
  private static String expectedGetRecordDC;

  private static String checkGetRecordDatacite;
  private static String expectedGetRecordDatacite;

  private static String checkListRecordsDCSet;
  private static Integer checkListRecordsDCSetExpectedPages;

  private static String checkListRecordsDCFrom;
  private static String checkListRecordsDCUntil;

  private static String checkListRecordsDataciteFrom;
  private static String checkListRecordsDataciteUntil;

  private static String checkListIdentifiersSet;
  private static Integer checkListIdentifiersSetExpectedPages;

  private static String expectedListSets;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Load properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(OaipmhUtilitiesOnline.getResource(PROPERTIES_FILE)));

    System.out.println("Properties file: " + PROPERTIES_FILE);
    p.list(System.out);

    // Get other needed properties.
    oaipmhEndpoint = p.getProperty("oaipmhEndpoint");

    checkGetRecordDC = p.getProperty("checkGetRecordDC");
    expectedGetRecordDC = p.getProperty("expectedGetRecordDC");

    checkGetRecordDatacite = p.getProperty("checkGetRecordDatacite");
    expectedGetRecordDatacite = p.getProperty("expectedGetRecordDatacite");

    checkListRecordsDCSet = p.getProperty("checkListRecordsDCSet");
    checkListRecordsDCSetExpectedPages =
        Integer.parseInt(p.getProperty("checkListRecordsDCSetExpectedPages").trim());

    checkListRecordsDCFrom = p.getProperty("checkListRecordsDCFrom");
    checkListRecordsDCUntil = p.getProperty("checkListRecordsDCUntil");

    checkListRecordsDataciteFrom = p.getProperty("checkListRecordsDataciteFrom");
    checkListRecordsDataciteUntil = p.getProperty("checkListRecordsDataciteUntil");

    checkListIdentifiersSet = p.getProperty("checkListIdentifiersSet");
    checkListIdentifiersSetExpectedPages =
        Integer.parseInt(p.getProperty("checkListIdentifiersSetExpectedPages").trim());

    expectedListSets = p.getProperty("expectedListSets");

    // Get web client from endpoint.
    oaipmhWebClient = OaipmhUtilitiesOnline.getOAIPMHWEebClient(oaipmhEndpoint);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   */
  @Test
  public void testGetVersion() throws IOException {

    String shouldStartWith = "oaipmh-core";

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#GETVERSION for '" + shouldStartWith + "'");

    Response response = OaipmhUtilitiesOnline.getVersionHttpResponse(oaipmhWebClient);
    int status = response.getStatus();
    String versionString = IOUtils.readStringFromStream((InputStream) response.getEntity());

    if (status != HttpStatus.SC_OK && !versionString.startsWith(shouldStartWith)) {
      String message = "[" + status + "]: response should start with '" + shouldStartWith + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + versionString);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testRootWithSlash() throws IOException {

    String url = "oai/";
    String shouldContain = "Illegal OAI verb";

    testRootURL(url, shouldContain);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testRootWithoutSlash() throws IOException {

    String url = "oai";
    String shouldContain = "Illegal OAI verb";

    testRootURL(url, shouldContain);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testBlubbVerb() throws IOException {

    String url = "verb=BLUBB";
    String shouldContain = "Illegal OAI verb";

    testRootURL(url, shouldContain);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testIdentify() throws IOException {

    String verb = "Identify";
    String shouldContain = "DARIAH-DE Repository";

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#IDENTIFY");

    Response response = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, "verb=" + verb);
    int status = response.getStatus();

    String responseString = IOUtils.readStringFromStream((InputStream) response.getEntity());

    if (status != HttpStatus.SC_OK || !responseString.contains(shouldContain)) {
      String message = "[" + status + "]: response should contain '" + shouldContain + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + responseString);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListSets() throws IOException {
    OaipmhUtilitiesOnline.testDHListSet(oaipmhWebClient,
        OaipmhUtilitiesOnline.NO_SET,
        expectedListSets);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        OaipmhUtilitiesOnline.NO_SET);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsSetDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS");

    int pages =
        OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            checkListRecordsDCSet);
    if (pages != checkListRecordsDCSetExpectedPages) {
      assertTrue(pages + " != " + checkListRecordsDCSetExpectedPages, false);
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsSetDCDifferentPrefixes() throws IOException {

    String uri = checkListRecordsDCSet;

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS - HDL PREFIX");

    if (!uri.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      assertTrue("missing '" + OaipmhUtilitiesOnline.HDL_PREFIX + "' prefix!", false);
    }
    int pages1 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        uri);
    if (pages1 != checkListRecordsDCSetExpectedPages) {
      System.out.println(pages1 + " != " + checkListRecordsDCSetExpectedPages);
    }

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS - NO PREFIX");

    if (checkListRecordsDCSet.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      uri = checkListRecordsDCSet.substring(4);
    }
    int pages2 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        uri);
    if (pages2 != checkListRecordsDCSetExpectedPages) {
      System.out.println(pages2 + " != " + checkListRecordsDCSetExpectedPages);
    }

    // Check pages count.
    if (pages1 != checkListRecordsDCSetExpectedPages
        || pages2 != checkListRecordsDCSetExpectedPages) {
      assertTrue("page count does not match", false);
    }
  }

  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testListRecordsSetDCStarPrefix() throws IOException {

    // TODO Un-ignore if identifier check is implemented!

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS - STAR PREFIX");

    if (checkListRecordsDCSet.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      checkListRecordsDCSet =
          OaipmhUtilitiesOnline.STAR_PREFIX + checkListRecordsDCSet.substring(4);
    }

    int pages3 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        checkListRecordsDCSet);
    if (pages3 != 1) {
      System.out.println(pages3 + " != " + 1);
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCFromUntil() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        OaipmhUtilitiesOnline.NO_SET,
        checkListRecordsDCFrom,
        checkListRecordsDCUntil);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDatacite() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        OaipmhUtilitiesOnline.NO_SET);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDataciteFromUntil() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTRECORDS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        OaipmhUtilitiesOnline.NO_SET,
        checkListRecordsDataciteFrom, checkListRecordsDataciteUntil);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#GETRECORD");

    OaipmhUtilitiesOnline.testDHRecord(oaipmhWebClient,
        checkGetRecordDC,
        OaipmhUtilitiesOnline.VERB_GET_RECORD,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        expectedGetRecordDC);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDatacite() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#GETRECORD");

    OaipmhUtilitiesOnline.testDHRecord(oaipmhWebClient,
        checkGetRecordDatacite,
        OaipmhUtilitiesOnline.VERB_GET_RECORD,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        expectedGetRecordDatacite);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        OaipmhUtilitiesOnline.NO_SET);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersSetDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS");

    int pages = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        checkListIdentifiersSet);
    if (pages != checkListIdentifiersSetExpectedPages) {
      assertTrue(pages + " != " + checkListIdentifiersSetExpectedPages, false);
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersSetDCDifferentPrefixes() throws IOException {

    String uri = checkListIdentifiersSet;

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS - HDL PREFIX");

    if (!uri.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      assertTrue("missing '" + OaipmhUtilitiesOnline.HDL_PREFIX + "' prefix!", false);
    }
    System.out.println("uri: " + uri);
    int pages1 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        uri);
    if (pages1 != checkListIdentifiersSetExpectedPages) {
      System.out.println(pages1 + " != " + checkListIdentifiersSetExpectedPages);
    }

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS - NO PREFIX");

    if (checkListIdentifiersSet.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      uri = checkListIdentifiersSet.substring(4);
    }
    System.out.println("uri: " + uri);
    int pages2 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        uri);
    if (pages2 != checkListIdentifiersSetExpectedPages) {
      System.out.println(pages2 + " != " + checkListIdentifiersSetExpectedPages);
    }

    // Check pages count.
    if (pages1 != checkListRecordsDCSetExpectedPages
        || pages2 != checkListRecordsDCSetExpectedPages) {
      assertTrue("pages count does not match", false);
    }
  }


  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testListIdentifiersSetDCStarPrefixe() throws IOException {

    // TODO Un-ignore if identifier check is implemented!

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS - STAR PREFIX");

    if (checkListIdentifiersSet.startsWith(OaipmhUtilitiesOnline.HDL_PREFIX)) {
      checkListIdentifiersSet =
          OaipmhUtilitiesOnline.STAR_PREFIX + checkListIdentifiersSet.substring(4);
    }
    System.out.println("uri: " + checkListIdentifiersSet);

    int pages3 = OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        checkListIdentifiersSet);
    if (pages3 != checkListIdentifiersSetExpectedPages) {
      System.out.println(pages3 + " != " + checkListIdentifiersSetExpectedPages);
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersDatacite() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#LISTIDENTIFIERS");

    OaipmhUtilitiesOnline.testDHList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        OaipmhUtilitiesOnline.NO_SET);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theURL
   * @param shouldContain
   * @throws IOException
   */
  private static void testRootURL(String theURL, String shouldContain)
      throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "#ROOTURL");

    Response response = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, theURL);
    int status = response.getStatus();

    String responseString = IOUtils
        .readStringFromStream((InputStream) response.getEntity());

    if (status != HttpStatus.SC_OK || !responseString.contains(shouldContain)) {
      String message = "[" + status + "] response should contain '" + shouldContain + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + responseString);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

}

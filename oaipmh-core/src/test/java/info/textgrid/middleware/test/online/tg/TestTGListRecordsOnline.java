package info.textgrid.middleware.test.online.tg;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.cxf.jaxrs.client.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Some online tests for the TextGrid OAIMPH service --> verb=ListRecords <--
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES in class <b>OAIPMHUtilitiesOnline</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
@Ignore
public class TestTGListRecordsOnline {

  // **
  // STATICS
  // **

  // Some JAXRS things.
  private static String oaipmhEndpoint;
  static Client oaipmhWebClient;

  // Properties
  private static String checkListRecordsDCSet;
  private static String checkListRecordsDCFrom;
  private static String checkListRecordsDCUntil;
  private static String checkListRecordsDataciteFrom;
  private static String checkListRecordsDataciteUntil;
  private static String checkListRecordsIdiomFrom;
  private static String checkListRecordsIdiomUntil;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get properties.
    Properties p =
        OaipmhUtilitiesOnline.getPropertiesFromFile(OaipmhUtilitiesOnline.PROPERTIES_FILE);

    // Set properties.
    oaipmhEndpoint = p.getProperty("oaipmhEndpoint");

    checkListRecordsDCSet = p.getProperty("checkListRecordsDCSet");
    checkListRecordsDCFrom = p.getProperty("checkListRecordsDCFrom");
    checkListRecordsDCUntil = p.getProperty("checkListRecordsDCUntil");

    checkListRecordsDataciteFrom = p.getProperty("checkListRecordsDataciteFrom");
    checkListRecordsDataciteUntil = p.getProperty("checkListRecordsDataciteUntil");

    checkListRecordsIdiomFrom = p.getProperty("checkListRecordsIdiomFrom");
    checkListRecordsIdiomUntil = p.getProperty("checkListRecordsIdiomUntil");

    // Get web client from endpoint.
    oaipmhWebClient = OaipmhUtilitiesOnline.getOAIPMHWEebClient(oaipmhEndpoint);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCMorePages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDCMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        18, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCSetMorePages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDCSetMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        checkListRecordsDCSet,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        20, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCSetMorePagesNoProjectPrefix() throws IOException {

    System.out.println(
        OaipmhUtilitiesOnline.TESTING + "testListRecordsDCSetMorePagesNoProjectPrefix()");

    String setWithoutProjectPRefix = checkListRecordsDCSet.replace("project:", "");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        setWithoutProjectPRefix,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        1, 0,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCAllPages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDCAllPages()");

    if (OaipmhUtilitiesOnline.TEST_ALL_PAGES) {

      OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
          OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
          OaipmhUtilitiesOnline.NO_SET,
          OaipmhUtilitiesOnline.OAI_DC_PREFIX,
          0, 100,
          OaipmhUtilitiesOnline.NO_THREAD_NAME,
          OaipmhUtilitiesOnline.NO_FROM,
          OaipmhUtilitiesOnline.NO_UNTIL,
          OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
          OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

      System.out.println(OaipmhUtilitiesOnline.OK);
    } else {
      System.out.println("...skipping");
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDCMorePagesFromUntil() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDCMorePagesFromUntil()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        checkListRecordsDCSet,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        0, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        checkListRecordsDCFrom,
        checkListRecordsDCUntil,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test(expected = IOException.class)
  public void testListRecordsIdiomMETSSomePagesAndMetadataFormatWithRestok() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING
        + "testListRecordsIdiomMETSSomePagesAndMetadataFormatWithRestok()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        3, 30,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.METADATA_FORMAT_WITH_RESTOK, OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsIdiomMETSAllPages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsIdiomMETSAllPages()");

    if (OaipmhUtilitiesOnline.TEST_ALL_PAGES) {

      OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
          OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
          OaipmhUtilitiesOnline.NO_SET,
          OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
          0, 30,
          OaipmhUtilitiesOnline.NO_THREAD_NAME,
          OaipmhUtilitiesOnline.NO_FROM,
          OaipmhUtilitiesOnline.NO_UNTIL,
          OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
          OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

      System.out.println(OaipmhUtilitiesOnline.OK);
    } else {
      System.out.println("...skipping");
    }
  }

  /**
   * NOTE The amount of records will change due to changes on the data! Please check from and until
   * values before!
   *
   * @throws IOException
   */
  @Test
  public void testListRecordsIdiomMETSMorePagesFromUntil() throws IOException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testListRecordsIdiomMETSMorePagesFromUntil()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        6, 30,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        checkListRecordsIdiomFrom,
        checkListRecordsIdiomUntil,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsIdiomMETSMorePages() throws IOException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testListRecordsIdiomMETSMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        6, 30,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDataciteMorePages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDataciteMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        15, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDataciteAllPages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDataciteAllPages()");

    if (OaipmhUtilitiesOnline.TEST_ALL_PAGES) {
      OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
          OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
          OaipmhUtilitiesOnline.NO_SET,
          OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
          0, 100,
          OaipmhUtilitiesOnline.NO_THREAD_NAME,
          OaipmhUtilitiesOnline.NO_FROM,
          OaipmhUtilitiesOnline.NO_UNTIL,
          OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
          OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

      System.out.println(OaipmhUtilitiesOnline.OK);
    } else {
      System.out.println("...skipping");
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListRecordsDataciteMorePagesFromUntil() throws IOException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testListRecordsDataciteMorePagesFromUntil()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        15, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        checkListRecordsDataciteFrom,
        checkListRecordsDataciteUntil,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @Test
  public void testListRecordsConcurrentlyIdiomMETSMorePages()
      throws IOException, InterruptedException, ExecutionException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testListRecordsConcurrentlyIdiomMETSMorePages()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            2, 30,
            "IDIOM1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            3, 30,
            "IDIOM2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            4, 30,
            "IDIOM3"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [IDIOM1]=" + f1.get() + ", [IDIOM2]="
        + f2.get() + ", [IDIOM3]=" + f3.get());
  }


  /**
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListRecordsDC()
      throws InterruptedException, ExecutionException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testRestokConcurrentlyListRecordsDC()");

    ExecutorService executor = Executors.newFixedThreadPool(4);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            44, 100,
            "DC1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            33, 100,
            "DC2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            66, 100,
            "DC3"));
    Future<Boolean> f4 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            28, 100,
            "DC4"));

    executor.shutdown();

    System.out.println(
        OaipmhUtilitiesOnline.OK + ": [DC1]=" + f1.get() + ", [DC2]=" + f2.get() + ", [DC3]="
            + f3.get() + ", [DC4]=" + f4.get());
  }

  /**
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListRecords() throws InterruptedException, ExecutionException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testRestokConcurrentlyListRecords()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            6, 100,
            "B1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            7, 100,
            "B2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            8, 100,
            "B3"));
    Future<Boolean> f4 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            9, 100,
            "B4"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [B1]=" + f1.get() + ", [B2]=" + f2.get()
        + ", [B3]=" + f3.get() + ", [B4]=" + f4.get());
  }

  /**
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testListRecordsRestokConcurrentlyDCAndIdiomMETS()
      throws InterruptedException, ExecutionException {

    System.out.println(
        OaipmhUtilitiesOnline.TESTING + "testListRecordsRestokConcurrentlyDCAndIdiomMets()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            34, 100,
            "DC1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            27, 100,
            "DC2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_RECORDS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            5, 30,
            "IDIOM"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [DC1]=" + f1.get() + ", [DC2]=" + f2.get()
        + ", [IOIOM]=" + f3.get());
  }

}

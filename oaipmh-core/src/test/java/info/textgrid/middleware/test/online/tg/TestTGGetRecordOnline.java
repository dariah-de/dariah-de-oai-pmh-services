package info.textgrid.middleware.test.online.tg;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Some online tests for the TextGrid OAIMPH service --> verb=GetRecord <--
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES in class <b>OAIPMHUtilitiesOnline</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
@Ignore
public class TestTGGetRecordOnline {

  // **
  // STATICS
  // **

  // Some JAXRS things.
  private static String oaipmhEndpoint;
  static Client oaipmhWebClient;

  // Properties
  private static String checkGetRecordDC;
  private static String expectedGetRecordDC;
  private static String checkGetRecordDCNoEdition;
  private static String expectedGetRecordDCNoEdition;

  private static String checkGetRecordDatacite;
  private static String expectedGetRecordDatacite;
  private static String checkGetRecordDataciteNoEdition;
  private static String expectedGetRecordDataciteNoEdition;

  private static String checkGetRecordIdiom;
  private static String expectedGetRecordIdiom;

  private static String checkGetRecordIdiomImage;
  private static String expectedGetRecordIdiomImage;

  private static List<String> checkGetRecordIDList = new ArrayList<String>();

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get properties.
    Properties p =
        OaipmhUtilitiesOnline.getPropertiesFromFile(OaipmhUtilitiesOnline.PROPERTIES_FILE);

    // Set properties.
    oaipmhEndpoint = p.getProperty("oaipmhEndpoint");

    checkGetRecordDC = p.getProperty("checkGetRecordDC");
    expectedGetRecordDC = p.getProperty("expectedGetRecordDC");
    checkGetRecordDCNoEdition = p.getProperty("checkGetRecordDCNoEdition");
    expectedGetRecordDCNoEdition = p.getProperty("expectedGetRecordDCNoEdition");

    checkGetRecordDatacite = p.getProperty("checkGetRecordDatacite");
    expectedGetRecordDatacite = p.getProperty("expectedGetRecordDatacite");
    checkGetRecordDataciteNoEdition = p.getProperty("checkGetRecordDataciteNoEdition");
    expectedGetRecordDataciteNoEdition = p.getProperty("expectedGetRecordDataciteNoEdition");

    checkGetRecordIdiom = p.getProperty("checkGetRecordIdiom");
    expectedGetRecordIdiom = p.getProperty("expectedGetRecordIdiom");

    checkGetRecordIdiomImage = p.getProperty("checkGetRecordIdiomImage");
    expectedGetRecordIdiomImage = p.getProperty("expectedGetRecordIdiomImage");

    checkGetRecordIDList =
        OaipmhUtilitiesOnline.getListFromProperties((String) p.get("checkGetRecordIDList"));

    // Get web client from endpoint.
    oaipmhWebClient = OaipmhUtilitiesOnline.getOAIPMHWEebClient(oaipmhEndpoint);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDC() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordDC()");

    String query = "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier="
        + checkGetRecordDC + "&metadataPrefix=" + OaipmhUtilitiesOnline.OAI_DC_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordDC)) {
      String message = "[" + status + "] response should contain '" + expectedGetRecordDC + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDCNoEdition() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordDCNoEdition()");

    String query = "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier="
        + checkGetRecordDCNoEdition + "&metadataPrefix=" + OaipmhUtilitiesOnline.OAI_DC_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordDCNoEdition)) {
      String message =
          "[" + status + "] response should contain '" + expectedGetRecordDCNoEdition + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @param theThreadName
   * @throws IOException
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetRecordIdiomMETS()
      throws IOException, ParseException, DatatypeConfigurationException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordIdiomMETS()");

    String query = "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier="
        + checkGetRecordIdiom + "&metadataPrefix=" + OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordIdiom)) {
      String message = "[" + status + "] response should contain '" + expectedGetRecordIdiom + "'";
      assertTrue(message, false);
    }

    // Test OAI header.
    OaipmhUtilitiesOnline.examineTGHeaderIDs(response, OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        OaipmhUtilitiesOnline.NO_THREAD_NAME);
    OaipmhUtilitiesOnline.examineTGModificationDates(response);

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @param theThreadName
   * @throws IOException
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetRecordIdiomMETSImage()
      throws IOException, ParseException, DatatypeConfigurationException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordIdiomMETS_Image()");

    String query =
        "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier=" + checkGetRecordIdiomImage
            + "&metadataPrefix=" + OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordIdiomImage)) {
      String message =
          "[" + status + "] response should contain '" + expectedGetRecordIdiomImage + "'";
      assertTrue(message, false);
    }

    // Test OAI header.
    OaipmhUtilitiesOnline.examineTGHeaderIDs(response, OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        OaipmhUtilitiesOnline.NO_THREAD_NAME);
    OaipmhUtilitiesOnline.examineTGModificationDates(response);

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDatacite() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordDatacite()");

    String query = "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier="
        + checkGetRecordDatacite + "&metadataPrefix=" + OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordDatacite)) {
      String message =
          "[" + status + "] response should contain '" + expectedGetRecordDatacite + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testGetRecordDataciteNoEdition() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetRecordDataciteNoEdition()");

    String query = "verb=" + OaipmhUtilitiesOnline.VERB_GET_RECORD + "&identifier="
        + checkGetRecordDataciteNoEdition + "&metadataPrefix="
        + OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        query);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedGetRecordDataciteNoEdition)) {
      String message =
          "[" + status + "] response should contain '" + expectedGetRecordDataciteNoEdition + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testConcurrentlyDCGetRecord() throws InterruptedException, ExecutionException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testConcurrentlyDCGetRecord()");

    ExecutorService executor = Executors.newFixedThreadPool(18);

    Future<Boolean> f1 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(0), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f2 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(1), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f3 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(2), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f4 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(3), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f5 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(4), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f6 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(5), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f7 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(6), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f8 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(7), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f9 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(8), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f10 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(9), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f11 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(10), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f12 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(11), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f13 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(12), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f14 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(13), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f15 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(14), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f16 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(15), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f17 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(16), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));
    Future<Boolean> f18 =
        executor.submit(new OaiPmhGetRecordTestThread(OaipmhUtilitiesOnline.VERB_GET_RECORD,
            checkGetRecordIDList.get(17), OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            "GR[" + checkGetRecordIDList.get(0) + "]"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + "[GR1]=" + f1.get() +
        ", [GR2]=" + f2.get() +
        ", [GR3]=" + f3.get() +
        ", [GR4]=" + f4.get() +
        ", [GR5]=" + f5.get() +
        ", [GR6]=" + f6.get() +
        ", [GR7]=" + f7.get() +
        ", [GR8]=" + f8.get() +
        ", [GR9]=" + f9.get() +
        ", [GR10]=" + f10.get() +
        ", [GR11]=" + f11.get() +
        ", [GR12]=" + f12.get() +
        ", [GR13]=" + f13.get() +
        ", [GR14]=" + f14.get() +
        ", [GR15]=" + f15.get() +
        ", [GR16]=" + f16.get() +
        ", [GR17]=" + f17.get() +
        ", [GR18]=" + f18.get());
  }

  /**
   * <p>
   * Starts a new OAI-PMH thread.
   * </p>
   */
  private class OaiPmhGetRecordTestThread implements Callable<Boolean> {

    protected String verb;
    protected String identifier;
    protected String metadataPrefix;
    protected String threadName;

    /**
     * @param theVerb
     * @param theIdentifier
     * @param theMetadataPrefix
     * @param theThreadName
     */
    public OaiPmhGetRecordTestThread(String theVerb, String theIdentifier, String theMetadataPrefix,
        String theThreadName) {
      this.verb = theVerb;
      this.identifier = theIdentifier;
      this.metadataPrefix = theMetadataPrefix;
      this.threadName = theThreadName;
    }

    /**
     *
     */
    @Override
    public Boolean call() throws IOException {

      String query = "verb=" + this.verb + "&identifier=" + this.identifier + "&metadataPrefix="
          + this.metadataPrefix;
      OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, this.threadName, query);

      return true;
    }
  }

}

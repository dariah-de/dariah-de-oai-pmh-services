package info.textgrid.middleware.test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.IdentifierListDelivererDC;
import info.textgrid.middleware.IdentifierListDelivererDatacite;
import info.textgrid.middleware.IdentifierListDelivererIdiom;
import info.textgrid.middleware.IdiomImages;
import info.textgrid.middleware.MetadataFormatListDelivererInterface;
import info.textgrid.middleware.MetadataFormatListDelivererTG;
import info.textgrid.middleware.OaipmhConstants;
import info.textgrid.middleware.OaipmhElasticSearchClient;
import info.textgrid.middleware.OaipmhImpl;
import info.textgrid.middleware.OaipmhUtilities;
import info.textgrid.middleware.RecordDelivererDC;
import info.textgrid.middleware.RecordDelivererDatacite;
import info.textgrid.middleware.RecordDelivererIdiom;
import info.textgrid.middleware.RecordListDelivererDC;
import info.textgrid.middleware.RecordListDelivererDatacite;
import info.textgrid.middleware.RecordListDelivererIdiom;
import info.textgrid.middleware.RepIdentification;
import info.textgrid.middleware.SetListDeliverer;
import info.textgrid.middleware.common.TextGridMimetypes;

/**
 * TODO Add documentation how to use this class, use SSH tunnel?
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
@Ignore
public class TestTGOaipmhLocally {

  public static final String ITEM_IDENTIFIER_PREFIX = "textgrid:";
  public static final String URI = "textgridUri";
  public static final String PID = "pid.value";
  public static final String CREATED = "created";
  public static final String NOTES = "notes";
  public static final String FORMAT = "format";
  public static final String[] FORMAT_LIST = {FORMAT};
  public static final String IDENTIFIER = "identifier";
  public static final String[] IDENTIFIER_LIST = {URI, PID, IDENTIFIER};
  public static final String DATA_CONTRIBUTOR = "dataContributor";
  public static final String[] CONTRIBUTOR_LIST = {DATA_CONTRIBUTOR};
  public static final String PROJECT_ID = "project.id";
  public static final String IS_DERIVED_FROM = "relations.isDerivedFrom";
  public static final String[] RELATIONS_LIST = {PROJECT_ID, IS_DERIVED_FROM};
  public static final String TITLE = "title";
  public static final String LAST_MODIFIED = "lastModified";
  public static final String EDITION_ISEDITIONOF = "edition.isEditionOf";
  public static final String EDITION_AGENT = "edition.agent.value";
  public static final String EDITION_LICENSEURI = "edition.license.licenseUri";
  public static final String EDITION_LANGUAGE = "edition.language";
  public static final String BIBCIT_AUTHOR =
      "edition.source.bibliographicCitation.author.value";
  public static final String BIBCIT_EDITOR =
      "edition.source.bibliographicCitation.editor.value";
  public static final String BIBCIT_TITLE =
      "edition.source.bibliographicCitation.editionTitle";
  public static final String BIBCIT_PLACEPUB =
      "edition.source.bibliographicCitation.placeOfPublication.value";
  public static final String BIBCIT_PUBLISHER =
      "edition.source.bibliographicCitation.publisher.value";
  public static final String BIBCIT_NO = "edition.source.bibliographicCitation.editionNo";
  public static final String BIBCIT_SERIES = "edition.source.bibliographicCitation.series";
  public static final String BIBCIT_VOLUME = "edition.source.bibliographicCitation.volume";
  public static final String BIBCIT_ISSUE = "edition.source.bibliographicCitation.issue";
  public static final String BIBCIT_EPAGE = "edition.source.bibliographicCitation.epage";
  public static final String BIBCIT_SPAGE = "edition.source.bibliographicCitation.spage";
  public static final String BIBCIT_BIBID = "edition.source.bibliographicCitation.bibidentifier";
  public static final String WORK_ABSTRACT = "work.abstract";
  public static final String WORK_GENRE = "work.genre";
  public static final String WORK_TYPE = "work.type";
  public static final String WORK_SPATIAL = "work.spatial.value";
  public static final String WORK_TEMPORAL = "work.temporal.value";
  public static final String WORK_AGENT = "work.agent.value";
  public static final String WORK_ID = "work.subject.id.value";
  public static final String RANGE_FIELD = CREATED;
  // String Arrays to define which TextGrid fields belongs to the regarding DC fields
  public static final String[] COVERAGE_LIST = {WORK_SPATIAL, WORK_TEMPORAL};
  public static final String[] CREATOR_LIST = {WORK_AGENT};
  public static final String[] DATE_LIST = {CREATED};
  public static final String[] DESCRIPTION_LIST = {WORK_ABSTRACT};
  public static final String[] LANGUAGE_LIST = {EDITION_LANGUAGE};
  public static final String[] PUBLISHER_LIST = {BIBCIT_PUBLISHER};
  // Some more lists.
  public static final String[] TGFields =
      {CREATED, FORMAT, IDENTIFIER, DATA_CONTRIBUTOR, URI, PROJECT_ID, IS_DERIVED_FROM, TITLE, PID,
          EDITION_ISEDITIONOF, EDITION_AGENT, EDITION_LICENSEURI, EDITION_LANGUAGE, BIBCIT_AUTHOR,
          BIBCIT_EDITOR, BIBCIT_TITLE, BIBCIT_PLACEPUB, BIBCIT_PUBLISHER, BIBCIT_NO, BIBCIT_SERIES,
          BIBCIT_VOLUME, BIBCIT_ISSUE, BIBCIT_EPAGE, BIBCIT_SPAGE, BIBCIT_BIBID, WORK_ABSTRACT,
          WORK_GENRE, WORK_TYPE, WORK_SPATIAL, WORK_TEMPORAL, WORK_AGENT, LAST_MODIFIED};
  public static final String[] TGWorkFields = {CREATED, WORK_ABSTRACT, IS_DERIVED_FROM, URI,
      WORK_GENRE, TITLE, WORK_TYPE, WORK_SPATIAL, WORK_TEMPORAL, WORK_AGENT, WORK_ID};
  public static final String[] SOURCE_LIST =
      {BIBCIT_AUTHOR, BIBCIT_EDITOR, BIBCIT_TITLE, BIBCIT_PLACEPUB, BIBCIT_PUBLISHER, BIBCIT_NO,
          BIBCIT_SERIES, BIBCIT_VOLUME, BIBCIT_ISSUE, BIBCIT_SPAGE, BIBCIT_EPAGE};
  public static final String[] RIGHTS_LIST = {EDITION_LICENSEURI};
  public static final String[] SUBJECT_LIST = {WORK_ID};
  public static final String[] TITLE_LIST = {TITLE};
  public static final String[] TYPE_LIST = {WORK_GENRE, WORK_TYPE};
  public static final String[] IDENTIFIER_LIST_FIELDS = {URI, CREATED, FORMAT};
  public static final String[] RELATIONS_FOR_WORK_LIST = {IS_DERIVED_FROM, TITLE, URI};

  // **
  // STATICS
  // **

  public static OaipmhElasticSearchClient oaiEsClient;

  private static RecordDelivererDC record;
  private static RecordDelivererIdiom recordIDIOM;
  private static RecordDelivererDatacite recordDATACITE;
  private static RecordListDelivererDatacite recordListDATACITE;
  private static IdentifierListDelivererDatacite identifierListDATACITE;
  private static RecordListDelivererDC recordList;
  private static RecordListDelivererIdiom recordListIDIOM;
  private static IdentifierListDelivererDC identifierList =
      new IdentifierListDelivererDC(true, false);
  private static IdentifierListDelivererIdiom identifierListIDIOM =
      new IdentifierListDelivererIdiom(true, false);
  private static SetListDeliverer setListTextGrid;

  // **
  // CLASS
  // **

  private RepIdentification rep =
      new RepIdentification("TextGrid-Repository", "https://dev.textgridlab.org",
          "textgrid-support@gwdg.de", "2011-06-11T02:32:40Z");
  private MetadataFormatListDelivererInterface metadataFormatList =
      new MetadataFormatListDelivererTG();
  private OaipmhImpl request = new OaipmhImpl(this.rep,
      TestTGOaipmhLocally.record,
      TestTGOaipmhLocally.recordIDIOM,
      TestTGOaipmhLocally.recordDATACITE,
      TestTGOaipmhLocally.recordList,
      TestTGOaipmhLocally.recordListIDIOM,
      TestTGOaipmhLocally.recordListDATACITE,
      this.metadataFormatList,
      TestTGOaipmhLocally.setListTextGrid,
      TestTGOaipmhLocally.identifierList,
      TestTGOaipmhLocally.identifierListIDIOM,
      TestTGOaipmhLocally.identifierListDATACITE);

  OaipmhUtilities settings = new OaipmhUtilities();

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUp() throws Exception {

    int[] ports = new int[] {9202};
    oaiEsClient = new OaipmhElasticSearchClient("localhost", ports);
    oaiEsClient.setEsIndex("textgrid-public");

    record = new RecordDelivererDC(true, false);
    record.setOaiEsClient(oaiEsClient);
    record.setWorkFields(TGWorkFields);

    recordIDIOM = new RecordDelivererIdiom(true, false);
    recordIDIOM.setOaiEsClient(oaiEsClient);

    recordDATACITE = new RecordDelivererDatacite(true, false);
    recordDATACITE.setOaiEsClient(oaiEsClient);

    recordListDATACITE = new RecordListDelivererDatacite(true, false);
    recordListDATACITE.setOaiEsClient(oaiEsClient);

    identifierListIDIOM = new IdentifierListDelivererIdiom(true, false);
    recordList = new RecordListDelivererDC(true, false);
    recordList.setFields(TGFields);
    recordList.setWorkFields(TGWorkFields);
    recordList.setOaiEsClient(oaiEsClient);
    recordList.setSearchResponseSize(100);

    recordListIDIOM = new RecordListDelivererIdiom(true, false);
    recordListIDIOM.setOaiEsClient(oaiEsClient);

    identifierList = new IdentifierListDelivererDC(true, false);
    identifierList.setOaiEsClient(oaiEsClient);
    identifierList.setIdentifierListFields(IDENTIFIER_LIST_FIELDS);

    setListTextGrid = new SetListDeliverer(true, false);
  }

  /**
   * 
   */
  @Test
  @Ignore
  public void testOpenAireIdentifierList() {

    this.settings.setRangeField(RANGE_FIELD);
    this.settings.setFormatField(FORMAT);
    this.settings.setFormatToFilter(TextGridMimetypes.EDITION);
    this.settings.setSearchResponseSize(100);

    String p =
        this.request.getRequest("ListIdentifiers", "", "oai_datacite", "", "2013", "2014", "");

    System.out.println(p);
  }

  /**
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testDateNow() throws DatatypeConfigurationException {

    System.out.println("---------------Test Datestamp Parsing with now date -----------------");
    String nowTest = OaipmhUtilities.getCurrentUTCDateAsString();
    System.out.println(nowTest);
    System.out.println("-------------------------------------------");
  }

  /**
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testDateGregorianType() throws ParseException, DatatypeConfigurationException {

    String dateformatbefore = "2012-02-06T20:48:39.614+01:00";
    System.out.println("--------- Test Datestamp Parsing with a given date ---------------");
    System.out.println("Original date: " + dateformatbefore);
    XMLGregorianCalendar testDate = OaipmhUtilities.getUTCDateAsGregorian(dateformatbefore);
    System.out.println("Date after conversion: " + testDate);
    System.out.println("---------------------------------------------");
  }

  /**
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testDateStringType() throws ParseException, DatatypeConfigurationException {

    String dateformatbefore = "2012-02-06T20:48:39.614+01:00";
    System.out.println("------------String Version-------------------");
    System.out.println("Original date: " + dateformatbefore);
    String testDate = OaipmhUtilities.getUTCDateAsString(dateformatbefore);
    System.out.println("Date after conversion: " + testDate);
    System.out.println("---------------------------------------------");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestIdentify() throws ParseException {

    System.out.println("Test for the verb \"Identify\" with successful response");
    String r = this.request.getRequest("Identify", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestIdentifyVerbError() throws ParseException {

    System.out.println("Test for the verb \"Identify\" with error response (MetadaFormat)");
    String t = this.request.getRequest("Identify", "", "a", "", "", "", "");
    System.out.println(t);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestIdentifyArgumentError() throws ParseException {

    System.out.println("Test for the verb \"Identify\" with error response (Until and Identifier)");
    String z = this.request.getRequest("Identify", "a", "", "", "", "a", "");
    System.out.println(z);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testGetRequestGetRecordTextGrid() throws ParseException {

    record.setContributor(CONTRIBUTOR_LIST);
    record.setCoverage(COVERAGE_LIST);
    record.setCreator(CREATOR_LIST);
    record.setDates(DATE_LIST);
    record.setDescriptions(DESCRIPTION_LIST);
    record.setFormats(FORMAT_LIST);
    record.setIdentifiers(IDENTIFIER_LIST);
    record.setLanguages(LANGUAGE_LIST);
    record.setPublishers(PUBLISHER_LIST);
    record.setRelations(RELATIONS_LIST);
    record.setRights(RIGHTS_LIST);
    record.setSources(SOURCE_LIST);
    record.setSubjects(SUBJECT_LIST);
    record.setTitles(TITLE_LIST);
    record.setTypes(TYPE_LIST);
    record.setFields(TGFields);
    record.setFormatField(FORMAT);
    record.setFormatToFilter(TextGridMimetypes.EDITION);
    record.setDateOfObjectCreation(CREATED);
    record.setRelationToFurtherMetadataObject(EDITION_ISEDITIONOF);
    record.setRepositoryObjectURIPrefix(ITEM_IDENTIFIER_PREFIX);
    record.setIdentifierField("textgridUri");

    System.out.println("Test for the verb \"GetRecord\" with successful response");
    String p = this.request.getRequest("GetRecord", "textgrid:11hp0.0", "oai_dc", "", "", "", "");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testGetRequestListRecordsIDIOMImagesWithResToken() throws ParseException {

    System.out.println("Test for the verb \"ListRecords\" with successful response for IMAGES");
    IdiomImages.getCursorCollector().put("2930c87d-5209-4b14-9d6a-72f10476d947", 129);
    String p = this.request.getRequest("ListRecords", "",
        "", "", "", "", "2930c87d-5209-4b14-9d6a-72f10476d947");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testGetRequestListIdentifiersIDIOMImageswithResToken() throws ParseException {

    System.out.println("Test for the verb \"ListIdentifiers\" with successful response");
    IdiomImages.getCursorCollector().put("8eea2c07-7dc9-4404-a106-c1412b9ce8b4", 129);
    String p = this.request.getRequest("ListIdentifiers", "",
        "", "", "", "", "8eea2c07-7dc9-4404-a106-c1412b9ce8b4");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestGetRecordIDIOM() throws ParseException {

    System.out.println("Test for the verb \"GetRecord\" with successful response");
    String p = this.request.getRequest("GetRecord", "textgrid:3whkp",
        OaipmhConstants.METADATA_IDIOM_PREFIX, "", "", "", "");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   * @throws JAXBException
   * @throws DatatypeConfigurationException
   * @throws IOException
   */
  @Test
  @Ignore
  public void testGetRequestGetRecordOpenAire()
      throws ParseException, JAXBException, DatatypeConfigurationException, IOException {

    System.out.println("Test for OpenAire get record");

    System.out.println("-----------------------------------\n");
    // OAIPMHUtilities.marshal(oar.getResource());
    // System.out.println(OAIPMHImpl.getStringFromJAXBOAIElement("GetRecord",
    // oar.getRecord("24gv8.0")));

    // OAIPMHUtilities.marshal(oar.getRecord("24gv8.0"));

    String p = this.request.getRequest("GetRecord", "mk7j.0",
        OaipmhConstants.METADATA_OPENAIRE_PREFIX, "", "", "", "");

    System.out.println(p);
  }

  /**
   * @throws IOException
   */
  @Test
  @Ignore
  public void testLIstRecordsOpenAire() throws IOException {

    recordListDATACITE.setOaiEsClient(oaiEsClient);
    recordListDATACITE.setRangeField(RANGE_FIELD);
    recordListDATACITE.setFormatField(FORMAT);
    recordListDATACITE.setFormatToFilter(TextGridMimetypes.EDITION);
    recordListDATACITE.setSearchResponseSize(100);

    String p = this.request.getRequest("ListRecords", "", OaipmhConstants.METADATA_OPENAIRE_PREFIX,
        "", "", "", "");

    System.out.println(p);

  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestGetRecordErrorIdentifierMissing() throws ParseException {

    System.out.println("Test for the verb \"GetRecord\" with error response (Identifier Missing)");
    String p = this.request.getRequest("GetRecord", "", "", "", "", "", "");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListIdentifierSetsDC() throws ParseException {

    TestTGOaipmhLocally.identifierList.setFieldForRange(RANGE_FIELD);
    TestTGOaipmhLocally.identifierList.setIdentifierListFields(IDENTIFIER_LIST_FIELDS);
    TestTGOaipmhLocally.identifierList.setDateOfObjectCreation(CREATED);
    TestTGOaipmhLocally.identifierList.setRepositoryObjectURIPrefix(ITEM_IDENTIFIER_PREFIX);
    TestTGOaipmhLocally.identifierList.setIdentifierField("textgridUri");
    TestTGOaipmhLocally.identifierList.setSearchResponseSize("100");
    System.out.println("Test for the verb \"ListIdentifiers\" with successful response");
    String p =
        this.request.getRequest("ListIdentifiers", "", "oai_dc", "", "2000-02-05", "", "junk");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListIdentifierWithResumptionTokenDC() throws ParseException {

    TestTGOaipmhLocally.identifierList.setFieldForRange(RANGE_FIELD);
    TestTGOaipmhLocally.identifierList.setIdentifierListFields(IDENTIFIER_LIST_FIELDS);
    TestTGOaipmhLocally.identifierList.setDateOfObjectCreation(CREATED);
    TestTGOaipmhLocally.identifierList.setRepositoryObjectURIPrefix(ITEM_IDENTIFIER_PREFIX);
    TestTGOaipmhLocally.identifierList.setIdentifierField("textgridUri");
    TestTGOaipmhLocally.identifierList.setSearchResponseSize("100");
    System.out.println("Test for the verb \"ListIdentifiers\" with successful response");
    String p = this.request.getRequest("ListIdentifiers", "", "oai_dc", "", "", "", "");
    System.out.println(p);
    String resToken = "";
    for (Map.Entry<String, Integer> entry : IdentifierListDelivererDC.getCursorCollector()
        .entrySet()) {
      resToken = entry.getKey();
    }
    System.out.println("HASH MAP BEFORE: ");
    System.out.println(IdentifierListDelivererDC.getCursorCollector());
    System.out.println(resToken);
    String p2 = this.request.getRequest("ListIdentifiers", "", "", "", "", "", resToken);
    System.out.println(p2);
    System.out.println("HASH MAP AFTER: ");
    System.out.println(IdentifierListDelivererDC.getCursorCollector());
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListIdentifierIDIOMInvalidResumptionToken() throws ParseException {

    System.out
        .println("Test for the verb \"ListIdentifiers\" in IDIOM with an invalid resumption token");
    String p = this.request.getRequest("ListIdentifiers", "", "", "", "", "",
        "45d8b524-b69e-44e8-81a9-a3634005a430");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testMissingVerbArgument() throws ParseException {

    System.out.println("Test for request with missing verb argument");
    String p = this.request.getRequest("", "", "", "", "", "", "");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }


  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListIdentifierIDIOMWithResumptionToken() throws ParseException {

    System.out.println("Test for the verb \"ListIdentifiers\" in IDIOM with successfull response");
    String p = this.request.getRequest("ListIdentifiers", "", OaipmhConstants.METADATA_IDIOM_PREFIX,
        "", "", "", "");
    System.out.println(p);
    String resToken = "";
    for (Map.Entry<String, Integer> entry : IdentifierListDelivererIdiom.getCursorCollector()
        .entrySet()) {
      resToken = entry.getKey();
    }
    String p2 = this.request.getRequest("ListIdentifiers", "", "", "", "", "", resToken);
    System.out.println(p2);
    System.out.println("HASH MAP AFTER: ");
    System.out.println(IdentifierListDelivererIdiom.getCursorCollector());
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListRecordSets() throws ParseException {

    TestTGOaipmhLocally.recordList.setContributors(CONTRIBUTOR_LIST);
    TestTGOaipmhLocally.recordList.setCoverages(COVERAGE_LIST);
    TestTGOaipmhLocally.recordList.setCreators(CREATOR_LIST);
    TestTGOaipmhLocally.recordList.setDates(DATE_LIST);
    TestTGOaipmhLocally.recordList.setDescriptions(DESCRIPTION_LIST);
    TestTGOaipmhLocally.recordList.setFormats(FORMAT_LIST);
    TestTGOaipmhLocally.recordList.setIdentifiers(IDENTIFIER_LIST);
    TestTGOaipmhLocally.recordList.setLanguages(LANGUAGE_LIST);
    TestTGOaipmhLocally.recordList.setPublishers(PUBLISHER_LIST);
    TestTGOaipmhLocally.recordList.setRelations(RELATIONS_LIST);
    TestTGOaipmhLocally.recordList.setRelationsForWork(RELATIONS_FOR_WORK_LIST);
    TestTGOaipmhLocally.recordList.setRights(RIGHTS_LIST);
    TestTGOaipmhLocally.recordList.setSources(SOURCE_LIST);
    TestTGOaipmhLocally.recordList.setSubjects(SUBJECT_LIST);
    TestTGOaipmhLocally.recordList.setTitles(TITLE_LIST);
    TestTGOaipmhLocally.recordList.setTypes(TYPE_LIST);
    TestTGOaipmhLocally.recordList.setFormatField(FORMAT);
    TestTGOaipmhLocally.recordList.setFormatToFilter(TextGridMimetypes.EDITION);
    TestTGOaipmhLocally.recordList.setDateOfObjectCreation(CREATED);
    TestTGOaipmhLocally.recordList.setRelationToFurtherMetadataObject(EDITION_ISEDITIONOF);
    TestTGOaipmhLocally.recordList.setRepositoryObjectURIPrefix(ITEM_IDENTIFIER_PREFIX);
    TestTGOaipmhLocally.recordList.setRangeField(CREATED);
    TestTGOaipmhLocally.recordList.setModifiedField(LAST_MODIFIED);
    TestTGOaipmhLocally.recordList.setIdentifierField("textgridUri");

    System.out.println("Test for the verb \"ListRecords\" with sets with successful response");
    // String p = this.request.getRequest("ListRecords", "", "oai_dc",
    // "project:TGPR-f89ad029-4eb2-ae5c-6028-5db876513128", "", "", "");
    String p = this.request.getRequest("ListRecords", "", "oai_dc", "", "", "", "");

    System.out.println(p);
    /*
     * String resToken = ""; for (Map.Entry<String, Integer> entry :
     * RecordListDelivererDC.cursorCollector.entrySet()) { resToken = entry.getKey(); String p2 =
     * this.request.getRequest("ListRecords", "", "", "", "", "", resToken); System.out.println(p2);
     * System.out.println("HASH MAP AFTER: ");
     * System.out.println(RecordListDelivererDC.cursorCollector);
     * System.out.println("-----------------------------------\n"); }
     */
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListRecordWithResumptionTokenForIDIOM() throws ParseException {

    System.out.println("Test for the verb \"ListRecords\" for IDIOM with successful response");
    String p = this.request.getRequest("ListRecords", "", OaipmhConstants.METADATA_IDIOM_PREFIX, "",
        "", "", "");
    System.out.println(p);
    String resToken = "";
    for (Map.Entry<String, Integer> entry : RecordListDelivererIdiom.getCursorCollector()
        .entrySet()) {
      resToken = entry.getKey();
      String p2 = this.request.getRequest("ListRecords", "", "", "", "", "", resToken);
      System.out.println(p2);
      System.out.println("HASH MAP AFTER: ");
      System.out.println(RecordListDelivererIdiom.getCursorCollector());
      System.out.println("-----------------------------------\n");
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListRecords() throws ParseException {

    String r =
        this.request.getRequest("ListRecords", "", "oai_dc", "", "2011-02-06T20:48:39", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListRecords2() throws ParseException {

    TestTGOaipmhLocally.recordList.setContributors(CONTRIBUTOR_LIST);
    TestTGOaipmhLocally.recordList.setCoverages(COVERAGE_LIST);
    TestTGOaipmhLocally.recordList.setCreators(CREATOR_LIST);
    TestTGOaipmhLocally.recordList.setDates(DATE_LIST);
    TestTGOaipmhLocally.recordList.setDescriptions(DESCRIPTION_LIST);
    TestTGOaipmhLocally.recordList.setFormats(FORMAT_LIST);
    TestTGOaipmhLocally.recordList.setIdentifiers(IDENTIFIER_LIST);
    TestTGOaipmhLocally.recordList.setLanguages(LANGUAGE_LIST);
    TestTGOaipmhLocally.recordList.setPublishers(PUBLISHER_LIST);
    TestTGOaipmhLocally.recordList.setRelations(RELATIONS_LIST);
    TestTGOaipmhLocally.recordList.setRelationsForWork(RELATIONS_FOR_WORK_LIST);
    TestTGOaipmhLocally.recordList.setRights(RIGHTS_LIST);
    TestTGOaipmhLocally.recordList.setSources(SOURCE_LIST);
    TestTGOaipmhLocally.recordList.setSubjects(SUBJECT_LIST);
    TestTGOaipmhLocally.recordList.setTitles(TITLE_LIST);
    TestTGOaipmhLocally.recordList.setTypes(TYPE_LIST);
    TestTGOaipmhLocally.recordList.setFormatField(FORMAT);
    TestTGOaipmhLocally.recordList.setFormatToFilter(TextGridMimetypes.EDITION);
    TestTGOaipmhLocally.recordList.setDateOfObjectCreation(CREATED);
    TestTGOaipmhLocally.recordList.setRelationToFurtherMetadataObject(EDITION_ISEDITIONOF);
    TestTGOaipmhLocally.recordList.setRepositoryObjectURIPrefix(ITEM_IDENTIFIER_PREFIX);
    TestTGOaipmhLocally.recordList.setRangeField(CREATED);
    TestTGOaipmhLocally.recordList.setModifiedField(CREATED);
    TestTGOaipmhLocally.recordList.setIdentifierField(URI);
    TestTGOaipmhLocally.recordList.setSearchResponseSize(100);
    String r = this.request.getRequest("ListRecords", "", "oai_dc", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListMetadataFormatsForIdentifier() throws ParseException {

    String r =
        this.request.getRequest("ListMetadataFormats", "textgrid:125x1.0", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListMetadataFormatsWithoutID() throws ParseException {

    String r = this.request.getRequest("ListMetadataFormats", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  @Ignore
  public void testListSetsTG() throws ParseException {

    TestTGOaipmhLocally.setListTextGrid.setFormatField("format");
    TestTGOaipmhLocally.setListTextGrid.setFormatToFilter(TextGridMimetypes.EDITION);
    TestTGOaipmhLocally.setListTextGrid.setIdentifierField("textgridUri");
    TestTGOaipmhLocally.setListTextGrid.setRepositoryObjectURIPrefix("textgrid:");
    String r = this.request.getRequest("ListSets", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testEmptyRequest() throws ParseException {

    TestTGOaipmhLocally.identifierList.setFieldForRange(RANGE_FIELD);
    TestTGOaipmhLocally.identifierList
        .setIdentifierListFields(IDENTIFIER_LIST_FIELDS);
    TestTGOaipmhLocally.identifierList.setDateOfObjectCreation(CREATED);
    TestTGOaipmhLocally.identifierList.setIdentifierField("textgridUri");
    TestTGOaipmhLocally.identifierList.setRepositoryObjectURIPrefix("textgrid:");
    TestTGOaipmhLocally.identifierList.setSearchResponseSize("100");
    String r = this.request.getRequest("", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

}

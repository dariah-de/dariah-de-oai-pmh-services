package info.textgrid.middleware.test.online.tg;

import java.io.IOException;
import java.util.concurrent.Callable;
import org.apache.cxf.jaxrs.client.Client;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Starts a new OAI-PMH thread.
 * </p>
 */

public class TGOaipmhResumptionTokenThread implements Callable<Boolean> {

  protected Client client;
  protected String verb;
  protected String set;
  protected String metadataPrefix;
  protected int numberOfPagesToTest;
  protected int recordsExpectedPerRequest;
  protected String threadName;

  /**
   * @param theClient
   * @param theVerb
   * @param theSet
   * @param theMetadataPrefix
   * @param numberOfPagesToTest
   * @param recordsExpectedPerRequest
   * @param theThreadName
   */
  public TGOaipmhResumptionTokenThread(Client theClient, String theVerb, String theSet,
      String theMetadataPrefix, int numberOfPagesToTest, int recordsExpectedPerRequest,
      String theThreadName) {
    this.client = theClient;
    this.verb = theVerb;
    this.set = theSet;
    this.metadataPrefix = theMetadataPrefix;
    this.numberOfPagesToTest = numberOfPagesToTest;
    this.recordsExpectedPerRequest = recordsExpectedPerRequest;
    this.threadName = theThreadName;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Thread#run()
   */
  @Override
  public Boolean call() throws IOException {
    OaipmhUtilitiesOnline.examineTGList(
        this.client,
        this.verb,
        this.set,
        this.metadataPrefix,
        this.numberOfPagesToTest,
        this.recordsExpectedPerRequest,
        this.threadName,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);
    return true;
  }

}

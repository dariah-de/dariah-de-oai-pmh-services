package info.textgrid.middleware.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.Test;
import info.textgrid.middleware.OaipmhUtilities;

/**
 *
 */
public class TestOaipmhUtilities {

  // **
  // FINALS
  // **

  public static final String UTC_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final DateTimeFormatter UTC_FORMATTER =
      DateTimeFormatter.ofPattern(UTC_FORMAT_STRING).withZone(ZoneId.of("UTC"));

  /**
   * 
   */
  @Test
  public void testOmitPrefixFromIdentifier() {

    String id;
    String exp;
    String res;

    // Check Handle IDs (dhrep).
    id = "hdl:21.11113/0000-000B-C8EF-7";
    exp = "21.11113/0000-000B-C8EF-7";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check tgrep set IDs (do not filter project: prefix!).
    id = "project:TGPR-2c283391-6ed5-70b4-2e5e-5501c856bca9";
    exp = "project:TGPR-2c283391-6ed5-70b4-2e5e-5501c856bca9";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check tgrep identifier.
    id = "textgrid:24gv8.0";
    exp = "24gv8.0";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check unknown prefix.
    id = "urgli:furgli";
    exp = "urgli:furgli";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check no prefix.
    id = "URGLARGLAUA";
    exp = "URGLARGLAUA";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check **: prefix.
    id = "**:21.T11991/0000-001C-2AA6-8";
    exp = "**:21.T11991/0000-001C-2AA6-8";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);

    // Check ** prefix.
    id = "**21.T11991/0000-001C-2AA6-8";
    exp = "**21.T11991/0000-001C-2AA6-8";
    res = OaipmhUtilities.omitPrefixFromIdentifier(id);
    assertEquals(res, exp);
  }

  /**
   * 
   */
  @Test
  public void testGetSetSpec() {

    String id = "hdl:21.T11991/0000-001B-4C00-E";
    String set;
    String exp;
    String setSpec;

    // Set is empty.
    set = "";
    exp = "hdl:21.T11991/0000-001B-4C00-E";
    setSpec = OaipmhUtilities.getSetSpec(set, "hdl:", id);
    assertEquals(setSpec, exp);

    // Set is prefix only.
    set = "hdl:";
    exp = "hdl:21.T11991/0000-001B-4C00-E";
    setSpec = OaipmhUtilities.getSetSpec(set, "hdl:", id);
    assertEquals(setSpec, exp);

    // Set is NOT empty.
    set = "project:53281-3452-34-5";
    exp = "project:53281-3452-34-5";
    setSpec = OaipmhUtilities.getSetSpec(set, "project:", id);
    assertEquals(setSpec, exp);
  }

  /**
   * 
   */
  @Test
  public void testGetTextGridBaseURI() {

    String uri;
    String baseURI;
    String uriExpected = "textgrid:12345";

    uri = "textgrid:12345.13";
    baseURI = OaipmhUtilities.getTextGridBaseURI(uri);
    assertEquals(baseURI, uriExpected);

    uri = "textgrid:12345.1";
    baseURI = OaipmhUtilities.getTextGridBaseURI(uri);
    assertEquals(baseURI, uriExpected);

    uri = "textgrid:12345";
    baseURI = OaipmhUtilities.getTextGridBaseURI(uri);
    assertEquals(baseURI, uriExpected);

    uri = "textgrid:12345.3847";
    baseURI = OaipmhUtilities.getTextGridBaseURI(uri);
    assertEquals(baseURI, uriExpected);
  }

  /**
   * 
   */
  @Test
  public void testGetTextGridRevisionURI() {

    String uriExpected;
    String uri;
    String revisionURI;

    uriExpected = "textgrid:12345.0";
    uri = "textgrid:12345";
    revisionURI = OaipmhUtilities.getTextGridRevisionURI(uri);
    assertEquals(revisionURI, uriExpected);

    uriExpected = "textgrid:12345.15";
    uri = "textgrid:12345.15";
    revisionURI = OaipmhUtilities.getTextGridRevisionURI(uri);
    assertEquals(revisionURI, uriExpected);
  }

  /**
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetUTCDateAsStringTG() throws DatatypeConfigurationException {

    String tgDateStamp = "2012-02-10T23:45:00.507+01:00";
    String expectedUTCDate = "2012-02-10T22:45:00Z";
    String utcDate = OaipmhUtilities.getUTCDateAsString(tgDateStamp);
    assertEquals(utcDate, expectedUTCDate);
  }

  /**
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetUTCDateAsStringDH() throws DatatypeConfigurationException {

    String tgDateStamp = "2012-02-10T23:45:00.507";
    // Maybe not a real test, because we should test with an expected string, cannot be done,
    // because we have different local time zones on server and develop machines.
    String expectedUTCDate = OaipmhUtilities.UTC_FORMATTER
        .format(LocalDateTime.parse(tgDateStamp).atZone(ZoneId.of("CET")).toInstant());
    String utcDate = OaipmhUtilities.getUTCDateAsString(tgDateStamp);
    assertEquals(utcDate, expectedUTCDate);
  }

  /**
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetUTCDateAsGregorian() throws DatatypeConfigurationException {

    String tgDateStamp = "2022-07-22T14:28:13.139+02:00";
    XMLGregorianCalendar expectedCal =
        DatatypeFactory.newInstance().newXMLGregorianCalendar("2022-07-22T12:28:13Z");
    XMLGregorianCalendar utcCal = OaipmhUtilities.getUTCDateAsGregorian(tgDateStamp);
    assertEquals(expectedCal, utcCal);
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  @Test
  public void testIsCorrectFromUntilDate() throws DatatypeConfigurationException, ParseException {

    String date;

    date = "1970-12-10T18:30:00";
    assertTrue(OaipmhUtilities.isParsableDate(date));

    date = "1970-12-10T22:30:97";
    assertTrue(OaipmhUtilities.isParsableDate(date));

    date = "1970-12-10";
    assertTrue(OaipmhUtilities.isParsableDate(date));

    date = "1970-53-65";
    assertTrue(OaipmhUtilities.isParsableDate(date));

    date = "1970-12-10T18:30";
    assertFalse(OaipmhUtilities.isParsableDate(date));

    date = "1970-12-10T18";
    assertFalse(OaipmhUtilities.isParsableDate(date));

    date = "1970-12";
    assertFalse(OaipmhUtilities.isParsableDate(date));

    date = "1970";
    assertFalse(OaipmhUtilities.isParsableDate(date));

    date = "kakki";
    assertFalse(OaipmhUtilities.isParsableDate(date));

    date = "kakki-fakki";
    assertFalse(OaipmhUtilities.isParsableDate(date));
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  @Test
  public void testGetCurrentUTCDateAsString()
      throws DatatypeConfigurationException, ParseException {
    // Nothing to compare to at the moment... current date would be a different one if created here
    // as comparison!
    OaipmhUtilities.getCurrentUTCDateAsString();
  }

  /**
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  @Test
  public void testGetCurrentUTCDateAsGregorian()
      throws DatatypeConfigurationException, ParseException {
    // Same here! :-)
    OaipmhUtilities.getCurrentUTCDateAsGregorian();
  }

}

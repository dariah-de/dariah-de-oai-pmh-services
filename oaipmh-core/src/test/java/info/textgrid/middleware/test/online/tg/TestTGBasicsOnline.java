package info.textgrid.middleware.test.online.tg;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.ws.rs.core.Response;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Some basic online tests for the TextGrid OAIMPH service.
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES in class <b>OAIPMHUtilitiesOnline</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
 @Ignore
public class TestTGBasicsOnline {

  // **
  // STATICS
  // **

  // Some JAXRS things.
  private static String oaipmhEndpoint;
  static Client oaipmhWebClient;

  // Properties
  private static String expectedListSets;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get properties.
    Properties p =
        OaipmhUtilitiesOnline.getPropertiesFromFile(OaipmhUtilitiesOnline.PROPERTIES_FILE);

    // Set properties.
    oaipmhEndpoint = p.getProperty("oaipmhEndpoint");

    expectedListSets = p.getProperty("expectedListSets");

    // Get OAI-PMH REST endpoint and HTTP client.
    System.out.println("Getting OAI-PMH HTTP client --> " + oaipmhEndpoint + " <--");

    // Get web client from endpoint.
    oaipmhWebClient = OaipmhUtilitiesOnline.getOAIPMHWEebClient(oaipmhEndpoint);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   */
  @Test
  public void testGetVersion() throws IOException {

    String shouldStartWith = "oaipmh-core";

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testGetVersion()");

    Response httpResponse = OaipmhUtilitiesOnline.getVersionHttpResponse(oaipmhWebClient);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.startsWith(shouldStartWith)) {
      String message = "response should start with '" + shouldStartWith + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testRootUrl() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testRootUrl()");

    String shouldContain = TestTGBasicsOnline.oaipmhEndpoint;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient, "/");
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(shouldContain)) {
      String message = "[" + status + "] response should contain '" + shouldContain + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testIdentify() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testIdentify()");

    String shouldContain = TestTGBasicsOnline.oaipmhEndpoint;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        "verb=" + OaipmhUtilitiesOnline.VERB_IDENTIFY);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(shouldContain)) {
      String message = "[" + status + "] response should contain '" + shouldContain + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListMetadataFormats() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListMetadataFormats()");

    String shouldContainDC = OaipmhUtilitiesOnline.OAI_DC_PREFIX;
    String shouldContainIDIOM = OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX;
    String shouldContainDATACITE = OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX;

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        "verb=" + OaipmhUtilitiesOnline.VERB_LIST_METADATA_FORMATS);

    int status = httpResponse.getStatus();
    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(shouldContainDC)
        || !response.contains(shouldContainIDIOM) || !response.contains(shouldContainDATACITE)) {
      String message = "[" + status + "] response should contain '" + shouldContainDC + "' and '"
          + shouldContainIDIOM + "' and '" + shouldContainDATACITE + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListSets() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListSets()");

    Response httpResponse = OaipmhUtilitiesOnline.getOAIHttpResponse(oaipmhWebClient,
        "verb=" + OaipmhUtilitiesOnline.VERB_LIST_SETS);
    int status = httpResponse.getStatus();

    String response = IOUtils.readStringFromStream((InputStream) httpResponse.getEntity());

    if (status != HttpStatus.SC_OK || !response.contains(expectedListSets)) {
      String message = "[" + status + "] response should contain '" + expectedListSets + "'";
      assertTrue(message, false);
    }

    System.out.println("\tresponse: " + response);
    System.out.println(OaipmhUtilitiesOnline.OK);
  }

}

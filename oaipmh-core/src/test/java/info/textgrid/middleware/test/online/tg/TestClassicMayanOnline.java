package info.textgrid.middleware.test.online.tg;

import java.io.IOException;
import java.text.ParseException;
import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import org.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import info.textgrid.middleware.IdiomImages;

/**
 *
 */
@Ignore
public class TestClassicMayanOnline {

  // TODO Check ignored tests!! Do they make any sense??

  /**
   * @throws JSONException
   * @throws IOException
   * @throws SAXException
   * @throws ParseException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  @Test
  @Ignore
  public void testGetRecord() throws JSONException, IOException, SAXException, ParseException,
      TransformerFactoryConfigurationError, TransformerException {

    IdiomImages imageList = new IdiomImages("idiomTgcrudEndpoint", "idiomRbacSessionID");
    Document document;

    try {
      document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      JAXB.marshal(imageList.getRecordById("11480"), new DOMResult(document));
      imageList.printXML(document);
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * @throws JSONException
   * @throws IOException
   * @throws ParseException
   * @throws SAXException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  @Test
  @Ignore
  public void testOAIPMHRequest() throws JSONException, IOException, ParseException, SAXException,
      TransformerFactoryConfigurationError, TransformerException {

    IdiomImages imageList = new IdiomImages("idiomTgcrudEndpoint", "idiomRbacSessionID");

    imageList.setKindID("1");
    imageList.setPerPage("100");
    imageList.setPageNumber("1");
  }

}

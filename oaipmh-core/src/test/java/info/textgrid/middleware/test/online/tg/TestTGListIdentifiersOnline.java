package info.textgrid.middleware.test.online.tg;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.cxf.jaxrs.client.Client;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.test.online.OaipmhUtilitiesOnline;

/**
 * <p>
 * Some online tests for the TextGrid OAIMPH service --> verb=ListIdentifiers <--
 * </p>
 * 
 * <p>
 * Please set PROPERTIES_FILE and TEST_ALL_PAGES in class <b>OAIPMHUtilitiesOnline</b>!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
@Ignore
public class TestTGListIdentifiersOnline {

  // **
  // STATICS
  // **

  // Some JAXRS things.
  private static String oaipmhEndpoint;
  static Client oaipmhWebClient;

  // Properties

  private static String checkListIdentifiersSet;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get properties.
    Properties p =
        OaipmhUtilitiesOnline.getPropertiesFromFile(OaipmhUtilitiesOnline.PROPERTIES_FILE);

    // Set properties.
    oaipmhEndpoint = p.getProperty("oaipmhEndpoint");

    checkListIdentifiersSet = p.getProperty("checkListIdentifiersSet");

    // Get web client from endpoint.
    oaipmhWebClient = OaipmhUtilitiesOnline.getOAIPMHWEebClient(oaipmhEndpoint);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersDCMorePages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListIdentifiersDCMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        checkListIdentifiersSet,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        30, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test(expected = IOException.class)
  public void testListIdentifiersDCMorePagesAndMetadataFormatWithRestok() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING
        + "testListIdentifiersDCMorePagesAndMetadataFormatWithRestok()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        checkListIdentifiersSet,
        OaipmhUtilitiesOnline.OAI_DC_PREFIX,
        30, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.METADATA_FORMAT_WITH_RESTOK, OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersIdiomMETSAllPages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListIdentifiersIdiomMETSAllPages()");

    if (OaipmhUtilitiesOnline.TEST_ALL_PAGES) {
      OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
          OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
          OaipmhUtilitiesOnline.NO_SET,
          OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
          0, 30,
          OaipmhUtilitiesOnline.NO_THREAD_NAME,
          OaipmhUtilitiesOnline.NO_FROM,
          OaipmhUtilitiesOnline.NO_UNTIL,
          OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
          OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

      System.out.println(OaipmhUtilitiesOnline.OK);
    } else {
      System.out.println("...skipping");
    }
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersIdiomMETSMorePages() throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING + "testListIdentifiersIdiomMETSMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        6, 30,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test(expected = IOException.class)
  public void testListIdentifiersIdiomMETSSomePagesAndMetadataFormatWithRestok()
      throws IOException {

    System.out.println(OaipmhUtilitiesOnline.TESTING
        + "testListIdentifiersIdiomMETSSomePagesAndMetadataFormatWithRestok()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
        3, 30,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.METADATA_FORMAT_WITH_RESTOK, OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testListIdentifiersDataciteMorePages() throws IOException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testListIdentifiersDataciteMorePages()");

    OaipmhUtilitiesOnline.examineTGList(oaipmhWebClient,
        OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
        OaipmhUtilitiesOnline.NO_SET,
        OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
        22, 100,
        OaipmhUtilitiesOnline.NO_THREAD_NAME,
        OaipmhUtilitiesOnline.NO_FROM,
        OaipmhUtilitiesOnline.NO_UNTIL,
        OaipmhUtilitiesOnline.NO_METADATA_FORMAT_WITH_RESTOK,
        OaipmhUtilitiesOnline.NO_ERROR_EXPECTED);

    System.out.println(OaipmhUtilitiesOnline.OK);
  }

  /**
   * @throws IOException
   * @throws ExecutionException
   * @throws InterruptedException
   */
  @Test
  public void testListIdentifiersConcurrentlyIdiomMETSMorePages()
      throws IOException, InterruptedException, ExecutionException {

    System.out.println(
        OaipmhUtilitiesOnline.TESTING + "testListIdentifiersConcurrentlyIdiomMETSMorePages()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor
        .submit(new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            3, 30,
            "C1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            5, 30,
            "C2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            8, 30,
            "C2"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [C1]=" + f1.get() + ", [C2]=" + f2.get()
        + ", [C3]=" + f3.get());
  }

  /**
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListIdentifiersDC()
      throws InterruptedException, ExecutionException {

    System.out
        .println(OaipmhUtilitiesOnline.TESTING + "testRestokConcurrentlyListIdentifiersDC()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            36, 100,
            "A1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            27, 100,
            "A2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            19, 100,
            "A3"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [A1]=" + f1.get() + ", [A2]=" + f2.get()
        + ", [A3]=" + f3.get());
  }

  /**
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListIdentifiersIdiomMETS()
      throws InterruptedException, ExecutionException {

    System.out
        .println(
            OaipmhUtilitiesOnline.TESTING + "testRestokConcurrentlyListIdentifiersIdiom()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            13, 30,
            "A1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            35, 30,
            "A2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            11, 30,
            "A3"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [A1]=" + f1.get() + ", [A2]=" + f2.get()
        + ", [A3]=" + f3.get());
  }

  /**
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListIdentifiersDatacite()
      throws InterruptedException, ExecutionException {

    System.out
        .println(
            OaipmhUtilitiesOnline.TESTING + "testRestokConcurrentlyListIdentifiersDatacite()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
            13, 100,
            "A1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
            20, 100,
            "A2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DATACITE_PREFIX,
            8, 100,
            "A3"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [A1]=" + f1.get() + ", [A2]=" + f2.get()
        + ", [A3]=" + f3.get());
  }

  /**
   * @throws InterruptedException
   * @throws ExecutionException
   */
  @Test
  public void testRestokConcurrentlyListIdentifiersDCAndIdiomMETS()
      throws InterruptedException, ExecutionException {

    System.out.println(
        OaipmhUtilitiesOnline.TESTING
            + "testRestokConcurrentlyListIdentifiersDCAndIdiomMets()");

    ExecutorService executor = Executors.newFixedThreadPool(3);

    Future<Boolean> f1 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            53, 100,
            "DC1"));
    Future<Boolean> f2 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_DC_PREFIX,
            28, 100,
            "DC2"));
    Future<Boolean> f3 = executor.submit(
        new TGOaipmhResumptionTokenThread(oaipmhWebClient,
            OaipmhUtilitiesOnline.VERB_LIST_IDENTIFIERS,
            OaipmhUtilitiesOnline.NO_SET,
            OaipmhUtilitiesOnline.OAI_IDIOMMETS_PREFIX,
            10, 30,
            "IDIOM"));

    executor.shutdown();

    System.out.println(OaipmhUtilitiesOnline.OK + ": [DC1]=" + f1.get() + ", [DC2]=" + f2.get()
        + ", [IDIOM]=" + f3.get());
  }

}

package info.textgrid.middleware.test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.DublinCoreBuilder;
import info.textgrid.middleware.IdentifierListDelivererDC;
import info.textgrid.middleware.IdentifierListDelivererDatacite;
import info.textgrid.middleware.IdentifierListDelivererIdiom;
import info.textgrid.middleware.MetadataFormatListDelivererDH;
import info.textgrid.middleware.MetadataFormatListDelivererInterface;
import info.textgrid.middleware.OaipmhElasticSearchClient;
import info.textgrid.middleware.OaipmhImpl;
import info.textgrid.middleware.OaipmhUtilities;
import info.textgrid.middleware.RecordDelivererDC;
import info.textgrid.middleware.RecordDelivererDatacite;
import info.textgrid.middleware.RecordDelivererIdiom;
import info.textgrid.middleware.RecordListDelivererDC;
import info.textgrid.middleware.RecordListDelivererDatacite;
import info.textgrid.middleware.RecordListDelivererIdiom;
import info.textgrid.middleware.RepIdentification;
import info.textgrid.middleware.SetListDeliverer;
import info.textgrid.middleware.common.TextGridMimetypes;

/**
 * TODO Add documentation how to use this class, use SSH tunnel?
 */
@Ignore
public class TestDHOaipmhLocally {

  // **
  // FINALS
  // **

  // TODO Run tests with real configuration, we changed a lot currently!
  public static final String TEST_DARIAH_ITEM_IDENTIFIER_PREFIX = "hdl:";
  public static final String TEST_DARIAH_COLLECTIONREGISTRY_PREFIX = "dariah:collection:";
  public static final String TEST_DARIAH_COLLECTION_MIMETYPE =
      "text/vnd.dariah.dhrep.collection+turtle";

  // DARIAH Metadata Fields for OAI-PMH Request (Mapping to DC)
  public static final String TEST_DARIAH_CREATED = "descriptiveMetadata.dc:date";
  public static final String TEST_DARIAH_FORMAT = "descriptiveMetadata.dc:format";
  public static final String TEST_DARIAH_IDENTIFIER = "administrativeMetadata.dcterms:identifier";
  public static final String[] TEST_DARIAH_FIELDS =
      {"descriptiveMetadata.dc:contributor", "descriptiveMetadata.dc:coverage",
          "administrativeMetadata.dcterms:created", "administrativeMetadata.dcterms:modified",
          "descriptiveMetadata.dc:description", "descriptiveMetadata.dc:format",
          "descriptiveMetadata.dc:identifier", "descriptiveMetadata.dc:language",
          "descriptiveMetadata.dc:publisher", "descriptiveMetadata.dc:relation",
          "descriptiveMetadata.dc:rights", "descriptiveMetadata.:source",
          "descriptiveMetadata.dc:subject", "descriptiveMetadata.dc:title",
          "descriptiveMetadata.dc:type", "administrativeMetadata.dcterms:identifier"};

  // String Arrays to define which TextGrid fields belongs to the regarding DC fields
  public static final String[] TEST_DARIAH_CONTRIBUTOR_LIST =
      {"descriptiveMetadata.dc:contributor"};
  public static final String[] TEST_DARIAH_COVERAGE_LIST = {"descriptiveMetadata.dc:coverage"};
  public static final String[] TEST_DARIAH_CREATOR_LIST = {"descriptiveMetadata.dc:cretor"};
  public static final String[] TEST_DARIAH_DATE_LIST = {"administrativeMetadata.dcterms:modified"};
  public static final String[] TEST_DARIAH_DESCRIPTION_LIST =
      {"descriptiveMetadata.dc:description"};
  public static final String[] TEST_DARIAH_FORMAT_LIST = {"descriptiveMetadata.dc:format"};
  public static final String[] TEST_DARIAH_IDENTIFIER_LIST = {"descriptiveMetadata.dc:identifier"};
  public static final String[] TEST_DARIAH_LANGUAGE_LIST = {"descriptiveMetadata.dc:language"};
  public static final String[] TEST_DARIAH_PUBLISHER_LIST = {"descriptiveMetadata.dc:publisher"};
  public static final String[] TEST_DARIAH_RELATIONS_LIST = {"descriptiveMetadata.dc:relation"};
  public static final String[] TEST_DARIAH_RIGHTS_LIST = {"descriptiveMetadata.dc:rights"};
  public static final String[] TEST_DARIAH_SOURCE_LIST = {"descriptiveMetadata.dc:source"};
  public static final String[] TEST_DARIAH_SUBJECT_LIST = {"descriptiveMetadata.dc:subject"};
  public static final String[] TEST_DARIAH_TITLE_LIST = {"descriptiveMetadata.dc:title"};
  public static final String[] TEST_DARIAH_TYPE_LIST = {"descriptiveMetadata.dc:type"};
  public static final String[] TEST_DARIAH_IDENTIFIER_LIST_FIELDS =
      {"administrativeMetadata.dcterms:identifier", "administrativeMetadata.dcterms:modified",
          "descriptiveMetadata.dc:identifier", "descriptiveMetadata.dc:date",
          "descriptiveMetadata.dc:format"};
  public static final String TEST_DARIAH_RANGE_FIELD = "administrativeMetadata.dcterms:modified";
  public static final String TEST_DARIAH_MODIFIED_FIELD = "administrativeMetadata.dcterms:modified";

  // **
  // STATICS
  //

  public static OaipmhElasticSearchClient oaiEsClient;

  private static RecordDelivererDC record;
  private static RecordListDelivererDC recordList;
  private static IdentifierListDelivererDC identifierList;
  private static MetadataFormatListDelivererInterface metadataFormatList =
      new MetadataFormatListDelivererDH();
  private static SetListDeliverer setListDARIAH = new SetListDeliverer(false, true);

  // private static OpenAireRecord openAireRecord;
  private static RecordDelivererDatacite recordDATACITE;
  // private static OpenAireRecordList openAireRecordList;
  private static RecordListDelivererDatacite recordListDATACITE;
  // private static OpenAireIdentifierList openAireIdentifierList;
  private static IdentifierListDelivererDatacite identifierListDATACITE;
  // private static IdiomImages idiomImages;
  private RepIdentification rep = new RepIdentification("DARIAH-DE Repository",
      "https://trep.de.dariah.eu", "funk@sub.uni-goettingen.de", "2011-06-11T02:32:40Z");

  private static RecordDelivererIdiom recordIDIOM;
  private static RecordListDelivererIdiom recordListIDIOM;
  private static IdentifierListDelivererIdiom identifierListIDIOM =
      new IdentifierListDelivererIdiom(true, false);

  private OaipmhImpl request =
      new OaipmhImpl(this.rep,
          TestDHOaipmhLocally.record,
          TestDHOaipmhLocally.recordIDIOM,
          TestDHOaipmhLocally.recordDATACITE,
          TestDHOaipmhLocally.recordList,
          TestDHOaipmhLocally.recordListIDIOM,
          TestDHOaipmhLocally.recordListDATACITE,
          TestDHOaipmhLocally.metadataFormatList,
          TestDHOaipmhLocally.setListDARIAH,
          TestDHOaipmhLocally.identifierList,
          TestDHOaipmhLocally.identifierListIDIOM,
          TestDHOaipmhLocally.identifierListDATACITE);

  OaipmhUtilities settings = new OaipmhUtilities();

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUp() throws Exception {
    int[] ports = new int[] {9202};
    oaiEsClient = new OaipmhElasticSearchClient("localhost", ports);
    oaiEsClient.setEsIndex("dariah-public");

    record = new RecordDelivererDC(false, true);
    record.setOaiEsClient(oaiEsClient);
    record.setWorkFields(TEST_DARIAH_FIELDS);
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestIdentify() throws ParseException {
    System.out.println("Test for the verb \"Identify\" with succesfull response");
    String r = this.request.getRequest("Identify", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testGetRequestGetRecordDariah() throws ParseException {

    TestDHOaipmhLocally.record.setContributor(TEST_DARIAH_CONTRIBUTOR_LIST);
    TestDHOaipmhLocally.record.setCoverage(TEST_DARIAH_COVERAGE_LIST);
    TestDHOaipmhLocally.record.setCreator(TEST_DARIAH_CREATOR_LIST);
    TestDHOaipmhLocally.record.setDates(TEST_DARIAH_DATE_LIST);
    TestDHOaipmhLocally.record.setDescriptions(TEST_DARIAH_DESCRIPTION_LIST);
    TestDHOaipmhLocally.record.setFormats(TEST_DARIAH_FORMAT_LIST);
    TestDHOaipmhLocally.record.setIdentifiers(TEST_DARIAH_IDENTIFIER_LIST);
    TestDHOaipmhLocally.record.setLanguages(TEST_DARIAH_LANGUAGE_LIST);
    TestDHOaipmhLocally.record.setPublishers(TEST_DARIAH_PUBLISHER_LIST);
    TestDHOaipmhLocally.record.setRelations(TEST_DARIAH_RELATIONS_LIST);
    TestDHOaipmhLocally.record.setRights(TEST_DARIAH_RIGHTS_LIST);
    TestDHOaipmhLocally.record.setSources(TEST_DARIAH_SOURCE_LIST);
    TestDHOaipmhLocally.record.setSubjects(TEST_DARIAH_SUBJECT_LIST);
    TestDHOaipmhLocally.record.setTitles(TEST_DARIAH_TITLE_LIST);
    TestDHOaipmhLocally.record.setTypes(TEST_DARIAH_TYPE_LIST);
    TestDHOaipmhLocally.record.setFields(TEST_DARIAH_FIELDS);
    TestDHOaipmhLocally.record.setFormatField(TEST_DARIAH_FORMAT);
    TestDHOaipmhLocally.record.setFormatToFilter("metadata");
    TestDHOaipmhLocally.record
        .setDateOfObjectCreation(TEST_DARIAH_MODIFIED_FIELD);
    TestDHOaipmhLocally.record
        .setRepositoryObjectURIPrefix(TEST_DARIAH_ITEM_IDENTIFIER_PREFIX);
    TestDHOaipmhLocally.record.setIdentifierField("administrativeMetadata.dcterms:identifier");

    System.out.println("Test for the verb \"GetRecord\" for DARIAH with succesfull response");
    String p = this.request.getRequest("GetRecord", "hdl:21.T11991/0000-0005-E1AA-D", "oai_dc", "",
        "", "", "");
    System.out.println(p);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListRecordsDariah() throws ParseException {

    TestDHOaipmhLocally.recordList.setContributors(TEST_DARIAH_CONTRIBUTOR_LIST);
    TestDHOaipmhLocally.recordList.setCoverages(TEST_DARIAH_COVERAGE_LIST);
    TestDHOaipmhLocally.recordList.setCreators(TEST_DARIAH_CREATOR_LIST);
    TestDHOaipmhLocally.recordList.setDates(TEST_DARIAH_DATE_LIST);
    TestDHOaipmhLocally.recordList.setDescriptions(TEST_DARIAH_DESCRIPTION_LIST);
    TestDHOaipmhLocally.recordList.setFormats(TEST_DARIAH_FORMAT_LIST);
    TestDHOaipmhLocally.recordList.setIdentifiers(TEST_DARIAH_IDENTIFIER_LIST);
    TestDHOaipmhLocally.recordList.setLanguages(TEST_DARIAH_LANGUAGE_LIST);
    TestDHOaipmhLocally.recordList.setPublishers(TEST_DARIAH_PUBLISHER_LIST);
    TestDHOaipmhLocally.recordList.setRelations(TEST_DARIAH_RELATIONS_LIST);
    TestDHOaipmhLocally.recordList.setRights(TEST_DARIAH_RIGHTS_LIST);
    TestDHOaipmhLocally.recordList.setSources(TEST_DARIAH_SOURCE_LIST);
    TestDHOaipmhLocally.recordList.setSubjects(TEST_DARIAH_SUBJECT_LIST);
    TestDHOaipmhLocally.recordList.setTitles(TEST_DARIAH_TITLE_LIST);
    TestDHOaipmhLocally.recordList.setTypes(TEST_DARIAH_TYPE_LIST);
    TestDHOaipmhLocally.recordList.setFormatField(TEST_DARIAH_FORMAT);
    TestDHOaipmhLocally.recordList.setDateOfObjectCreation(TEST_DARIAH_CREATED);
    TestDHOaipmhLocally.recordList.setRangeField(TEST_DARIAH_MODIFIED_FIELD);
    TestDHOaipmhLocally.recordList
        .setDateOfObjectCreation(TEST_DARIAH_RANGE_FIELD);
    TestDHOaipmhLocally.recordList.setIdentifierField(TEST_DARIAH_IDENTIFIER);
    TestDHOaipmhLocally.recordList.setFormatToFilter(TextGridMimetypes.DARIAH_COLLECTION);
    TestDHOaipmhLocally.recordList.setModifiedField(TEST_DARIAH_MODIFIED_FIELD);
    TestDHOaipmhLocally.recordList
        .setRelationToFurtherMetadataObject("descriptiveMetadata.dc:relation");
    TestDHOaipmhLocally.recordList.setSearchResponseSize(100);

    String r = this.request.getRequest("ListRecords", "", "oai_dc", "21.T11991/0000-0003-718E-E",
        "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListSetsDARIAH() throws ParseException {

    TestDHOaipmhLocally.setListDARIAH.setFormatField(TEST_DARIAH_FORMAT);
    TestDHOaipmhLocally.setListDARIAH
        .setFormatToFilter(TEST_DARIAH_COLLECTION_MIMETYPE);
    TestDHOaipmhLocally.setListDARIAH.setIdentifierField(TEST_DARIAH_IDENTIFIER);
    TestDHOaipmhLocally.setListDARIAH
        .setRepositoryObjectURIPrefix(TEST_DARIAH_ITEM_IDENTIFIER_PREFIX);
    TestDHOaipmhLocally.setListDARIAH
        .setIdentifierField("administrativeMetadata.dcterms:identifier");
    TestDHOaipmhLocally.setListDARIAH
        .setSpecFieldPrefix(TEST_DARIAH_COLLECTIONREGISTRY_PREFIX);
    String r = this.request.getRequest("ListSets", "", "", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testListIdentifiersDARIAH() throws ParseException {

    TestDHOaipmhLocally.identifierList.setFieldForRange(TEST_DARIAH_RANGE_FIELD);
    TestDHOaipmhLocally.identifierList.setIdentifierListFields(TEST_DARIAH_IDENTIFIER_LIST_FIELDS);
    TestDHOaipmhLocally.identifierList
        .setDateOfObjectCreation("administrativeMetadata.dcterms:modified");
    TestDHOaipmhLocally.identifierList
        .setRepositoryObjectURIPrefix(TEST_DARIAH_ITEM_IDENTIFIER_PREFIX);
    TestDHOaipmhLocally.identifierList
        .setIdentifierField("administrativeMetadata.dcterms:identifier");
    TestDHOaipmhLocally.identifierList.setSearchResponseSize("100");
    String r = this.request.getRequest("ListIdentifiers", "", "oai_dc", "", "", "", "");
    System.out.println(r);
    System.out.println("-----------------------------------\n");
  }

  /**
   * 
   */
  @Test
  public void testGetArrayFromESIndex() {

    String jsonAsString =
        "{\"administrativeMetadata\":{\"dcterms:identifier\":\"hdl:21.T11991/0000-001B-4C02-C\",\"dcterms:modified\":\"2021-06-08T11:03:37.448\"},\"descriptiveMetadata\":{\"dc:creator\":\"fu\",\"dc:format\":\"text/vnd.dariah.dhrep.collection+turtle\",\"dc:rights\":\"free\",\"dc:title\":\"First ES6 Tests for DH-rep using Publikator\",\"dc:identifier\":[\"hdl:21.T11991/0000-001B-4C02-C\",\"doi:10.20375/0000-001B-4C02-C\"]}}";

    JSONObject json = new JSONObject(jsonAsString);
    // System.out.println(json);
    DublinCoreBuilder result = new DublinCoreBuilder();
    List<String> identifier = new ArrayList<String>();
    System.out.println(OaipmhUtilities.firstEntryFieldLoader(json, TEST_DARIAH_IDENTIFIER));
    identifier = OaipmhUtilities.listFieldLoader(json, "descriptiveMetadata.dc:identifier");
    for (String id : identifier) {
      System.out.println(id);
    }

    result.setIdentifier(identifier);
  }

  /**
   * 
   */
  @Test
  public void testGetArrayFromESIndexForListRecords() {

    String jsonAsString = "{\r\n"
        + "        \"administrativeMetadata\": {\r\n"
        + "            \"dcterms:created\": \"2021-06-16T17:15:48.689\",\r\n"
        + "            \"dcterms:creator\": \"StefanFunk@dariah.eu\",\r\n"
        + "            \"dcterms:extent\": 154064,\r\n"
        + "            \"dcterms:identifier\": \"hdl:21.T11991/0000-001B-4D41-4\",\r\n"
        + "            \"dcterms:modified\": \"2021-06-16T17:15:48.689\"\r\n"
        + "        },\r\n"
        + "        \"descriptiveMetadata\": {\r\n"
        + "            \"dc:creator\": \"fu\",\r\n"
        + "            \"dc:description\": \"just some ictures\",\r\n"
        + "            \"dc:format\": \"image/jpeg\",\r\n"
        + "            \"dc:identifier\": [\r\n"
        + "                \"hdl:21.T11991/0000-001B-4D41-4\",\r\n"
        + "                \"doi:10.20375/0000-001B-4D41-4\"\r\n"
        + "            ],\r\n"
        + "            \"dc:relation\": \"hdl:21.T11991/0000-001B-4D28-1\",\r\n"
        + "            \"dc:rights\": \"free\",\r\n"
        + "            \"dc:title\": \"2017-07-12-Waffel copy 2.jpeg\"\r\n"
        + "        },\r\n"
        + "        \"technicalMetadata\": {\r\n"
        + "            \"externalIdentifier\": \"fmt/43\",\r\n"
        + "            \"externalIdentifierType\": \"puid\",\r\n"
        + "            \"format\": \"JPEG File Interchange Format\",\r\n"
        + "            \"mimetype\": \"image/jpeg\",\r\n"
        + "            \"valid\": \"true\",\r\n"
        + "            \"version\": \"1.01\",\r\n"
        + "            \"well-formed\": \"true\"\r\n"
        + "        }\r\n"
        + "    }";

    JSONObject json = new JSONObject(jsonAsString);
    DublinCoreBuilder result = new DublinCoreBuilder();
    List<String> identifier = new ArrayList<String>();
    System.out.println(OaipmhUtilities.firstEntryFieldLoader(json, TEST_DARIAH_IDENTIFIER));
    identifier = OaipmhUtilities.listFieldLoader(json, "descriptiveMetadata.dc:identifier");
    for (String id : identifier) {
      System.out.println(id);
    }

    result.setIdentifier(identifier);

    System.out.println(result);
  }

}

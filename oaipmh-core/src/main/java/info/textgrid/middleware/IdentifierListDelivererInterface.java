package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import info.textgrid.middleware.oaipmh.ListIdentifiersType;

/**
 *
 */
public interface IdentifierListDelivererInterface {

  /**
   * @param from Start value for the range query
   * @param to End value to the range query
   * @param set
   * @param resumptionToken
   * @return
   * @throws ParseException
   * @throws IOException
   */
  public ListIdentifiersType processIdentifierList(String from, String to, String set,
      String resumptionToken) throws ParseException, IOException;

}

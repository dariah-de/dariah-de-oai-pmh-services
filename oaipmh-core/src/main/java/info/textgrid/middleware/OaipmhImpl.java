package info.textgrid.middleware;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import org.classicmayan.tools.ConedaKor2TextGridRep;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.IdentifyType;
import info.textgrid.middleware.oaipmh.ListIdentifiersType;
import info.textgrid.middleware.oaipmh.ListMetadataFormatsType;
import info.textgrid.middleware.oaipmh.ListRecordsType;
import info.textgrid.middleware.oaipmh.ListSetsType;
import info.textgrid.middleware.oaipmh.OAIPMHtype;
import info.textgrid.middleware.oaipmh.ObjectFactory;
import info.textgrid.middleware.oaipmh.RequestType;
import info.textgrid.middleware.oaipmh.VerbType;

/**
 * <p>
 * This class takes all possible arguments of an OAI-PMH request and proceeds depended on the given
 * arguments. It also deals as response handler.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class OaipmhImpl implements OaipmhProducer {

  // **
  // PRIVATE STATIC FINAL
  // **

  private static final String ERR_VAL_IDENTIFIER = "identifier";
  private static final String ERR_VAL_FROM = "from";
  private static final String ERR_VAL_MDPREFIX = "metadataPrefix";
  private static final String ERR_VAL_RESTOK = "resumptionToken";
  private static final String ERR_VAL_UNTIL = "until";
  private static final String ERR_VAL_SET = "set";
  private static final String ILLEGAL_OAI_VERB = "Illegal OAI verb";
  private static final String ILLEGAL_ARG = "The request includes illegal arguments: ";
  private static final String ILLEGAL_RESTOK =
      "The value of the resumptionToken is invalid or expired.";
  private static final String ILLEGAL_OTHER = "Something else seems to be illegal here.";
  private static final String ILLEGAL_ID =
      "The value of the identifier is unknown or illegal in this repository: ";
  private static final String REQUIRED_ARG =
      "The request includes illegal arguments or is missing required arguments: ";

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(OaipmhImpl.class.getName());
  private static ObjectFactory obf = new ObjectFactory();

  // **
  // PRIVATES
  // **

  private RepIdentification rep;

  private RecordListDelivererDC recordListDC;
  private RecordListDelivererIdiom recordListIdiom;
  private RecordListDelivererDatacite recordListDatacite;

  private RecordDelivererDC recordDC;
  private RecordDelivererIdiom recordIdiom;
  private RecordDelivererDatacite recordDatacite;

  private MetadataFormatListDelivererInterface metadataFormatList;
  private SetListDeliverer setList;

  private IdentifierListDelivererDC identifierListDC;
  private IdentifierListDelivererIdiom identifierListIdiom;
  private IdentifierListDelivererDatacite identifierListDatacite;

  // Only for idiomImageUpdateProperties method.
  private String idiomImageUpdateProperties;

  // **
  // PUBLIC
  // **

  public boolean textgrid;
  public boolean dariah;

  /**
   * @param rep
   * @param recordDC
   * @param recordIdiom
   * @param recordDatacite
   * @param recordListDC
   * @param recordListIdiom
   * @param recordListDatacite
   * @param metadataFormatList
   * @param setList
   * @param identifierList
   * @param identifierListIdiom
   * @param identifierListDatacite
   */
  public OaipmhImpl(
      RepIdentification rep, // 0
      RecordDelivererDC recordDC, // 1
      RecordDelivererIdiom recordIdiom, // 2
      RecordDelivererDatacite recordDatacite, // 3
      RecordListDelivererDC recordListDC, // 4
      RecordListDelivererIdiom recordListIdiom, // 5
      RecordListDelivererDatacite recordListDatacite, // 6
      MetadataFormatListDelivererInterface metadataFormatList, // 7
      SetListDeliverer setList, // 8
      IdentifierListDelivererDC identifierList, // 9
      IdentifierListDelivererIdiom identifierListIdiom, // 10
      IdentifierListDelivererDatacite identifierListDatacite // 11
  ) {

    this.rep = rep;

    this.recordDC = recordDC;
    this.recordIdiom = recordIdiom;
    this.recordDatacite = recordDatacite;

    this.recordListDC = recordListDC;
    this.recordListIdiom = recordListIdiom;
    this.recordListDatacite = recordListDatacite;

    this.metadataFormatList = metadataFormatList;
    this.setList = setList;

    this.identifierListDC = identifierList;
    this.identifierListIdiom = identifierListIdiom;
    this.identifierListDatacite = identifierListDatacite;
  }

  /**
   *
   */
  @Override
  public String postRequest(final String verb, final String identifier, final String metadataPrefix,
      final String set, final String from, final String until, final String resumptionToken) {

    log.fine("  --------------------  START postRequest " + this.hashCode());

    log.fine("[POST] verb: " + verb);
    log.fine("[POST] identifier: " + identifier);
    log.fine("[POST] metadataPrefix: " + metadataPrefix);

    String result = "";

    // Check IDs.
    String checkedID = OaipmhUtilities.omitPrefixFromIdentifier(identifier);
    String checkedSET = OaipmhUtilities.omitPrefixFromIdentifier(set);

    try {

      log.fine("  ##  [POST] START getStringFromJAXBOAIElement");

      result = getStringFromJAXBOAIElement(verb,
          handleRequest(
              verb,
              checkedID,
              metadataPrefix,
              checkedSET,
              from,
              until,
              resumptionToken));

      log.fine("  ##  [POST] END getStringFromJAXBOAIElement");

    } catch (ParseException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    log.fine("  --------------------  END postRequest " + this.hashCode());

    return result;
  }

  /**
   *
   */
  @Override
  public String getRequest(final String verb, final String identifier, final String metadataPrefix,
      final String set, final String from, final String until, final String resumptionToken) {

    log.fine("  --------------------  START getRequest " + this.hashCode());

    log.fine("[GET] verb: " + verb);
    log.fine("[GET] identifier: " + identifier);
    log.fine("[GET] metadataPrefix: " + metadataPrefix);

    String result = "";

    // Check IDs.
    String checkedID = OaipmhUtilities.omitPrefixFromIdentifier(identifier);
    String checkedSET = OaipmhUtilities.omitPrefixFromIdentifier(set);

    try {

      log.fine("  ##  [GET] START getStringFromJAXBOAIElement");

      result = getStringFromJAXBOAIElement(verb,
          handleRequest(
              verb,
              checkedID,
              metadataPrefix,
              checkedSET,
              from,
              until,
              resumptionToken));

      log.fine("  ##  [GET] END getStringFromJAXBOAIElement");

    } catch (ParseException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    log.fine("  --------------------  END getRequest " + this.hashCode());

    return result;
  }

  /**
   * @return
   */
  @Override
  public String getVersion() {
    return OaipmhServiceVersion.BUILDNAME + "-" + OaipmhServiceVersion.VERSION + "+"
        + OaipmhServiceVersion.BUILDDATE;
  }

  /**
   *
   */
  @Override
  public Response triggerConedaKORImageMetadataToTextGridUpdate() {

    log.fine("  --------------------  START triggerConedaKORImageMetadataToTextGridUpdate "
        + this.hashCode());

    String properties = this.idiomImageUpdateProperties;

    if (properties == null || properties.isEmpty()) {
      ResponseBuilder rBuilder = Response.serverError();
      rBuilder.entity("no properties file given!");
      return rBuilder.build();
    }
    File f = new File(properties);
    if (!f.exists()) {
      ResponseBuilder rBuilder = Response.serverError();
      rBuilder.entity("properties file " + properties + " is not existing!");
      return rBuilder.build();
    }

    // Create update object and set properties.
    ConedaKor2TextGridRep main = new ConedaKor2TextGridRep();
    main.setPropertiesFileLocation(properties);

    // Do all the update magic.
    String res = "";
    try {
      main.preparations();
      res = main.updateExisting();
      res += "\n" + main.importNew();
    } catch (IOException | CrudClientException e) {
      ResponseBuilder rBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
      rBuilder.entity(res);
      return rBuilder.build();
    }

    log.fine("  --------------------  END triggerConedaKORImageMetadataToTextGridUpdate "
        + this.hashCode());

    // Build response.
    ResponseBuilder rBuilder = Response.ok(res);

    return rBuilder.build();
  }

  /**
   * <p>
   * Checks the Identify request of correctness and response including errors in case of an
   * incorrect request.
   * </p>
   * 
   * @param oaipmhRoot
   * @param request
   * @return
   */
  public OAIPMHtype identifyRequest(OAIPMHtype oaipmhRoot, RequestType request) {

    List<String> errorValues = new ArrayList<String>();
    IdentifyType repIdentificationRequest = new IdentifyType();

    if (this.rep.requestChecker(request)) {

      repIdentificationRequest.setRepositoryName(this.rep.getRepositoryName());
      repIdentificationRequest.setBaseURL(this.rep.getBaseURL());
      repIdentificationRequest.getAdminEmail().add(this.rep.getAdminMail());
      repIdentificationRequest.setEarliestDatestamp(this.rep.getEarliestDatestamp());

      repIdentificationRequest.setDeletedRecord(RepIdentification.DELETE_RECORD);
      repIdentificationRequest.setGranularity(RepIdentification.GRANULARITY);
      repIdentificationRequest.setProtocolVersion(RepIdentification.PROTOCOL_VERSION);

      oaipmhRoot.setIdentify(repIdentificationRequest);

    } else {
      if (request.getIdentifier() != null) {
        errorValues.add(ERR_VAL_IDENTIFIER);
      }
      if (request.getFrom() != null) {
        errorValues.add(ERR_VAL_FROM);
      }
      if (request.getMetadataPrefix() != null) {
        errorValues.add(ERR_VAL_MDPREFIX);
      }
      if (request.getResumptionToken() != null) {
        errorValues.add(ERR_VAL_RESTOK);
      }
      if (request.getUntil() != null) {
        errorValues.add(ERR_VAL_FROM);
      }
      if (request.getSet() != null) {
        errorValues.add(ERR_VAL_SET);
      }

      return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_ARGUMENT, ILLEGAL_ARG + errorValues);
    }

    return oaipmhRoot;
  }

  /**
   * <p>
   * Checks the GetRecord request of correctness and response including errors in case of an
   * incorrect request.
   * </p>
   * 
   * @param oaipmhRoot
   * @param request
   * @return
   */
  public OAIPMHtype getRecordRequest(OAIPMHtype oaipmhRoot, RequestType request) {

    log.fine("  >>>>>>>>>>  getRecordRequest START " + this.hashCode());

    // Check request. In GetRecord: allowed parameters: identifier, metadataPrefix, verb forbidden:
    // resumptionToken, from, till, set optional: no one.

    ErrorHandler requestErrors = RecordDelivererAbstract.requestChecker(request);

    if (requestErrors.getError().getCode() != null) {
      oaipmhRoot.getError().add(requestErrors.getError());
    }

    // Get Record Request is done HERE!
    else {

      // Default is DC.
      RecordDelivererInterface recDeliv = this.recordDC;

      // Take IDIOM if IDIOM prefix.
      if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)) {
        recDeliv = this.recordIdiom;
      }
      if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
        recDeliv = this.recordDatacite;
      }

      // Finally start the QUERY!
      GetRecordType getRecord = new GetRecordType();

      String id = request.getIdentifier();

      log.fine("  >>>>>>>>>>  " + recDeliv.getClass().getName() + " created for ID " + id);

      try {
        getRecord = recDeliv.getRecordById(id);
      } catch (ParseException | DatatypeConfigurationException | IOException e) {
        // TODO Handle exceptions with errors in other methods, too!
        return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_ID_DOES_NOT_EXIST, e.getMessage());
      }

      oaipmhRoot.setGetRecord(getRecord);
    }

    log.fine("  <<<<<<<<<<  getRecordRequest END " + this.hashCode());

    return oaipmhRoot;
  }

  /**
   * <p>
   * Checks the ListIdentifiers request of correctness and response including errors in case of an
   * incorrect request.
   * </p>
   * 
   * @param oai
   * @param request
   * @return
   * @throws ParseException
   */
  public OAIPMHtype listIdentifiersRequest(OAIPMHtype oaipmhRoot, RequestType request)
      throws ParseException {

    // Check request first.
    ErrorHandler requestErrors = IdentifierListDelivererAbstract.requestChecker(request);
    if (requestErrors.getError().getValue() != null) {
      oaipmhRoot.getError().add(requestErrors.getError());
      return oaipmhRoot;
    } else {

      IdentifierListDelivererInterface idListDeliv = null;

      // If metadataFormat IS SET, set list deliverer accordingly.
      if (request.getMetadataPrefix() != null) {
        if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OAIDC_PREFIX)) {
          idListDeliv = this.identifierListDC;
        } else if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)) {
          idListDeliv = this.identifierListIdiom;
        } else if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
          idListDeliv = this.identifierListDatacite;
        }
      }

      // If metadata prefix is NOT set, check resumption token hash maps to decide which metadata
      // format we shall use.
      else {
        // Token is from DC request.
        if (IdentifierListDelivererDC.getCursorCollector() != null && IdentifierListDelivererDC
            .getCursorCollector().containsKey(request.getResumptionToken())) {
          idListDeliv = this.identifierListDC;
        }
        // Token is from IDIOM request.
        else if (IdentifierListDelivererIdiom.getCursorCollector() != null
            && IdentifierListDelivererIdiom.getCursorCollector()
                .containsKey(request.getResumptionToken())) {
          idListDeliv = this.identifierListIdiom;
        }
        // Token is from DATACITE request.
        else if (IdentifierListDelivererDatacite.cursorCollector != null
            && IdentifierListDelivererDatacite.cursorCollector
                .containsKey(request.getResumptionToken())) {
          idListDeliv = this.identifierListDatacite;
        }
        // We have got an invalid resumptionToken here!
        else {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_RESUMPTION_TOKEN, ILLEGAL_RESTOK);
        }
      }

      ListIdentifiersType listIdentifiers = null;
      try {
        listIdentifiers = idListDeliv.processIdentifierList(request.getFrom(), request.getUntil(),
            request.getSet(), request.getResumptionToken());
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      if (listIdentifiers != null) {
        if (listIdentifiers.getHeader() == null || listIdentifiers.getHeader().isEmpty()) {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH, "");
        }
        oaipmhRoot.setListIdentifiers(listIdentifiers);
      } else {
        return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH,
            ILLEGAL_ID + request.getIdentifier());
      }
    }

    return oaipmhRoot;
  }

  /**
   * <p>
   * Checks the ListSets request of correctness and response including errors in case of an
   * incorrect request.
   * </p>
   * 
   * @param oai
   * @param request
   * @return
   */
  public OAIPMHtype listSetsRequest(OAIPMHtype oaipmhRoot, RequestType request) {

    ListSetsType sl = this.setList.setListBuilder();

    if (this.setList.requestChecker(request)) {
      oaipmhRoot.setListSets(sl);
    } else {
      List<String> errorValues = new ArrayList<String>();
      if (request.getIdentifier() != null) {
        errorValues.add(ERR_VAL_IDENTIFIER);
      }
      if (request.getFrom() != null) {
        errorValues.add(ERR_VAL_FROM);
      }
      if (request.getMetadataPrefix() != null) {
        errorValues.add(ERR_VAL_MDPREFIX);
      }
      if (request.getResumptionToken() != null) {
        errorValues.add(ERR_VAL_RESTOK);
      }
      if (request.getUntil() != null) {
        errorValues.add(ERR_VAL_UNTIL);
      }
      if (request.getSet() != null) {
        errorValues.add(ERR_VAL_SET);
      }

      return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_ARGUMENT, ILLEGAL_ARG + errorValues);
    }

    return oaipmhRoot;
  }

  /**
   * @param id
   * @param oaipmhRoot
   * @param request
   * @return
   */
  public OAIPMHtype listMetadataFormatsRequest(String id, OAIPMHtype oaipmhRoot,
      RequestType request) {

    ListMetadataFormatsType listMF = new ListMetadataFormatsType();

    List<String> errorValues = new ArrayList<String>();

    if (this.metadataFormatList.requestChecker(request)) {

      if (id.isEmpty()) {
        listMF = this.metadataFormatList.setMetadataFormatList();
      } else {
        listMF = this.metadataFormatList.setMetadataFormatList(request.getIdentifier());
      }

      if (listMF != null && !listMF.getMetadataFormat().isEmpty()) {
        oaipmhRoot.setListMetadataFormats(listMF);
      } else {
        return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH, ILLEGAL_ID);
      }
    }

    else {
      if (request.getFrom() != null) {
        errorValues.add(ERR_VAL_FROM);
      }
      if (request.getMetadataPrefix() != null) {
        errorValues.add(ERR_VAL_MDPREFIX);
      }
      if (request.getResumptionToken() != null) {
        errorValues.add(ERR_VAL_RESTOK);
      }
      if (request.getSet() != null) {
        errorValues.add(ERR_VAL_SET);
      }
      if (request.getUntil() != null) {
        errorValues.add(ERR_VAL_UNTIL);
      }

      return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_ARGUMENT, REQUIRED_ARG + errorValues);
    }

    return oaipmhRoot;
  }

  /**
   * @param oai
   * @param request
   * @return
   * @throws ParseException
   * @throws IOException
   */
  public OAIPMHtype listRecordsRequest(OAIPMHtype oaipmhRoot, RequestType request)
      throws ParseException, IOException {

    // Check request first.
    ErrorHandler requestErrors = RecordListDelivererAbstract.requestChecker(request);
    if (requestErrors.getError().getCode() != null) {
      oaipmhRoot.getError().add(requestErrors.getError());
      return oaipmhRoot;
    } else {

      RecordListDelivererInterface recListDeliv = null;

      // If metadataFormat IS SET, set list deliverer accordingly.
      if (request.getMetadataPrefix() != null) {
        if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OAIDC_PREFIX)) {
          recListDeliv = this.recordListDC;

          log.fine("created RecordListDelivererDC");
        }
        if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)) {
          recListDeliv = this.recordListIdiom;

          log.fine("created RecordListDelivererIdiom");
        }
        if (request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
          recListDeliv = this.recordListDatacite;

          log.fine("created RecordListDelivererDatacite");
        }
      }

      // If metadata prefix is NOT set, check resumption token hash maps to decide which metadata
      // format we shall use.
      else {

        // Token is from DC request.
        if (RecordListDelivererDC.cursorCollector != null
            && RecordListDelivererDC.cursorCollector.containsKey(request.getResumptionToken())) {
          recListDeliv = this.recordListDC;

          log.fine("created RecordListDelivererDC from restok");
        }
        // Token is from IDIOM request.
        else if (RecordListDelivererIdiom.getCursorCollector() != null && RecordListDelivererIdiom
            .getCursorCollector().containsKey(request.getResumptionToken())) {
          recListDeliv = this.recordListIdiom;

          log.fine("created RecordListDelivererIdiom from restok");
        }
        // Token is from DATACITE request.
        else if (RecordListDelivererDatacite.cursorCollector != null
            && RecordListDelivererDatacite.cursorCollector
                .containsKey(request.getResumptionToken())) {
          recListDeliv = this.recordListDatacite;

          log.fine("created RecordListDelivererDatacite from restok");
        }
        // We have got an invalid resumptionToken here!
        else {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_RESUMPTION_TOKEN, ILLEGAL_RESTOK);
        }
      }

      ListRecordsType listRecords = null;
      try {
        log.fine("calling getRecords [" + this.hashCode() + "]");

        listRecords = recListDeliv.getRecords(
            request.getFrom(),
            request.getUntil(),
            request.getSet(),
            request.getResumptionToken());

        log.fine("called getRecords [" + this.hashCode() + "]");

      } catch (IOException e) {
        // TODO Handle exceptions with errors in other methods, too!
        String eMessage = e.getMessage();
        if (eMessage.startsWith(OaipmhConstants.ERROR_SET_UNKNOWN)) {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH, e.getMessage());
        } else if (eMessage.startsWith(OaipmhConstants.ERROR_FETCH_FIELDS)) {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH, e.getMessage());
        } else {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_ARGUMENT, e.getMessage());
        }
      }

      if (listRecords != null) {
        if (listRecords.getRecord() == null || listRecords.getRecord().isEmpty()) {
          return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_NO_RECORD_MATCH, "");
        }
        oaipmhRoot.setListRecords(listRecords);
      } else {
        return oaipmhError(oaipmhRoot, OaipmhConstants.OAI_BAD_ARGUMENT, ILLEGAL_OTHER);
      }
    }

    return oaipmhRoot;
  }

  /**
   * <p>
   * Taking the string from the REST request and converting into an OAIPMH verb type.
   * </p>
   * 
   * @param verb : what is to within the repository
   * @return verbParam: the verbParam as an OAIPMH verb type
   */
  public VerbType setVerb(String verb) {

    VerbType verbParam = null;

    if (verb != null && verb.equals("Identify")) {
      verbParam = VerbType.IDENTIFY;
    } else if (verb != null && verb.equals("ListMetadataFormats")) {
      verbParam = VerbType.LIST_METADATA_FORMATS;
    } else if (verb != null && verb.equals("ListSets")) {
      verbParam = VerbType.LIST_SETS;
    } else if (verb != null && verb.equals("ListIdentifiers")) {
      verbParam = VerbType.LIST_IDENTIFIERS;
    } else if (verb != null && verb.equals("ListRecords")) {
      verbParam = VerbType.LIST_RECORDS;
    } else if (verb != null && verb.equals("GetRecord")) {
      verbParam = VerbType.GET_RECORD;
    }

    return verbParam;
  }

  /**
   * <p>
   * Setting the from value for the request.
   * </p>
   * 
   * @param from
   */
  public void setFromRequestValue(String from, RequestType request) {
    if (!from.isEmpty()) {
      request.setFrom(from);
    }
  }

  /**
   * <p>
   * Setting the until value for the request.
   * </p>
   * 
   * @param until
   */
  public void setUntilRequestValue(String until, RequestType request) {
    if (!until.isEmpty()) {
      request.setUntil(until);
    }
  }

  /**
   * <p>
   * Setting the identifier value for the request.
   * </p>
   * 
   * @param identifier
   */
  public void setIdentifierRequestValue(String identifier, RequestType request) {
    if (!identifier.isEmpty()) {
      request.setIdentifier(identifier);
    }
  }

  /**
   * <p>
   * Setting the metadataPrefix value for the request.
   * </p>
   * 
   * @param metadataPrefix
   */
  public void setMetadataPrefixRequestValue(String metadataPrefix, RequestType request) {
    if (!metadataPrefix.isEmpty()) {
      request.setMetadataPrefix(metadataPrefix);
    }
  }

  /**
   * <p>
   * Setting the resumptionToken value for the request.
   * </p>
   * 
   * @param resumptionToken
   */
  public void setResumptionTokenRequestValue(String resumptionToken, RequestType request) {
    if (!resumptionToken.isEmpty()) {
      request.setResumptionToken(resumptionToken);
    }
  }

  /**
   * <p>
   * Setting the set value for the request
   * </p>
   * 
   * @param set
   * @param request
   */
  public void setSetRequestValue(String set, RequestType request) {
    if (!set.isEmpty()) {
      request.setSet(set);
    }
  }

  /**
   * @param textgrid
   */
  public void setTextGrid(boolean textgrid) {
    this.textgrid = textgrid;
  }

  /**
   * @param dariah
   */
  public void setDariah(boolean dariah) {
    this.dariah = dariah;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param verb
   * @param identifier
   * @param metadataPrefix
   * @param set
   * @param from
   * @param until
   * @param resumptionToken
   * @return
   * @throws ParseException
   * @throws IOException
   */
  private JAXBElement<OAIPMHtype> handleRequest(String verb, String identifier,
      String metadataPrefix, String set, String from, String until, String resumptionToken)
      throws ParseException, IOException {

    log.fine("  ##  START handleRequest");

    JAXBElement<OAIPMHtype> result;

    OAIPMHtype oaipmhRoot = new OAIPMHtype();
    VerbType verbParam = setVerb(verb);
    RequestType request = new RequestType();

    // Set the responseDate of today.
    try {
      oaipmhRoot.setResponseDate(OaipmhUtilities.getCurrentUTCDateAsGregorian());
    } catch (DatatypeConfigurationException e) {
      log.severe("datatype configuration failed");
    }

    // Convert the request values.
    setFromRequestValue(from, request);
    setUntilRequestValue(until, request);
    setIdentifierRequestValue(identifier, request);
    setMetadataPrefixRequestValue(metadataPrefix, request);
    setResumptionTokenRequestValue(resumptionToken, request);
    setSetRequestValue(set, request);
    request.setVerb(verbParam);
    request.setValue(this.rep.getBaseURL());

    oaipmhRoot.setRequest(request);

    if (verbParam != null) {
      if (verbParam.value().equals("Identify")) {
        oaipmhRoot = identifyRequest(oaipmhRoot, request);
      } else if (verbParam.value().equals("GetRecord")) {

        log.fine("  ##  START getRecordRequest");

        oaipmhRoot = getRecordRequest(oaipmhRoot, request);

        log.fine("  ##  END getRecordRequest");

      } else if (verbParam.value().equals("ListIdentifiers")) {
        oaipmhRoot = listIdentifiersRequest(oaipmhRoot, request);
      } else if (verbParam.value().equals("ListSets")) {
        oaipmhRoot = listSetsRequest(oaipmhRoot, request);
      } else if (verbParam.value().equals("ListMetadataFormats")) {
        oaipmhRoot = listMetadataFormatsRequest(identifier, oaipmhRoot, request);
      } else if (verbParam.value().equals("ListRecords")) {
        oaipmhRoot = listRecordsRequest(oaipmhRoot, request);
      }
    } else {
      oaipmhRoot = oaipmhError(oaipmhRoot, OaipmhConstants.OAI_VERB_ERROR, ILLEGAL_OAI_VERB);
    }

    result = obf.createOAIPMH(oaipmhRoot);

    log.fine("  ##  END handleRequest");

    return result;
  }

  /**
   * <p>
   * Transforms the JaxB element into a string that contains the xsi:schemaLocation attribute. No
   * other way has been working here! I tried EVERYTHING else! Really!
   * </p>
   * 
   * @param theVerb
   * @param theJAXBOAIElement
   * @return
   */
  private static String getStringFromJAXBOAIElement(final String theVerb,
      final JAXBElement<OAIPMHtype> theJAXBOAIElement) {

    String result = "";

    // Always add OAIPMH schema location.
    String schemaLocations =
        OaipmhConstants.OAIPMH_NAMESPACE + " " + OaipmhConstants.OAIPMH_SCHEMA_LOCATION;

    // Add OAI_DC, DC, and openaire datacite schema location if needed.
    if (theVerb != null && (theVerb.equals("GetRecord") || theVerb.endsWith("ListRecords"))) {
      schemaLocations +=
          " " + OaipmhConstants.OAIDC_NAMESPACE + " " + OaipmhConstants.OAIDC_SCHEMA_LOCATION + " "
              + OaipmhConstants.DC_NAMESPACE + " " + OaipmhConstants.DC_SCHEMA_LOCATION + " "
              + OaipmhConstants.DATACITE_NAMESPACE + " " + OaipmhConstants.DATACITE_SCHEMA_LOCATION;
    }

    // PLEASE NOTE We always get oai_dc and dc prefixes defined in the root element due to the
    // bindings.xml file. Seems to be no problem for the time being.

    try {
      // OBACHT! Maybe GEMOKEL! Add schemaLocations to OAI root element here!
      JAXBContext jc = JAXBContext.newInstance(OAIPMHtype.class);

      Marshaller marshaller = jc.createMarshaller();
      // Comment out if one-line-output wanted!
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemaLocations);

      StringWriter sw = new StringWriter();
      marshaller.marshal(theJAXBOAIElement, sw);
      result = sw.toString();
      sw.close();

    } catch (JAXBException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return result;
  }

  /**
   * <p>
   * Build an OAI-PMH error.
   * </p>
   * 
   * @param theOaipmhError
   * @param theErrorMessage
   * @return
   */
  private static OAIPMHtype oaipmhError(OAIPMHtype theOaipmhRoot, String theOaipmhError,
      String theErrorMessage) {

    OAIPMHtype result = theOaipmhRoot;

    ErrorHandler e = new ErrorHandler();
    e.setError(theOaipmhError, theErrorMessage);

    result.getError().add(e.getError());

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param idiomImageUpdateProperties
   */
  public void setIdiomImageUpdateProperties(String idiomImageUpdateProperties) {
    this.idiomImageUpdateProperties = idiomImageUpdateProperties;
  }

}

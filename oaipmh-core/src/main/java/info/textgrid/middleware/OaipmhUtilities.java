package info.textgrid.middleware;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.clients.AuthClient;
import info.textgrid.clients.tgauth.AuthClientException;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.MetadataType;
import info.textgrid.middleware.oaipmh.RecordType;
import info.textgrid.middleware.oaipmh.Resource;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;

/**
 * <p>
 * OAIPMH Utilities is responsible to implement all necessary functions to implement the arguments
 * of a request.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class OaipmhUtilities {

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(OaipmhUtilities.class.getName());

  // **
  // FINALS
  // **

  public static final String ES_DIVIDER_CHAR = ".";
  public static final String ES_DIVIDER_REGEXP = "\\.";

  public static final String PREFIX_DEVIDER = ":";
  public static final String PREFIX_HDL = "hdl";
  public static final String PREFIX_TEXTGRID = "textgrid";
  public static final Set<String> IDENTIFIER_PREFIXES_TO_FILTER =
      new HashSet<String>(Arrays.asList(PREFIX_HDL, PREFIX_TEXTGRID));

  // Define date formats.
  public static final String UTC_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final DateTimeFormatter UTC_FORMATTER =
      DateTimeFormatter.ofPattern(UTC_FORMAT_STRING).withZone(ZoneId.of("UTC"));
  public static final SimpleDateFormat FROM_UNTIL_FORMAT_LONG =
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
  public static final SimpleDateFormat FROM_UNTIL_FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");
  public static final String FROM_UNTIL_SHORT_MATCH = "\\d{4}-\\d{2}-\\d{2}";
  public static final String FROM_UNTIL_LONG_MATCH = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}";

  // **
  // CLASS
  // **

  private String rangeField;
  private long resultSize;
  private boolean foundItems;
  private String formatField;
  private ResumptionTokenType resTokenForResponse;
  private String formatToFilter;
  private int searchResponseSize;

  // **
  // STATICS
  // **

  /**
   * @return
   * @throws AuthClientException
   */
  public static List<ProjectInfo> getProjectList() throws AuthClientException {

    AuthClient auth = new AuthClient();
    List<ProjectInfo> projectInfos = auth.getAllProjects();
    auth.getAllProjects();

    return projectInfos;
  }

  /**
   * @param projectID
   * @return
   * @throws AuthClientException
   */
  public static String getProjectName(String projectID) throws AuthClientException {
    AuthClient auth = new AuthClient();
    return auth.getProjectInfo(projectID).getName();
  }

  /**
   * <p>
   * Get the current UTC date string for OAI-PMH response dates from current system millis.
   * </p>
   * 
   * @return
   */
  public static String getCurrentUTCDateAsString() {
    return UTC_FORMATTER.format(Instant.now());
  }

  /**
   * <p>
   * Converting a given string representing a date value into the date format required for OAIPMH
   * and give it back as string.<br/>
   * The original date time string may here be tg datestamps from tg metadata
   * (2012-02-10T23:45:00.507+01:00), or dh datestamps from dcterms metadata
   * (2018-05-22T17:52:48.626).
   * </p>
   * 
   * @param originalDateTimeString
   * @return
   */
  public static String getUTCDateAsString(final String originalDateTimeString) {

    String result;

    log.fine("original DateTimeString: " + originalDateTimeString);

    Instant instant;
    try {
      OffsetDateTime odt = OffsetDateTime.parse(originalDateTimeString);
      instant = Instant.from(odt);
    }

    // NOTE We are missing to set time zones on dcterms date fields in administrative metadata!
    // Workaround for all DARIAH-DE Repository dates so far (until fix) is using LocalDateTime here
    // with ZoneId CET, what has been local time zone until now!
    catch (DateTimeParseException e) {
      LocalDateTime ldt = LocalDateTime.parse(originalDateTimeString);
      instant = ldt.atZone(ZoneId.of("CET")).toInstant();
    }

    result = UTC_FORMATTER.format(instant);

    log.fine("parsed to UTC: " + result);

    return result;
  }

  /**
   * <p>
   * Converting a given string representing a date value into XMLGregorianCalendar.
   * </p>
   * 
   * @param originalDateTimeString
   * @return
   * @throws DatatypeConfigurationException
   */
  public static XMLGregorianCalendar getUTCDateAsGregorian(String originalDateTimeString)
      throws DatatypeConfigurationException {
    return DatatypeFactory.newInstance()
        .newXMLGregorianCalendar(getUTCDateAsString(originalDateTimeString));
  }

  /**
   * @param originalDateTimeString
   * @return
   * @throws DatatypeConfigurationException
   */
  public static XMLGregorianCalendar getCurrentUTCDateAsGregorian()
      throws DatatypeConfigurationException {
    return getUTCDateAsGregorian(getCurrentUTCDateAsString());
  }

  /**
   * <p>
   * Check incoming date.
   * </p>
   * 
   * @param incomingDate
   * @return
   */
  public static boolean isParsableDate(String incomingDate) {

    boolean result = false;

    log.fine("incoming date: " + incomingDate);

    // Look for a match for valid date formatting, only "1970-12-10T18:30:00" or "1970-12-10" are
    // allowed!
    boolean matchesShort = incomingDate.matches(FROM_UNTIL_SHORT_MATCH);
    boolean matchesLong = incomingDate.matches(FROM_UNTIL_LONG_MATCH);

    try {
      // If matching long or short date, look for valid and parsable date!
      if (matchesShort) {
        FROM_UNTIL_FORMAT_SHORT.parse(incomingDate);
        result = true;
      } else if (matchesLong) {
        FROM_UNTIL_FORMAT_LONG.parse(incomingDate);
        result = true;
      }
    } catch (ParseException e) {
      log.warning("date can not be parsed: " + incomingDate);
    }

    return result;
  }

  /**
   * <p>
   * Returns a resumptionToken for a search, if necessary.
   * </p>
   * 
   * @param completeListSize
   * @param resumptionToken
   * @param cursorCollector
   * @param scrollID
   * @param searchResponseSize
   * @return A resumption token if applicable, null otherwise.
   */
  public static ResumptionTokenType getResumptionToken(final long completeListSize,
      final String resumptionToken, Map<String, Integer> cursorCollector, final String scrollID,
      final int searchResponseSize) {

    ResumptionTokenType result = null;

    log.fine("need a resumption token?\n"
        + "  completeListSize: " + completeListSize + "\n"
        + "  searchResponseSize: " + searchResponseSize + "\n"
        + "  resumptionToken: " + resumptionToken + "\n"
        + "  scrollID: " + scrollID + "\n"
        + "  cursorCollector (size:" + cursorCollector.size() + "): " + cursorCollector + "\n");

    if (completeListSize > searchResponseSize) {
      int cursor;
      result = new ResumptionTokenType();

      log.fine("yes, we NEED a restok");

      // 1. Complete list size is > searchResponseSize and a token already is existing: we need to
      // check hash map!
      if (resumptionToken != null && cursorCollector.containsKey(resumptionToken)) {
        // Get cursor from resumption token map and increase.
        cursor = cursorCollector.get(resumptionToken).intValue() + searchResponseSize;
      }
      // 2. Complete list size is > searchResponseSize and we have no token: we do need one!
      else {
        // Cursor must be 0 in first response.
        cursor = 0;
      }
      // Set cursor, token value, and complete list size.
      result.setCursor(BigInteger.valueOf(cursor));
      result.setValue(scrollID);
      result.setCompleteListSize(BigInteger.valueOf(completeListSize));
      // Update token map.
      cursorCollector.put(scrollID, cursor);

      log.fine("restok from scroll id (cursor: " + cursor + "): " + result.getValue());

      // Last response, no more objects are available!
      if ((cursor + searchResponseSize) >= completeListSize) {
        // Remove resumption token string from response, and clear token map.
        result.setValue("");
        cursorCollector.remove(scrollID);

        log.fine("last response, restok deleted");

        // FIXME See, how large the map is getting! Add expiration date to response and delete
        // expired tokens!!
      }
    }

    // 3. Complete list size is <= searchResponseSize (we do not need a token! do nothing!)
    else {
      log.fine("no, we do NOT need a restok!");
    }

    return result;
  }

  /**
   * <p>
   * Loops over all fields and collects field contents.
   * </p>
   * 
   * @param resultFromGetRequestInES
   * @param fields
   * @return Returns a list with all field entries.
   */
  public static List<String> loopListFieldLoader(final JSONObject resultFromGetRequestInES,
      final String[] fields) {

    List<String> result = new ArrayList<String>();

    for (String f : fields) {
      result.addAll(listFieldLoader(resultFromGetRequestInES, f));
    }

    return result;
  }

  /**
   * <p>
   * Loads the field with one param.
   * </p>
   * 
   * @param resultFromGetRequestInES
   * @param field
   * @return Returns one or more params as a list.
   */
  public static List<String> listFieldLoader(final JSONObject resultFromGetRequestInES,
      final String field) {

    List<String> fieldResults = new ArrayList<String>();

    log.fine("JSON: " + (resultFromGetRequestInES == null ? "NULL" : resultFromGetRequestInES));
    log.fine("FIELD: " + field);

    // Check if field has got "." in it, such as
    // "edition.source.bibliographicCitation.placeOfPublication".
    String[] fieldPathForESIndex = field.split(ES_DIVIDER_REGEXP);

    JSONObject singlePath = resultFromGetRequestInES;
    try {
      // Loop the splitted field components, such as "edition", "source", etcpp.
      for (int i = 0; i < fieldPathForESIndex.length; i++) {

        String fieldComponent = fieldPathForESIndex[i];

        log.fine("i=" + i);
        log.fine("fieldPathLength=" + fieldPathForESIndex.length);

        // Case 1: Reduce JSON object.
        if (i < fieldPathForESIndex.length - 1) {

          log.fine(
              "case 1: " + i + "<" + (fieldPathForESIndex.length - 1) + " : reduce singlePath");

          singlePath = singlePath.getJSONObject(fieldPathForESIndex[i]);
        }
        // Case 2: We have one field component.
        else if (fieldPathForESIndex.length == 1) {

          log.fine("case 2: we have only one field component in field " + field);

          // Add content from JSON, Array or String.
          fieldResults.addAll(getFieldContent(singlePath, fieldComponent));

        }
        // Case 3: We have got more field components.
        else {

          log.fine("case 3: we have " + fieldPathForESIndex.length + " components in " + field);

          // Add content from JSON, Array or String.
          fieldResults.addAll(getFieldContent(singlePath, fieldComponent));

        }
      }
    } catch (JSONException e) {
      log.fine("IGNORING JSON ERROR: " + e.getMessage());
    }

    log.fine("field results: " + fieldResults);

    return fieldResults;
  }

  /**
   * <p>
   * Loads the field with the first param from. Please use for single value fields only.
   * </p>
   * 
   * @param resultFromGetRequestInES
   * @param field
   * @return Returns only the first String from the result list, or "" if list is empty.
   */
  public static String firstEntryFieldLoader(JSONObject resultFromGetRequestInES, String field) {

    String result = "";

    List<String> list = listFieldLoader(resultFromGetRequestInES, field);
    if (!list.isEmpty()) {
      result = list.get(0);
    }

    return result;
  }

  /**
   * @param theESClient The ES client to use.
   * @param idInElasticSearchIndex The ID to get the ES record from.
   * @param includes Including the fields to fetch from the elastic search index.
   * @param excludes Excluding the fields to fetch from the elastic search index.
   * @return
   * @throws IOException
   */
  protected static GetResponse getRecordByIDFromElasticSearch(OaipmhElasticSearchClient theESClient,
      String idInElasticSearchIndex, String[] includes, String[] excludes) throws IOException {

    log.fine("id in es index: " + idInElasticSearchIndex);
    log.fine("include/exclude: " + includes.length + "/" + excludes.length);

    // Setting the source context for fetching the fields from the elastic search index
    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

    log.fine("esclient/index: " + (theESClient != null ? theESClient : "null") + "/"
        + (theESClient != null ? theESClient.getEsIndex() : "null"));

    // Check for ES client.
    if (theESClient == null) {
      throw new IOException("elasticsearch client is NULL!");
    }

    // Building the getRequest against the elastic search index.
    GetRequest getRequest = new GetRequest(theESClient.getEsIndex(), idInElasticSearchIndex)
        .fetchSourceContext(fetchSourceContext);

    log.fine("get request id: " + getRequest.id());

    // Declaration of the result from the get-request.
    GetResponse esResultObject = theESClient.getEsClient().get(getRequest, RequestOptions.DEFAULT);

    log.fine("es result object is existing: " + esResultObject.isExists());

    if (!esResultObject.isExists()) {
      throw new IOException("es record with id " + idInElasticSearchIndex + " is not existing!");
    }

    return esResultObject;
  }

  /**
   * @param resource
   * @throws JAXBException
   */
  public static void marshal(Resource resource) throws JAXBException {
    JAXBContext jc = JAXBContext.newInstance(Resource.class);
    Marshaller marshaller = jc.createMarshaller();
    StringWriter stringWriter = new StringWriter();
    marshaller.marshal(resource, stringWriter);
    marshaller.marshal(resource, System.out);
  }

  /**
   * @param metadata
   * @param header
   * @return
   */
  public static GetRecordType getRecordType(Object metadata, HeaderType header) {

    MetadataType metadataType = new MetadataType();
    RecordType recordType = new RecordType();
    GetRecordType getRecordType = new GetRecordType();

    metadataType.setAny(metadata);
    recordType.setMetadata(metadataType);
    recordType.setHeader(header);
    getRecordType.setRecord(recordType);

    return getRecordType;
  }

  /**
   * @param propertyName
   * @param propFile
   * @return
   */
  public static String[] fetchFieldsFromPropFile(String propertyName, Properties propFile) {
    // Trim with whitespaces, so we get the array with results trimmed.
    return propFile.getProperty(propertyName).split("\\s*,\\s*");
  }

  /**
   * <p>
   * Check identifier for prefixes: Remove "textgrid:" and "hdl:" prefixes.
   * </p>
   * 
   * @param identifier
   * @return The identifier without prefix, if contained in defined prefix list.
   */
  public static String omitPrefixFromIdentifier(final String identifier) {

    String result = identifier;

    if (identifier != null && !identifier.isEmpty() && identifier.contains(PREFIX_DEVIDER)) {
      String prefix[] = identifier.split(PREFIX_DEVIDER);
      String px = prefix[0];
      String id = prefix[1];
      if (IDENTIFIER_PREFIXES_TO_FILTER.contains(px)) {
        result = id;
        log.fine("incoming id " + id + " has been filtered to: " + result);
      }
    }

    return result;
  }

  /**
   * <p>
   * Create header type of OAI-PMH response.
   * </p>
   * 
   * @param dateOfCreation The date of the resource's creation.
   * @param identifier The identifier to set in the header. Please see to it, that it is correct
   *        here, and have not to be changed in any way.
   * @param setSpec The ID of the set spec.
   * @return
   */
  public static HeaderType computeResponseHeader(final String dateOfCreation,
      final String identifier, final String setSpec) {

    HeaderType result = new HeaderType();

    result.setIdentifier(identifier);
    result.setDatestamp(dateOfCreation);

    log.finer(
        "Header (DoC / ID / setSpec): " + dateOfCreation + " / " + identifier + " / " + setSpec);

    if (setSpec != null && !setSpec.isEmpty()) {
      result.getSetSpec().add(setSpec);
    }

    return result;
  }

  /**
   * <p>
   * Get the setSpec, if given, check for prefix, if not given, use prefix and given ID.
   * </p>
   * 
   * @param theSetSpec
   * @param theSpecPrefix
   * @param theID
   * @return A valid setSpec value for the set.
   */
  public static String getSetSpec(String theSetSpec, String theSpecPrefix, String theID) {

    String result = theSetSpec;

    // Check given setSpec value, add ID if empty.
    if (result == null || result.isEmpty() || result.equals(theSpecPrefix)) {
      result = theID;
    }

    // Then check for prefix, add prefix, if necessary.
    if (!result.startsWith(theSpecPrefix)) {
      result = theSpecPrefix + result;
    }

    return result;
  }

  /**
   * @param theSID
   * @return
   */
  public static String hideSID(String theSID) {

    String result = "(null or empty)";

    if (theSID != null && !theSID.isEmpty()) {
      int size = theSID.length();
      if (size < 5) {
        result = "(sid existing and too short)";
      } else {
        result = theSID.substring(0, 4) + "...";
      }
    }

    return result;
  }

  /**
   * @param theURI
   * @return
   */
  public static String getTextGridBaseURI(String theURI) {

    String result = theURI;

    if (theURI.contains(".")) {
      result = theURI.substring(0, theURI.indexOf('.'));
    }

    return result.trim();
  }

  /**
   * @param theURI
   * @return
   */
  public static String getTextGridRevisionURI(String theURI) {

    // TODO Get latest revision from tgsearch?
    // https://dev.textgridlab.org/1.0/tgsearch/info/textgrid:3qmmt/revisions?sid=***

    String result = theURI;

    if (!theURI.contains(".")) {
      result = getTextGridBaseURI(theURI) + ".0";
    }

    return result.trim();
  }


  /**
   * @param message
   * @return
   * @throws IOException
   */
  public static MetadataType buildMETSErrorMetadata(String message) throws IOException {

    MetadataType result = new MetadataType();

    String metsError =
        "<mets xsi:schemaLocation=\"http://www.loc.gov/METS/"
            + " http://www.loc.gov/standards/mets/mets.xsd\""
            + " xmlns=\"http://www.loc.gov/METS/\""
            + " xmlns:dv=\"http://dfg-viewer.de/\""
            + " xmlns:xlink=\"http://www.w3.org/1999/xlink\""
            + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
            + "  <amdSec ID=\"error\" />\n"
            + "  <structMap TYPE=\"LOGICAL\">\n"
            + "    <div LABEL=\"error\">\n"
            + "      <![CDATA[ " + message + " ]]>\n"
            + "    </div>\n"
            + "  </structMap>\n"
            + "</mets>\n";

    log.warning(message);

    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder;
      builder = factory.newDocumentBuilder();
      Document doc = builder.parse(new InputSource(new StringReader(metsError)));

      result.setAny(doc.getDocumentElement());

    } catch (ParserConfigurationException | SAXException e1) {
      log.severe(
          "ERROR BUILDING ERROR METS METADATA PART! PLEASE CONTACT YOUR LOCAL XML BUILDER! SYSTEM MESSAGE: "
              + e1.getMessage());
    }

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Get List of Strings from JSON object, Array or String!
   * </p>
   * 
   * @param singlePath
   * @param fieldComponent
   * @return
   */
  private static List<String> getFieldContent(JSONObject singlePath, String fieldComponent) {

    List<String> result = new ArrayList<String>();

    // Examine JSONArray, take String if not applicable.
    try {
      JSONArray singlePathArray = singlePath.getJSONArray(fieldComponent);

      log.fine("get json array: " + singlePathArray.toString(2));

      for (int j = 0; j < singlePathArray.length(); j++) {
        result.add(singlePathArray.getString(j));
      }
    } catch (JSONException e) {
      Object noArray = singlePath.get(fieldComponent);

      if (noArray instanceof String) {

        log.fine("get string: " + noArray);

        result.add(singlePath.getString(fieldComponent));

      } else if (noArray instanceof Integer) {

        log.fine("get int: " + noArray);

        result.add(String.valueOf(singlePath.getInt(fieldComponent)));
      } else {
        log.fine("no string & no int: " + noArray);
      }
    }

    return result;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public String getRangeField() {
    return this.rangeField;
  }

  /**
   * @param rangeField
   */
  public void setRangeField(String rangeField) {
    this.rangeField = rangeField;
  }

  /**
   * @return
   */
  public long getResultSize() {
    return this.resultSize;
  }

  /**
   * @param resultSize
   */
  public void setResultSize(long resultSize) {
    this.resultSize = resultSize;
  }

  /**
   * @return
   */
  public boolean isFoundItems() {
    return this.foundItems;
  }

  /**
   * @param foundItems
   */
  public void setFoundItems(boolean foundItems) {
    this.foundItems = foundItems;
  }

  /**
   * @return
   */
  public ResumptionTokenType getResTokenForResponse() {
    return this.resTokenForResponse;
  }

  /**
   * @param resTokenForResponse
   */
  public void setResTokenForResponse(ResumptionTokenType resTokenForResponse) {
    this.resTokenForResponse = resTokenForResponse;
  }

  /**
   * @return
   */
  public String setFormatField() {
    return this.formatField;
  }

  /**
   * @param formatField
   */
  public void setFormatField(String formatField) {
    this.formatField = formatField;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @return
   */
  public int getSearchResponseSize() {
    return this.searchResponseSize;
  }

  /**
   * @param searchResponseSize
   */
  public void setSearchResponseSize(int searchResponseSize) {
    this.searchResponseSize = searchResponseSize;
  }

}

package info.textgrid.middleware;

/**
 * <p>
 * Collection of OAI-PMH constant values.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */

public final class OaipmhConstants {

  // Metadata Prefixes.
  public static final String METADATA_OAIDC_PREFIX = "oai_dc";
  public static final String METADATA_IDIOM_PREFIX = "oai_idiom_mets";
  public static final String METADATA_OPENAIRE_PREFIX = "oai_datacite";

  // Namespaces
  public static final String OAIPMH_NAMESPACE = "http://www.openarchives.org/OAI/2.0/";
  public static final String OAIPMH_SCHEMA_LOCATION =
      "http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd";

  public static final String OAIDC_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/";
  public static final String OAIDC_SCHEMA_LOCATION =
      "https://www.openarchives.org/OAI/2.0/oai_dc.xsd";

  public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
  public static final String DC_SCHEMA_LOCATION =
      "https://dublincore.org/schemas/xmls/simpledc20021212.xsd";

  public static final String METS_NAMESPACE = "http://www.loc.gov/METS/";
  public static final String METS_SCHEMA_LOCATION = "http://www.loc.gov/standards/mets/mets.xsd";

  public static final String IDIOM_IMAGE_NAMESPACE = METS_NAMESPACE;
  public static final String IDIOM_IMAGE_SCHEMA_LOCATION = METS_SCHEMA_LOCATION;

  public static final String DATACITE_NAMESPACE = "http://datacite.org/schema/kernel-3";
  public static final String DATACITE_SCHEMA_LOCATION =
      "http://schema.datacite.org/meta/kernel-3/metadata.xsd";

  // Error String Constants.
  public static final String OAI_BAD_ARGUMENT = "BadArgument";
  public static final String OAI_METADATA_FORMAT_ERROR = "FormatError";
  public static final String OAI_NO_RECORD_MATCH = "RecordMatchError";
  public static final String OAI_NO_SET_HIERARCHY = "SetHierarchyError";
  public static final String OAI_BAD_RESUMPTION_TOKEN = "badResumptionToken";
  public static final String OAI_ID_DOES_NOT_EXIST = "idDoesNotExist";
  public static final String OAI_VERB_ERROR = "badVerb";

  // Other errors.
  public static final String ERROR_SET_UNKNOWN = "Set name is unknown";
  public static final String ERROR_FETCH_FIELDS = "Unable to fecht fields";
  public static final String ERROR_UNIDENTIFIED = "Unidentified error";

  // IDIOM field name constants.
  public static final String IDIOM_MODIFIED_FIELD = "lastModified";
  public static final String IDIOM_PROJECT_ID_FIELD = "project.id";
  public static final String IDIOM_FORMAT_FIELD = "format";
  public static final String IDIOM_NOTES_FIELD = "notes";
}

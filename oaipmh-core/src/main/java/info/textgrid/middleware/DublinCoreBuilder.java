package info.textgrid.middleware;

import java.text.ParseException;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import info.textgrid.middleware.oaipmh.ElementType;
import info.textgrid.middleware.oaipmh.MetadataType;
import info.textgrid.middleware.oaipmh.OaiDcType;
import info.textgrid.middleware.oaipmh.ObjectFactory;

/**
 * <p>
 * The DublinCoreBuilder is responsible for generating a List of JAXBElements in DublinCore
 * standard. Each function get a list of strings representing the mapped textgrid metadata for the
 * specific DC-field.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-10-17
 * @since 2014-02.18
 */

public final class DublinCoreBuilder {

  // Object factory for all the DC elements.
  private static ObjectFactory oaiDcObj = new ObjectFactory();

  // MetadataType of OAI schema.
  private MetadataType metadata = new MetadataType();

  // OAI_DC type of oai_dc schema.
  private OaiDcType tgMappedDC = new OaiDcType();

  /**
   * <p>
   * Constructor to build an DublinCore element for an OAIPMH request by getting TextGrid metadata.
   * </p>
   */
  public DublinCoreBuilder() {
    //
  }

  /**
   * <p>
   * Each of the following functions for generating the DC-XML file produces an ElementType for the
   * DC-field and puts each element of the list into the corresponding XML-tag.
   * </p>
   */

  /**
   * <p>
   * Takes a string list containing the contributors and build the specific DublinCore element for
   * each list entry.
   * </p>
   * 
   * @param contributors
   */
  public void setContributor(List<String> contributors) {
    for (String dccontributor : contributors) {
      ElementType contributorElement = new ElementType();
      JAXBElement<ElementType> dcCoreContributor = oaiDcObj.createContributor(contributorElement);
      contributorElement.setValue(dccontributor);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreContributor);
    }
  }

  /**
   * <p>
   * Takes a string list containing the creators and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param creators
   */
  public void setCreator(List<String> creators) {
    for (String dccreator : creators) {
      ElementType creatorElement = new ElementType();
      creatorElement.setValue(dccreator);
      JAXBElement<ElementType> dcCoreCreator = oaiDcObj.createCreator(creatorElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreCreator);
    }
  }

  /**
   * <P>
   * Takes a string list containing the coverages and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param coverages
   */
  public void setCoverage(List<String> coverages) {
    for (String dccoverage : coverages) {
      ElementType coverageElement = new ElementType();
      coverageElement.setValue(dccoverage);
      JAXBElement<ElementType> dcCoreCoverage = oaiDcObj.createCoverage(coverageElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreCoverage);
    }
  }

  /**
   * <p>
   * Takes a string list containing the dates and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param dates
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  public void setDate(List<String> dates) throws ParseException, DatatypeConfigurationException {
    for (String dcdate : dates) {
      ElementType dateElement = new ElementType();
      JAXBElement<ElementType> dcCoreDate = oaiDcObj.createDate(dateElement);
      dateElement.setValue(dcdate);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreDate);
    }
  }

  /**
   * <p>
   * Takes a string list containing the descriptions and build the specific DublinCore element for
   * each list entry.
   * </p>
   * 
   * @param descriptions
   */
  public void setDescription(List<String> descriptions) {
    for (String dcdescription : descriptions) {
      ElementType descriptionElement = new ElementType();
      descriptionElement.setValue(dcdescription);
      JAXBElement<ElementType> dcCoreDescription = oaiDcObj.createDescription(descriptionElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreDescription);
    }
  }

  /**
   * <p>
   * Takes a string list containing the formats and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param formats
   */
  public void setFormat(List<String> formats) {
    for (String dcformat : formats) {
      ElementType formatElement = new ElementType();
      formatElement.setValue(dcformat);
      JAXBElement<ElementType> dcCoreFormat = oaiDcObj.createFormat(formatElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreFormat);
    }
  }

  /**
   * <p>
   * Takes a string list containing the identifiers and build the specific DublinCore element for
   * each list entry.
   * </p>
   * 
   * @param identifiers
   */
  public void setIdentifier(List<String> identifiers) {

    if (identifiers != null) {
      for (String dcidentifier : identifiers) {

        ElementType identifierElement = new ElementType();
        identifierElement.setValue(dcidentifier);
        JAXBElement<ElementType> dcCoreIdentifier = oaiDcObj.createIdentifier(identifierElement);
        this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreIdentifier);
      }
    }
  }

  /**
   * <p>
   * Takes a string list containing the languages and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param languages
   */
  public void setLanguage(List<String> languages) {
    for (String dclanguage : languages) {
      ElementType languageElement = new ElementType();
      languageElement.setValue(dclanguage);
      JAXBElement<ElementType> dcCoreLanguage = oaiDcObj.createLanguage(languageElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreLanguage);
    }
  }

  /**
   * <p>
   * Takes a string list containing the publishers and build the specific DublinCore element for
   * list entry. each
   * </p>
   * 
   * @param publishers
   */
  public void setPublisher(List<String> publishers) {
    for (String dcpublisher : publishers) {
      ElementType publisherElement = new ElementType();
      publisherElement.setValue(dcpublisher);
      JAXBElement<ElementType> dcCorePublisher = oaiDcObj.createPublisher(publisherElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCorePublisher);
    }
  }

  /**
   * <p>
   * Takes a string list containing the relations and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param relations
   */
  public void setRelation(List<String> relations) {
    for (String dcrelation : relations) {
      ElementType relationElement = new ElementType();
      relationElement.setValue(dcrelation);
      JAXBElement<ElementType> dcCoreRelation = oaiDcObj.createRelation(relationElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreRelation);
    }
  }

  /**
   * <p>
   * Takes a string list containing the rights and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param rights
   */
  public void setRights(List<String> rights) {
    for (String dcrights : rights) {
      ElementType rightsElement = new ElementType();
      rightsElement.setValue(dcrights);
      JAXBElement<ElementType> dcCoreRights = oaiDcObj.createRights(rightsElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreRights);
    }
  }

  /**
   * <p>
   * Takes a string list containing the sources and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param sources
   */
  public void setSource(List<String> sources) {
    for (String dcsource : sources) {
      ElementType sourcesElement = new ElementType();
      sourcesElement.setValue(dcsource);
      JAXBElement<ElementType> dcCoreSource = oaiDcObj.createSource(sourcesElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreSource);
    }
  }

  /**
   * <p>
   * Takes a string list containing the subjects and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param subjects
   */
  public void setSubject(List<String> subjects) {
    for (String dcsubject : subjects) {
      ElementType subjectElement = new ElementType();
      subjectElement.setValue(dcsubject);
      JAXBElement<ElementType> dcCoreSubject = oaiDcObj.createSubject(subjectElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreSubject);
    }
  }

  /**
   * <p>
   * Takes a string list containing the titles and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param titles
   */
  public void setTitle(List<String> titles) {
    for (String dctitle : titles) {
      ElementType titlesElement = new ElementType();
      titlesElement.setValue(dctitle);
      JAXBElement<ElementType> dcCoreTitle = oaiDcObj.createTitle(titlesElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreTitle);
    }
  }

  /**
   * <p>
   * Takes a string list containing the types and build the specific DublinCore element for each
   * list entry.
   * </p>
   * 
   * @param types
   */
  public void setType(List<String> types) {
    for (String dctype : types) {
      ElementType typeElement = new ElementType();
      typeElement.setValue(dctype);
      JAXBElement<ElementType> dcCoreType = oaiDcObj.createType(typeElement);
      this.tgMappedDC.getTitleOrCreatorOrSubject().add(dcCoreType);
    }
  }

  /**
   * <p>
   * Fetching DublinCore element and putting it into the metadata element for the OAIPMH record
   * response.
   * </p>
   * 
   * @return metadata element with the dublinCore values for a specific object
   */
  public MetadataType getDC() {
    this.metadata.setAny(oaiDcObj.createDc(this.tgMappedDC));

    return this.metadata;
  }

}

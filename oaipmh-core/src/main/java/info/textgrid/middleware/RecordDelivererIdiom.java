package info.textgrid.middleware;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.classicmayan.tools.ClassicMayanMetsMods;
import org.elasticsearch.common.Strings;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.MetadataType;
import info.textgrid.middleware.oaipmh.RecordType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
@Component
public class RecordDelivererIdiom extends RecordDelivererAbstract {

  // **
  // FINALS
  // **

  private static final String TEXTGRID_URI_PREFIX = "textgrid:";
  private static final String CREATED = "created";
  private static final String NOTES = "notes";
  private static final String LAST_MODIFIED = "lastModified";

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(RecordDelivererIdiom.class.getName());

  // **
  // CLASS
  // **

  private IdiomImages idiomImages;

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordDelivererIdiom(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   * <p>
   * Building the record XML object for the OAI-PMH response.
   * </p>
   * 
   * @throws ParseException
   * @throws IOException
   */
  @Override
  public GetRecordType getRecordById(final String id) throws ParseException, IOException {

    GetRecordType result = new GetRecordType();

    RecordType recordType = new RecordType();

    log.fine("incoming id: " + id);

    // Get revision URI if no revision URI is put in.
    // TODO Implement getLatestRevision in OAIPMHUtilities.getTextGridRevisionURI()!!
    // TODO We just implement it as it has been before! DO MAKE IT RIGHT SOON!!
    String textgridRevisionURI = OaipmhUtilities.getTextGridRevisionURI(id);

    log.fine("textgrid revision uri: " + textgridRevisionURI);

    String objectType = getIdiomInfoFromES(textgridRevisionURI, NOTES);
    String creationDate = getIdiomInfoFromES(textgridRevisionURI, CREATED);
    String modificationDate = getIdiomInfoFromES(textgridRevisionURI, LAST_MODIFIED);

    // We want UTC dates for MODS records, too!
    String creationDateUTC = OaipmhUtilities.getUTCDateAsString(creationDate);
    String modificationDateUTC = OaipmhUtilities.getUTCDateAsString(modificationDate);

    log.fine("object type: " + objectType);
    log.fine("textgrid creationDate: " + creationDate + " (" + creationDateUTC + ")");
    log.fine("textgrid modificationDate: " + modificationDate + " (" + modificationDateUTC + ")");

    // Get TG URI from ID.
    if (!textgridRevisionURI.startsWith(TEXTGRID_URI_PREFIX)) {
      textgridRevisionURI = TEXTGRID_URI_PREFIX + textgridRevisionURI;
    }

    // Get base URI for ClassicMayanMetsMods and response header here.
    String textgridBaseURI = OaipmhUtilities.getTextGridBaseURI(textgridRevisionURI);

    log.fine("textgrid base uri: " + textgridBaseURI);
    log.fine(objectType + " doc/dom: " + creationDate + "/" + modificationDate);

    if (objectType.equals("ARTEFACT")) {
      try {

        log.fine("calling ClassicMayanMetsMods for: " + textgridRevisionURI);

        ClassicMayanMetsMods metsmods = new ClassicMayanMetsMods(
            textgridBaseURI,
            creationDateUTC,
            modificationDateUTC);

        log.fine("metsmods title: " + metsmods.getArtefactTitle());
        log.fine("setting metsmods metadata");

        MetadataType metadata = idiomMets(metsmods);
        recordType.setMetadata(metadata);

        log.fine("metsmods metadata set");

      } catch (ParserConfigurationException | SAXException | ParseException e) {
        String message = "ERROR getting IDIOM METS record for ID " + textgridRevisionURI + "! "
            + e.getClass().getName() + ": " + e.getMessage();
        recordType.setMetadata(OaipmhUtilities.buildMETSErrorMetadata(message));
      }
    }

    else if (objectType.startsWith("ConedaKorMediumData")) {

      log.fine("calling idiom image getRecordById()");

      GetRecordType idi = this.idiomImages.getRecordById(textgridRevisionURI);

      // Fixes #64
      if (idi != null) {
        recordType = idi.getRecord();
      } else {
        String message = "ERROR getting IDIOM IMAGE record for ID " + textgridRevisionURI + "!";
        recordType.setMetadata(OaipmhUtilities.buildMETSErrorMetadata(message));
      }
    }

    // No setSpec needed here! It COULD be the IDIOM project ID, but we do not need it at the
    // moment.
    String setSpec = "";
    // We need to have the base URI here in header (and record), it is used as Record ID of Mayan
    // artifacts here!
    HeaderType header =
        OaipmhUtilities.computeResponseHeader(modificationDateUTC, textgridBaseURI, setSpec);
    recordType.setHeader(header);

    result.setRecord(recordType);

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Getting the METS/MODS object for the ClassicMayan artifact and put them into metadata element
   * of the OAI-PMH response.
   * </p>
   * 
   * @param theMetsMods
   * @return
   * @throws ParserConfigurationException
   * @throws JSONException
   * @throws SAXException
   * @throws IOException
   * @throws ParseException
   */
  private static MetadataType idiomMets(ClassicMayanMetsMods theMetsMods)
      throws ParserConfigurationException, JSONException, SAXException, IOException,
      ParseException {

    MetadataType metadataMets = new MetadataType();
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;

    builder = factory.newDocumentBuilder();
    Document doc = builder.parse(new InputSource(new StringReader(theMetsMods.getMets())));
    metadataMets.setAny(doc.getDocumentElement());

    return metadataMets;
  }

  /**
   * <p>
   * ElasticSearch request in non-public index to get search field values from TextGridRep.
   * </p>
   * 
   * @param idInDatabase
   * @param theSearchField
   * @return
   * @throws ParseException
   * @throws IOException
   */
  private String getIdiomInfoFromES(final String idInDatabase, final String theSearchField)
      throws ParseException, IOException {

    String result = "";

    String changedId = idInDatabase;

    // Remove prefix.
    if (idInDatabase.startsWith(TEXTGRID_URI_PREFIX)) {
      changedId = idInDatabase.substring(TEXTGRID_URI_PREFIX.length());
    }

    String[] searchField = {theSearchField};
    JSONObject json = new JSONObject();
    json =
        new JSONObject(OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient, changedId,
            searchField, Strings.EMPTY_ARRAY).getSource());

    result = OaipmhUtilities.firstEntryFieldLoader(json, theSearchField);

    return result;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public IdiomImages getIdiomImages() {
    return this.idiomImages;
  }

  /**
   * @param idiomImages
   */
  public void setIdiomImages(IdiomImages idiomImages) {
    this.idiomImages = idiomImages;
  }

}

package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.ListRecordsType;
import info.textgrid.middleware.oaipmh.RecordType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class RecordListDelivererDatacite extends RecordListDelivererAbstract {

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(RecordListDelivererDatacite.class.getName());
  protected static Map<String, Integer> cursorCollector = new Hashtable<String, Integer>();
  private RecordDelivererDatacite dataciteRecord;

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordListDelivererDatacite(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   *
   */
  @Override
  public ListRecordsType getRecords(String from, String to, String set, String resumptionToken)
      throws ParseException, IOException {

    ListRecordsType recordList = new ListRecordsType();

    List<String> URIList = getUriList(from, to, set, resumptionToken);
    for (String uri : URIList) {

      String eventuallyChangedUri = uri;

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        // We must remove the prefix, as ElasticSearch is storing the IDs without it.
        eventuallyChangedUri = uri.replace("textgrid:", "");
      }

      // **
      // DARIAH
      // **

      try {

        GetRecordType grt = this.dataciteRecord.getRecordById(eventuallyChangedUri);
        RecordType record = grt.getRecord();
        // Header is already set by getRecordById().

        recordList.getRecord().add(record);

      } catch (DatatypeConfigurationException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    recordList.setResumptionToken(getResTokenForResponse());

    return recordList;
  }

  /**
   * @param from
   * @param to
   * @param set
   * @param resumptionToken
   * @return
   */
  private List<String> getUriList(String from, String to, String set, String resumptionToken) {

    List<String> result = new ArrayList<String>();

    QueryBuilder query;
    QueryBuilder rangeQuery = QueryBuilders.rangeQuery(this.dateOfObjectCreation).from(from).to(to);
    QueryBuilder tgFilterSandBox = QueryBuilders.matchPhraseQuery("nearlyPublished", "true");

    if (set != null && !set.equals("openaire_data")) {

      String queryField = "";
      String valueField = "";

      if (this.dariah == true) {
        // TODO Put query field into config!
        queryField = "administrativeMetadata.dcterms:relation";
        valueField = set;

        // Add new record with set metadata in it (to also find collection metadata in Repository
        // Search!)
        // TODO Did we do this??
      }

      if (this.textgrid == true) {
        String[] setParts = set.split(":");
        if (setParts[0].equals("project")) {
          // TODO Put query field into config!
          queryField = "project.id";
          valueField = setParts[1];
        }
      }

      QueryBuilder matchQuery = QueryBuilders.matchPhraseQuery(queryField, valueField);
      if (this.textgrid) {
        query =
            QueryBuilders.boolQuery().must(rangeQuery).must(matchQuery).mustNot(tgFilterSandBox);
      } else {
        query = QueryBuilders.boolQuery().must(rangeQuery).must(matchQuery);
      }
    } else {
      if (this.textgrid) {
        query = QueryBuilders.boolQuery().must(rangeQuery).mustNot(tgFilterSandBox);
      } else {
        query = QueryBuilders.boolQuery().must(rangeQuery);
      }
    }
    result = getFieldsFromESIndex(query, resumptionToken, set);

    return result;
  }

  /**
   * @param query
   * @param resumptionToken
   * @param set
   * @return
   */
  private List<String> getFieldsFromESIndex(QueryBuilder query, String resumptionToken,
      String set) {

    List<String> uriList = new ArrayList<String>();

    QueryBuilder recordFilter;
    if (this.textgrid) {
      // We filter out all editions here!
      recordFilter = QueryBuilders.boolQuery().must(query)
          .must(QueryBuilders.matchPhraseQuery("format", this.formatToFilter));
    } else {
      // Do not filter at all in DH. We need every ID!
      recordFilter = QueryBuilders.boolQuery().must(query);
    }

    SearchRequest searchRequest = new SearchRequest(this.oaiEsClient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    searchSourceBuilder.query(recordFilter);
    searchSourceBuilder.size(this.searchResponseSize);
    searchRequest.source(searchSourceBuilder);

    SearchResponse scrollResp = null;
    // SearchResponse scrollResp = new SearchResponse(null);
    try {
      if (resumptionToken != null) {
        SearchScrollRequest scrollRequest = new SearchScrollRequest(resumptionToken);
        scrollRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
        scrollResp = this.oaiEsClient.getEsClient().scroll(scrollRequest, RequestOptions.DEFAULT);
      } else {
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
        scrollResp = this.oaiEsClient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
      }
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    // TODO Check if scrollResp == null and what to do then!

    String scrollID = scrollResp.getScrollId();
    long completeListSize = scrollResp.getHits().getTotalHits().value;
    setResultSize(completeListSize);

    if (completeListSize > 0) {

      setFoundItems(true);
      for (SearchHit hit : scrollResp.getHits().getHits()) {
        if (hit != null && hit.getFields() != null) {
          String id2add;
          // TODO Could we not use hit.getId() also for TG hits? Where is the difference?
          // TODO See:
          // TODO hit.getId(): xj5w.0
          // TODO hit.getSourceAsMap(): textgrid:xj5w.0
          if (this.textgrid) {

            log.fine("  ####  hit.getId(): " + hit.getId());

            id2add = hit.getSourceAsMap().get(this.identifierField).toString();

            log.fine("  ####  hit.getSourceAsMap(): " + id2add);

          } else {
            id2add = hit.getId();
          }

          uriList.add(id2add);
        }
      }
      if (resumptionToken != null
          && this.resTokenForResponse.getCursor().intValue() >= completeListSize) {
        try {
          cursorCollector.remove(resumptionToken);
        } catch (NullPointerException couldNotRemove) {
          log.severe("could not remove hash value: " + resumptionToken + " from hash map");
        }
        this.resTokenForResponse.setValue("");
      } else {
        this.resTokenForResponse = OaipmhUtilities.getResumptionToken(completeListSize,
            resumptionToken, cursorCollector, scrollID, this.searchResponseSize);
      }

    } else {
      setFoundItems(false);
    }

    return uriList;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public RecordDelivererDatacite getDataciteRecord() {
    return this.dataciteRecord;
  }

  /**
   * @param dataciteRecord
   */
  public void setDataciteRecord(RecordDelivererDatacite dataciteRecord) {
    this.dataciteRecord = dataciteRecord;
  }

}

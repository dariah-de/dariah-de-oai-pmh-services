package info.textgrid.middleware;

import java.util.ArrayList;
import java.util.List;
import info.textgrid.middleware.oaipmh.RequestType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-09-07
 * @since 2019-03-07
 */
public abstract class RecordDelivererAbstract implements RecordDelivererInterface {

  // **
  // FINALS
  // **

  protected static final String ERROR_NO_TG_EDITION =
      "Requested object is not a textgrid edition: ";

  // **
  // CLASS
  // **

  protected boolean textgrid;
  protected boolean dariah;

  protected OaipmhElasticSearchClient oaiEsClient;

  protected String[] fields;
  protected String[] workFields;

  protected String formatField;
  protected String formatToFilter;
  protected String dateOfObjectCreation;
  protected String relationToFurtherMetadataObject;
  protected String repositoryObjectURIPrefix;
  protected String identifierField;
  protected String specField;
  protected String specFieldPrefix;

  // Lists for the Dublin Core elements. Lists are needed since each DC fields is possible to occur
  // several times.
  protected String[] contributorList;
  protected String[] coverageList;
  protected String[] creatorList;
  protected String[] dateList;
  protected String[] descriptionList;
  protected String[] formatList;
  protected String[] identifierList;
  protected String[] languageList;
  protected String[] publisherList;
  protected String[] relationList;
  protected String[] rightsList;
  protected String[] sourceList;
  protected String[] subjectList;
  protected String[] titleList;
  protected String[] typeList;

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordDelivererAbstract(boolean textgrid, boolean dariah) {
    this.textgrid = textgrid;
    this.dariah = dariah;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.RecordDelivererInterface#requestChecker(info.textgrid.middleware.
   * oaipmh.RequestType)
   */
  public static ErrorHandler requestChecker(RequestType request) {

    ErrorHandler result = new ErrorHandler();

    // Check if metadata prefix is existing and valid.
    if (request.getMetadataPrefix() != null
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OAIDC_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
      result.setError(OaipmhConstants.OAI_METADATA_FORMAT_ERROR,
          "The value of the metadataPrefix: " + request.getMetadataPrefix()
              + " is not supported by the item identified by the value of: "
              + request.getIdentifier());
    }

    // Check if other params are existing, they shouldn't!
    else {

      List<String> errorValues = new ArrayList<String>();
      if (request.getFrom() != null) {
        errorValues.add("from");
      }
      if (request.getResumptionToken() != null) {
        errorValues.add("resumptionToken");
      }
      if (request.getSet() != null) {
        errorValues.add("set");
      }
      if (request.getUntil() != null) {
        errorValues.add("until");
      }
      if (request.getMetadataPrefix() == null) {
        errorValues.add("metadataPrefix");
      }
      if (request.getIdentifier() == null) {
        errorValues.add("identifier");
      }
      if (errorValues.size() > 0) {
        result.setError(OaipmhConstants.OAI_BAD_ARGUMENT, "The request includes illegal arguments "
            + "or is missing required arguments: " + errorValues);
      }
    }

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @param formatField
   */
  public void setFormatField(String formatField) {
    this.formatField = formatField;
  }

  /**
   * @return
   */
  public String getDateOfObjectCreation() {
    return this.dateOfObjectCreation;
  }

  /**
   * @param dateOfObjectCreation
   */
  public void setDateOfObjectCreation(String dateOfObjectCreation) {
    this.dateOfObjectCreation = dateOfObjectCreation;
  }

  /**
   * @return
   */
  public String getRelationToFurtherMetadataObject() {
    return this.relationToFurtherMetadataObject;
  }

  /**
   * @param relationToFurtherMetadataObject
   */
  public void setRelationToFurtherMetadataObject(String relationToFurtherMetadataObject) {
    this.relationToFurtherMetadataObject = relationToFurtherMetadataObject;
  }

  /**
   * @return
   */
  public String getRepositoryObjectURIPrefix() {
    return this.repositoryObjectURIPrefix;
  }

  /**
   * @param repositoryObjectURIPrefix
   */
  public void setRepositoryObjectURIPrefix(String repositoryObjectURIPrefix) {
    this.repositoryObjectURIPrefix = repositoryObjectURIPrefix;
  }

  /**
   * @return
   */
  public String getIdentifierField() {
    return this.identifierField;
  }

  /**
   * @param identifierPrefix
   */
  public void setIdentifierField(String identifierPrefix) {
    this.identifierField = identifierPrefix;
  }

  /**
   * @param contributorList
   */
  public void setContributor(String[] contributorList) {
    this.contributorList = contributorList;
  }

  /**
   * @return
   */
  public String[] getContributor() {
    return this.contributorList;
  }

  /**
   * @param coveragesList
   */
  public void setCoverage(String[] coveragesList) {
    this.coverageList = coveragesList;
  }

  /**
   * @param creatorsList
   */
  public void setCreator(String[] creatorsList) {
    this.creatorList = creatorsList;
  }

  /**
   * @param datesList
   */
  public void setDates(String[] datesList) {
    this.dateList = datesList;
  }

  /**
   * @param descriptionsList
   */
  public void setDescriptions(String[] descriptionsList) {
    this.descriptionList = descriptionsList;
  }

  /**
   * @param formatsList
   */
  public void setFormats(String[] formatsList) {
    this.formatList = formatsList;
  }

  /**
   * @param identifiersList
   */
  public void setIdentifiers(String[] identifiersList) {
    this.identifierList = identifiersList;
  }

  /**
   * @param languagesList
   */
  public void setLanguages(String[] languagesList) {
    this.languageList = languagesList;
  }

  /**
   * @param publishersList
   */
  public void setPublishers(String[] publishersList) {
    this.publisherList = publishersList;
  }

  /**
   * @param relationsList
   */
  public void setRelations(String[] relationsList) {
    this.relationList = relationsList;
  }

  /**
   * @param rightsList
   */
  public void setRights(String[] rightsList) {
    this.rightsList = rightsList;
  }

  /**
   * @param sourcesList
   */
  public void setSources(String[] sourcesList) {
    this.sourceList = sourcesList;
  }

  /**
   * @param subjectsList
   */
  public void setSubjects(String[] subjectsList) {
    this.subjectList = subjectsList;
  }

  /**
   * @param titlesList
   */
  public void setTitles(String[] titlesList) {
    this.titleList = titlesList;
  }

  /**
   * @param typesList
   */
  public void setTypes(String[] typesList) {
    this.typeList = typesList;
  }

  /**
   * @return
   */
  public OaipmhElasticSearchClient getOaiEsClient() {
    return this.oaiEsClient;
  }

  /**
   * @param oaiEsClient
   */
  public void setOaiEsClient(OaipmhElasticSearchClient oaiEsClient) {
    this.oaiEsClient = oaiEsClient;
  }

  /**
   * @return
   */
  public String[] getWorkFields() {
    return this.workFields;
  }

  /**
   * @param workFields
   */
  public void setWorkFields(String[] workFields) {
    this.workFields = workFields;
  }

  /**
   * @param fields
   */
  public void setFields(String[] fields) {
    this.fields = fields;
  }

  /**
   * @return
   */
  public String[] getFields() {
    return this.fields;
  }

  /**
   * @return
   */
  public String getSpecField() {
    return this.specField;
  }

  /**
   * @param specField
   */
  public void setSpecField(String specField) {
    this.specField = specField;
  }

  /**
   * @return
   */
  public String getSpecFieldPrefix() {
    return this.specFieldPrefix;
  }

  /**
   * @param specFieldPrefix
   */
  public void setSpecFieldPrefix(String specFieldPrefix) {
    this.specFieldPrefix = specFieldPrefix;
  }

}

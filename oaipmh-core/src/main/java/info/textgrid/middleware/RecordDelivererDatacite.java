package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.common.Strings;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.oaipmh.ContributorType;
import info.textgrid.middleware.oaipmh.DateType;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.OpenaireDescriptionType;
import info.textgrid.middleware.oaipmh.RelatedIdentifierType;
import info.textgrid.middleware.oaipmh.RelationType;
import info.textgrid.middleware.oaipmh.Resource;
import info.textgrid.middleware.oaipmh.Resource.AlternateIdentifiers;
import info.textgrid.middleware.oaipmh.Resource.AlternateIdentifiers.AlternateIdentifier;
import info.textgrid.middleware.oaipmh.Resource.Contributors;
import info.textgrid.middleware.oaipmh.Resource.Contributors.Contributor;
import info.textgrid.middleware.oaipmh.Resource.Creators;
import info.textgrid.middleware.oaipmh.Resource.Creators.Creator;
import info.textgrid.middleware.oaipmh.Resource.Creators.Creator.NameIdentifier;
import info.textgrid.middleware.oaipmh.Resource.Dates;
import info.textgrid.middleware.oaipmh.Resource.Dates.Date;
import info.textgrid.middleware.oaipmh.Resource.Descriptions;
import info.textgrid.middleware.oaipmh.Resource.Descriptions.Description;
import info.textgrid.middleware.oaipmh.Resource.Formats;
import info.textgrid.middleware.oaipmh.Resource.GeoLocations;
import info.textgrid.middleware.oaipmh.Resource.GeoLocations.GeoLocation;
import info.textgrid.middleware.oaipmh.Resource.Identifier;
import info.textgrid.middleware.oaipmh.Resource.RelatedIdentifiers;
import info.textgrid.middleware.oaipmh.Resource.RelatedIdentifiers.RelatedIdentifier;
import info.textgrid.middleware.oaipmh.Resource.ResourceType;
import info.textgrid.middleware.oaipmh.Resource.RightsList;
import info.textgrid.middleware.oaipmh.Resource.RightsList.Rights;
import info.textgrid.middleware.oaipmh.Resource.Sizes;
import info.textgrid.middleware.oaipmh.Resource.Subjects;
import info.textgrid.middleware.oaipmh.Resource.Subjects.Subject;
import info.textgrid.middleware.oaipmh.Resource.Titles;
import info.textgrid.middleware.oaipmh.Resource.Titles.Title;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
@Component
public class RecordDelivererDatacite extends RecordDelivererAbstract {

  // **
  // FINALS
  // **

  private static final String TG_PROJECT_CONTRIBUTOR_SCHEME = "textgrid";
  private static final String TG_PROJECT_CONTRIBUTOR_SCHEME_URI =
      "https://textgridrep.org/project/";
  private static final String ERROR_NO_DATA_CAN_BE_RETRIEVED =
      "No data could be retrieved from ElasticSearch for ID: ";
  private static final String BYTES = "Bytes";
  private static final String DOT_VALUE = ".value";
  private static final String DOT_TYPE = ".type";
  private static final String DOT_ID = ".id";
  private static final String LICENSE_URI = ".licenseUri";
  private static final String EU_OPEN_ACCESS = "info:eu-repo/semantics/openAccess";
  private static final String ID_TYPE_HANDLE = "Handle";
  private static final String ID_TYPE_DOI = "DOI";
  private static final String ID_TYPE_URI = "URI";
  private static final String ID_TYPE_OTHER = "Other";
  private static final String PND = "PND";
  private static final String GND = "GND";
  private static final String GND_SCHEME = "https://d-nb.info/gnd/";
  private static final String VIAF = "VIAF";
  private static final String VIAF_SCHEME = "https://viaf.org/viaf/";
  private static final String ORCID = "ORCID";
  private static final String ORCID_SCHEME = "https://orcid.org/";

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(RecordDelivererDatacite.class.getName());

  // **
  // PRIVATES
  // **

  // Container object for the result of the elasticsearch request. This object contains the source
  // of the response.
  private JSONObject jsonObj = new JSONObject();

  // This variables contains the field requested in the ElasticSearch Index. The content will be
  // given by a configuration file.
  private String oarIdentifierField;
  private String oarSizeField;
  private String relationToWorkObject;
  private String handle;

  private String[] oarTitleFields;
  private String[] oarDateFields;
  private String[] oarContributorFields;
  private String[] oarCreatorFields;
  private String[] oarLanguageFields;
  private String[] oarAlternateIdentifierFields;
  private String[] oarFormatFields;
  private String[] oarRightsFields;
  private String[] oarDescriptionFields;
  private String[] oarRelatedIdentifierFields;
  private String[] oarGeoLocationFields;
  private String[] oarVersionFields;
  private String[] oarSubjectFields;

  /**
   * @param textgrid flag to indicate that the content of the record is context of the TextGrid
   *        Repository.
   * @param dariah flag to indicate that the content of the record is context of the DARIAH
   *        Repository.
   * @throws IOException
   */
  public RecordDelivererDatacite(boolean textgrid, boolean dariah) throws IOException {
    super(textgrid, dariah);
  }

  /**
   *
   */
  @Override
  public GetRecordType getRecordById(final String idInElasticSearchIndex)
      throws ParseException, DatatypeConfigurationException, IOException {

    String replacedID = idInElasticSearchIndex.replace(this.repositoryObjectURIPrefix, "");
    GetRecordType getRecordType = new GetRecordType();

    GetResponse esResultObject = OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient,
        replacedID, this.fields, Strings.EMPTY_ARRAY);

    // **
    // TEXTGRID
    // **

    if (this.textgrid == true) {
      // Error: We have got no Edition here!
      if (!esResultObject.getSourceAsMap().get(this.formatField).equals(this.formatToFilter)) {
        String message = ERROR_NO_TG_EDITION + idInElasticSearchIndex;
        throw new IOException(message);
      }
    }

    // Get ES object.
    this.jsonObj = new JSONObject(esResultObject.getSource());

    if (this.jsonObj == null || this.jsonObj.isEmpty()) {
      throw new IOException(ERROR_NO_DATA_CAN_BE_RETRIEVED + idInElasticSearchIndex);
    }

    // Set response header.
    String datestamp = OaipmhUtilities.getUTCDateAsString(
        OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.dateOfObjectCreation));

    String identifier = "";
    if (!idInElasticSearchIndex.startsWith(this.repositoryObjectURIPrefix)) {
      identifier = this.repositoryObjectURIPrefix + idInElasticSearchIndex;
    } else {
      identifier = idInElasticSearchIndex;
    }
    String setSpec = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.specField);
    String setSpecValue = OaipmhUtilities.getSetSpec(setSpec, this.specFieldPrefix, identifier);

    HeaderType header = OaipmhUtilities.computeResponseHeader(datestamp, identifier, setSpecValue);
    getRecordType =
        OaipmhUtilities.getRecordType(this.setOpenAireRecord(), header);

    return getRecordType;
  }

  /**
   * <p>
   * Adding the openaire record fields to the openaire record object.
   * </p>
   * 
   * @return
   * @throws ParseException
   * @throws DatatypeConfigurationException
   * @throws IOException
   */
  private Resource setOpenAireRecord()
      throws ParseException, DatatypeConfigurationException, IOException {

    Resource result = new Resource();

    Titles titles = addTitles();
    if (titles.getTitle() != null && !titles.getTitle().isEmpty()) {
      result.setTitles(titles);
    }
    String publisher = addPublisher();
    if (publisher != null && !publisher.isEmpty()) {
      result.setPublisher(publisher);
    }
    String publicationYear = addPublicationYear();
    if (publicationYear != null && !publicationYear.isEmpty()) {
      result.setPublicationYear(publicationYear);
    }
    Dates dates = this.addDates();
    if (dates.getDate() != null && !dates.getDate().isEmpty()) {
      result.setDates(dates);
    }
    Contributors contributors = addContributors();
    if (contributors.getContributor() != null && !contributors.getContributor().isEmpty()) {
      result.setContributors(contributors);
    }
    String language = addLanguage();
    if (language != null && !language.isEmpty()) {
      result.setLanguage(language);
    }
    AlternateIdentifiers alternateIdentifiers = addAlternateIdentifiers();
    if (alternateIdentifiers.getAlternateIdentifier() != null
        && !alternateIdentifiers.getAlternateIdentifier().isEmpty()) {
      result.setAlternateIdentifiers(alternateIdentifiers);
    }
    Formats formats = addFormats();
    if (formats.getFormat() != null && !formats.getFormat().isEmpty()) {
      result.setFormats(formats);
    }
    RightsList rights = addRights();
    if (rights.getRights() != null && !rights.getRights().isEmpty()) {
      result.setRightsList(rights);
    }
    Descriptions descriptions = addDescriptions();
    if (descriptions.getDescription() != null && !descriptions.getDescription().isEmpty()) {
      result.setDescriptions(descriptions);
    }
    RelatedIdentifiers relatedIdentifiers = addRelatedIdentifiers();
    if (relatedIdentifiers.getRelatedIdentifier() != null
        && !relatedIdentifiers.getRelatedIdentifier().isEmpty()) {
      result.setRelatedIdentifiers(relatedIdentifiers);
    }
    Identifier identifier = addIdentifier();
    if (identifier.getValue() != null && !identifier.getValue().isEmpty()) {
      result.setIdentifier(identifier);
    }
    Creators creators = addCreators();
    if (creators.getCreator() != null && !creators.getCreator().isEmpty()) {
      result.setCreators(creators);
    }
    GeoLocations geoLocations = addGeoLocation();
    if (geoLocations.getGeoLocation() != null && !geoLocations.getGeoLocation().isEmpty()) {
      result.setGeoLocations(geoLocations);
    }
    ResourceType resourceType = addResourceType();
    if (resourceType.getValue() != null && !resourceType.getValue().isEmpty()) {
      result.setResourceType(resourceType);
    }
    String version = addVersion();
    if (version != null && !version.isEmpty()) {
      result.setVersion(version);
    }
    Subjects subjects = addSubjects();
    if (subjects.getSubject() != null && !subjects.getSubject().isEmpty()) {
      result.setSubjects(subjects);
    }
    Sizes sizes = addSizes();
    if (sizes.getSize() != null && !sizes.getSize().isEmpty()) {
      result.setSizes(sizes);
    }

    return result;
  }

  /**
   * @return
   */
  private Sizes addSizes() {

    Sizes sizes = new Sizes();

    String size =
        OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.oarSizeField) + " " + BYTES;
    sizes.getSize().add(size);

    return sizes;
  }

  /**
   * @return
   */
  private Subjects addSubjects() {

    Subjects subjects = new Subjects();

    // Loop subject fields.
    for (String subjectField : this.oarSubjectFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        Subject subject = new Subject();
        String value =
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, subjectField + DOT_VALUE);
        if (!value.isEmpty()) {
          subject.setValue(value);
        }
        String scheme =
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, subjectField + DOT_ID + DOT_VALUE);
        if (!scheme.isEmpty()) {
          subject.setSubjectScheme(scheme);
        }
        String schemeURI =
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, subjectField + DOT_ID + DOT_TYPE);
        if (schemeURI.isEmpty()) {
          subject.setSchemeURI(schemeURI);
        }
        subjects.getSubject().add(subject);
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> sList = OaipmhUtilities.listFieldLoader(this.jsonObj, subjectField);
        for (String s : sList) {
          Subject subject = new Subject();
          if (!s.isEmpty()) {
            subject.setValue(s);
          }
          // TODO Extract subject schema and schema URI somehow? Set accordingly like in TG part
          // above.
          subjects.getSubject().add(subject);
        }
      }
    }

    return subjects;
  }

  /**
   * @return
   */
  private String addVersion() {

    String version = "";

    for (String versionField : this.oarVersionFields) {
      version = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, versionField);
    }

    return version;
  }

  /**
   * @return
   */
  private ResourceType addResourceType() {

    ResourceType resourceType = new ResourceType();

    // Loop format fields.
    for (String format : this.oarFormatFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        String resourceValue = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, format);
        resourceType.setValue(resourceValue);
        // Set collection for all aggregation types, take also into account: images and text.
        if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
          resourceType
              .setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.COLLECTION);
        } else if (TextGridMimetypes.IMAGE_SET.contains(format)) {
          resourceType
              .setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.IMAGE);
        } else if (TextGridMimetypes.ORIGINAL_SET.contains(format)) {
          resourceType
              .setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.TEXT);
        }
        // Use dataset for everything else.
        else {
          resourceType.setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.DATASET);
        }
        // We take only the first of all the resource types!
        if (resourceType.getValue() != null && !resourceType.getValue().isEmpty()) {
          break;
        }
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        String resourceValue = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, format);
        resourceType.setValue(resourceValue);
        // Set collection for DARIAH collection type here, data object for everything else.
        if (TextGridMimetypes.DARIAH_COLLECTION.equals(format)) {
          resourceType
              .setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.COLLECTION);
        }
        // Use dataset for everything else.
        else {
          resourceType.setResourceTypeGeneral(info.textgrid.middleware.oaipmh.ResourceType.DATASET);
        }
        // We take only the first of all the resource types!
        if (resourceType.getValue() != null && !resourceType.getValue().isEmpty()) {
          break;
        }
      }
    }

    return resourceType;
  }

  /**
   * 
   */
  private GeoLocations addGeoLocation() {

    GeoLocations geoLocations = new GeoLocations();

    for (String geoLocationField : this.oarGeoLocationFields) {
      GeoLocation geoLocation = new GeoLocation();
      String place = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, geoLocationField);
      geoLocation.setGeoLocationPlace(place);
      if (!place.isEmpty()) {
        geoLocations.getGeoLocation().add(geoLocation);
      }
    }

    return geoLocations;
  }

  /**
   * @return
   * @throws IOException
   */
  private RelatedIdentifiers addRelatedIdentifiers() throws IOException {

    RelatedIdentifiers relatedIdentifiers = new RelatedIdentifiers();

    // Loop related identifier fields.
    for (String relatedIdentifierField : this.oarRelatedIdentifierFields) {

      // **
      // TextGrid
      // *

      if (this.textgrid) {
        String uriForWork =
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, relatedIdentifierField)
                .replace(this.repositoryObjectURIPrefix, "");
        if (uriForWork != null && !uriForWork.isEmpty()) {

          log.fine("looking for work with id " + uriForWork);

          try {
            JSONObject resultOfFurtherObject =
                new JSONObject(OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient,
                    uriForWork, this.workFields, Strings.EMPTY_ARRAY).getSource());

            RelatedIdentifier relatedIdentifier = new RelatedIdentifier();
            relatedIdentifier.setRelatedIdentifierType(RelatedIdentifierType.HANDLE);
            // relatedIdentifier.setRelatedMetadataScheme(TGConstants.TEXTGRID_METADATASCHEME);
            relatedIdentifier.setRelationType(RelationType.IS_PART_OF);
            // relatedIdentifier.setSchemeType("XSD");
            // relatedIdentifier.setSchemeURI(TGConstants.TEXTGRID_METADATASCHEME_URI);
            relatedIdentifier.setValue(
                OaipmhUtilities.firstEntryFieldLoader(resultOfFurtherObject, this.handle));
            relatedIdentifiers.getRelatedIdentifier().add(relatedIdentifier);
          } catch (IOException e) {
            log.warning("work with id " + uriForWork + " does not exist in elasticsearch! "
                + e.getMessage());
          }
        }
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> relatedIdentiferList =
            OaipmhUtilities.listFieldLoader(this.jsonObj, relatedIdentifierField);
        for (String i : relatedIdentiferList) {
          RelatedIdentifier relatedID = new RelatedIdentifier();
          if (i.startsWith(RDFConstants.HDL_PREFIX)) {
            relatedID.setRelatedIdentifierType(RelatedIdentifierType.HANDLE);
            relatedID.setValue(omitHdlPrefix(i));
          } else if (i.startsWith(RDFConstants.DOI_PREFIX)) {
            relatedID.setRelatedIdentifierType(RelatedIdentifierType.DOI);
            relatedID.setValue(omitDoiPrefix(i));
          } else if (i.startsWith("http")) {
            relatedID.setRelatedIdentifierType(RelatedIdentifierType.URL);
          }
          // TODO Check other prefixes and add other values, too?
          else {
            // TODO No type OTHER existing! Value is mandatory, so we chose URL, what else can we
            // do?
            relatedID.setRelatedIdentifierType(RelatedIdentifierType.URL);
            relatedID.setValue(i);
          }
          // Relation type is REFERENCES for the time being (coming from dc:relation at the moment).
          // TODO Check if we can use isPartOf for subcollections and sub objects!
          relatedID.setRelationType(RelationType.REFERENCES);
          relatedIdentifiers.getRelatedIdentifier().add(relatedID);
        }
      }
    }

    return relatedIdentifiers;
  }

  /**
   * @return
   * @throws IOException
   */
  private Descriptions addDescriptions() throws IOException {

    Descriptions descriptions = new Descriptions();

    // Loop description fields.
    for (String descriptionField : this.oarDescriptionFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        String idForWorkObject =
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.relationToWorkObject);
        if (idForWorkObject != null && !idForWorkObject.isEmpty()) {
          idForWorkObject = idForWorkObject.replace(this.repositoryObjectURIPrefix, "");

          log.fine("looking for work with uri " + idForWorkObject);

          try {
            JSONObject resultOfFurtherObject =
                new JSONObject(OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient,
                    idForWorkObject, this.workFields, Strings.EMPTY_ARRAY).getSource());

            // Create abstract for every dc:description.
            List<String> abstractsFromWork =
                OaipmhUtilities.listFieldLoader(resultOfFurtherObject, descriptionField);

            log.fine("found abstracts: " + abstractsFromWork.toString());

            for (String d : abstractsFromWork) {
              Description description = new Description();
              description.setDescriptionType(OpenaireDescriptionType.ABSTRACT);
              description.getContent().add(d);
              descriptions.getDescription().add(description);
            }
          } catch (IOException e) {
            log.warning("work with id " + idForWorkObject + " does not exist in elasticsearch! "
                + e.getMessage());
          }
        }
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> descriptionsFromMetadata =
            OaipmhUtilities.listFieldLoader(this.jsonObj, descriptionField);
        // Create abstract for every dc:description.
        for (String d : descriptionsFromMetadata) {
          Description description = new Description();
          description.setDescriptionType(OpenaireDescriptionType.ABSTRACT);
          description.getContent().add(d);
          descriptions.getDescription().add(description);
        }
      }
    }

    return descriptions;
  }

  /**
   * @return
   */
  private RightsList addRights() {

    RightsList rightsResultList = new RightsList();

    // Loop rights fields.
    for (String rightsField : this.oarRightsFields) {

      // **
      // Textgrid
      // **

      if (this.textgrid) {
        Rights rights = new Rights();
        rights.setRightsURI(
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, rightsField + LICENSE_URI));
        rights.setValue(
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, rightsField + DOT_VALUE));
        rightsResultList.getRights().add(rights);
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> rList = OaipmhUtilities.listFieldLoader(this.jsonObj, rightsField);
        for (String r : rList) {
          Rights rights = new Rights();
          // Set rights URI assuming every "http://" or "https://" really IS an URI.
          if (r.startsWith("http")) {
            rights.setRightsURI(r);
          }
          // Set value in every case, even if doubled with URI.
          rights.setValue(r);

          rightsResultList.getRights().add(rights);
        }
      }
    }

    // Set mandatory if applicable OpenAIRE rights tag!
    if (rightsResultList.getRights().get(0).getValue() != null) {
      Rights openAccesRight = new Rights();
      openAccesRight.setRightsURI(EU_OPEN_ACCESS);
      rightsResultList.getRights().add(openAccesRight);
    }

    return rightsResultList;
  }

  /**
   * @return
   */
  private Formats addFormats() {

    Formats formats = new Formats();

    // Loop format fields.
    for (String format : this.oarFormatFields) {
      formats.getFormat().addAll(OaipmhUtilities.listFieldLoader(this.jsonObj, format));
    }

    return formats;
  }

  /**
   * @return
   */
  private AlternateIdentifiers addAlternateIdentifiers() {

    AlternateIdentifiers alternateIdentifiers = new AlternateIdentifiers();

    // Loop alternate identifiers.
    for (String alternateIdentifierField : this.oarAlternateIdentifierFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        AlternateIdentifier alternateIdentifier = new AlternateIdentifier();
        alternateIdentifier.setValue(
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, alternateIdentifierField));
        alternateIdentifier.setAlternateIdentifierType(ID_TYPE_URI);
        alternateIdentifiers.getAlternateIdentifier().add(alternateIdentifier);
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> alternateIdentifierList =
            OaipmhUtilities.listFieldLoader(this.jsonObj, alternateIdentifierField);
        for (String i : alternateIdentifierList) {
          AlternateIdentifier alternateID = new AlternateIdentifier();
          if (i.startsWith(RDFConstants.HDL_PREFIX)) {
            alternateID.setAlternateIdentifierType(ID_TYPE_HANDLE);
            alternateID.setValue(omitHdlPrefix(i));
          } else if (i.startsWith(RDFConstants.DOI_PREFIX)) {
            alternateID.setAlternateIdentifierType(ID_TYPE_DOI);
            alternateID.setValue(omitDoiPrefix(i));
          } else {
            alternateID.setAlternateIdentifierType(ID_TYPE_OTHER);
            alternateID.setValue(i);
          }
          alternateIdentifiers.getAlternateIdentifier().add(alternateID);
        }
      }
    }

    return alternateIdentifiers;
  }

  /**
   * @return
   */
  private String addLanguage() {

    String language = "";

    // TODO Only ONE language is permitted in Datacite schema? What if we have more then one in
    // DARIAH DC data? Take only first one for the moment!
    for (String languageField : this.oarLanguageFields) {
      List<String> langs = OaipmhUtilities.listFieldLoader(this.jsonObj, languageField);
      if (!langs.isEmpty()) {
        language = langs.get(0);
      }
    }

    return language;
  }

  /**
   * @return
   */
  private Contributors addContributors() {

    Contributors contributors = new Contributors();

    // Loop contributor fields.
    for (String contributorField : this.oarContributorFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        Contributor contributor = new Contributor();
        if (contributorField.equals("project")) {
          String fieldContent =
              OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, contributorField + DOT_VALUE);
          if (fieldContent != null && !fieldContent.isEmpty()) {
            contributor.setContributorName(fieldContent);
            contributor.setContributorType(ContributorType.OTHER);
            info.textgrid.middleware.oaipmh.Resource.Contributors.Contributor.NameIdentifier name =
                new info.textgrid.middleware.oaipmh.Resource.Contributors.Contributor.NameIdentifier();
            name.setNameIdentifierScheme(TG_PROJECT_CONTRIBUTOR_SCHEME);
            name.setValue(
                OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, contributorField + DOT_ID));
            name.setSchemeURI(TG_PROJECT_CONTRIBUTOR_SCHEME_URI);
            if (!name.getValue().isEmpty() && !name.getNameIdentifierScheme().isEmpty()
                && !name.getSchemeURI().isEmpty()) {
              contributor.setNameIdentifier(name);
            }
            contributors.getContributor().add(contributor);
          }
        } else {
          String fieldContent =
              OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, contributorField);
          if (fieldContent != null && !fieldContent.isEmpty()) {
            contributor.setContributorName(fieldContent);
            contributor.setContributorType(ContributorType.DATA_MANAGER);
            contributors.getContributor().add(contributor);

            // info.textgrid.middleware.oaipmh.Resource.Contributors.Contributor.NameIdentifier name
            // =
            // new
            // info.textgrid.middleware.oaipmh.Resource.Contributors.Contributor.NameIdentifier();
            // name.setNameIdentifierScheme("textgrid");
            // name.setValue("");
            // name.setSchemeURI("http://www.textgridlab.org/schema/textgrid-metadata_2010.xsd");
            // contributorInOpenAireRecord.setNameIdentifier(name);
          }
        }
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> contributorsResultList =
            OaipmhUtilities.listFieldLoader(this.jsonObj, contributorField);
        for (String i : contributorsResultList) {
          Contributor contributor = new Contributor();
          if (i != null && !i.isEmpty()) {
            contributor.setContributorName(i);
            if (contributorField.equals("administrativeMetadata.dcterms:creator")) {
              contributor.setContributorType(ContributorType.DATA_MANAGER);
            } else {
              contributor.setContributorType(ContributorType.OTHER);
            }
            contributors.getContributor().add(contributor);
          }
        }
      }
    }

    return contributors;
  }

  /**
   * @return
   */
  private Dates addDates() {

    Dates dates = new Dates();

    for (String dateField : this.oarDateFields) {

      Date dateInOpenAireRecord = new Date();

      // Only one value per field is needed here (I think).
      // We need ISO8601 here, take entries as they come from ElasticSearch!
      String dateValue = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, dateField);
      dateInOpenAireRecord.setValue(dateValue);

      // TODO Use extra created, issued, and updated fields in configuration!

      // Filter TextGrid values for date type.
      if (dateField.equals("created")) {
        dateInOpenAireRecord.setDateType(DateType.CREATED);
      }
      if (dateField.equals("issued")) {
        dateInOpenAireRecord.setDateType(DateType.ISSUED);
      }
      if (dateField.equals("lastModified")) {
        dateInOpenAireRecord.setDateType(DateType.UPDATED);
      }

      // Filter DARIAH values for date type.
      if (dateField.equals("administrativeMetadata.dcterms:created")) {
        dateInOpenAireRecord.setDateType(DateType.CREATED);
      }
      if (dateField.equals("administrativeMetadata.dcterms:modified")) {
        dateInOpenAireRecord.setDateType(DateType.UPDATED);
      }

      dates.getDate().add(dateInOpenAireRecord);
    }

    return dates;
  }

  /**
   * @return
   */
  private Identifier addIdentifier() {

    Identifier identifier = new Identifier();
    String idValue = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.oarIdentifierField);

    // **
    // TextGrid
    // **

    if (this.textgrid) {
      // We must set Handle here for TG, cause we do not have a DOI! So this is not valid due to
      // Datacite schema validation!
      if (idValue != null && !idValue.isEmpty()) {
        identifier.setIdentifierType(ID_TYPE_HANDLE);
        identifier.setValue(omitHdlPrefix(idValue));
      }
    }

    // **
    // DARIAH
    // **

    else if (this.dariah) {
      // We set DOI for DH, 'cause we HAVE it! :-D
      identifier.setIdentifierType(ID_TYPE_DOI);
      identifier.setValue(omitDoiPrefix(idValue));
    }

    return identifier;
  }

  /**
   * @return
   */
  private Creators addCreators() {

    Creators creators = new Creators();

    // Loop creator fields.
    for (String creatorField : this.oarCreatorFields) {

      // **
      // TextGrid
      // **

      if (this.textgrid) {
        Creator creator = new Creator();
        creator.setCreatorName(OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, creatorField));
        NameIdentifier nameIdentifier = new NameIdentifier();
        // TODO: creatorID Field from config file
        String creatorID = OaipmhUtilities.firstEntryFieldLoader(this.jsonObj,
            "edition.source.bibliographicCitation.author.id");
        if (creatorID != null && creatorID.contains(":")) {
          nameIdentifier.setValue(creatorID.split(":")[1]);
          nameIdentifier.setNameIdentifierScheme(creatorID.split(":")[0].toUpperCase());
          // Check if the scheme URI can be set for GND (and PND).
          if (nameIdentifier.getNameIdentifierScheme().equalsIgnoreCase(PND)
              || nameIdentifier.getNameIdentifierScheme().equalsIgnoreCase(GND)) {
            nameIdentifier.setNameIdentifierScheme(GND);
            nameIdentifier.setSchemeURI(GND_SCHEME);
          }
          // Check if scheme can be set for VIAF.
          else if (nameIdentifier.getNameIdentifierScheme().equalsIgnoreCase(VIAF)) {
            nameIdentifier.setNameIdentifierScheme(VIAF);
            nameIdentifier.setSchemeURI(VIAF_SCHEME);
          }
          // Check if scheme can be set for ORCID.
          else if (nameIdentifier.getNameIdentifierScheme().equalsIgnoreCase(ORCID)) {
            nameIdentifier.setNameIdentifierScheme(ORCID);
            nameIdentifier.setSchemeURI(ORCID_SCHEME);
          }
          // TODO Implement more scheme URIs!
        } else {
          nameIdentifier.setValue(creatorID);
        }
        // Check for all entries or leave it be.
        if (nameIdentifier.getValue() != null && !nameIdentifier.getValue().isEmpty()
            && nameIdentifier.getNameIdentifierScheme() != null
            && !nameIdentifier.getNameIdentifierScheme().isEmpty()
            && nameIdentifier.getSchemeURI() != null
            && !nameIdentifier.getSchemeURI().isEmpty()) {
          creator.setNameIdentifier(nameIdentifier);
        }

        creators.getCreator().add(creator);
      }

      // **
      // DARIAH
      // **

      else if (this.dariah) {
        List<String> creatorsList = OaipmhUtilities.listFieldLoader(this.jsonObj, creatorField);
        for (String c : creatorsList) {
          Creator creator = new Creator();
          creator.setCreatorName(c);
          creators.getCreator().add(creator);
        }
      }
    }

    return creators;
  }

  /**
   * @return
   */
  private Titles addTitles() {

    Titles result = new Titles();

    // Loop title fields.
    for (String oarTitle : this.oarTitleFields) {

      List<String> listOfTitles = OaipmhUtilities.listFieldLoader(this.jsonObj, oarTitle);
      for (String title : listOfTitles) {
        Title tileInOpenAireRecord = new Title();
        tileInOpenAireRecord.setValue(title);
        // Title type OTHER is not permitted, leave type out for the moment!
        // tileInOpenAireRecord.setTitleType(TitleType.OTHER);
        result.getTitle().add(tileInOpenAireRecord);
      }
    }

    return result;
  }

  /**
   * @return
   */
  private String addPublisher() {

    String result = "";

    /**
     * Publisher fields
     */
    // TODO: fields from config file
    // TODO: for each field
    // TODO: Adapt to DARIAH!
    // return OAIPMHUtilities.fieldLoader(jsonObj,
    // "edition.source.bibliographicCitation.publisher.value");

    if (this.textgrid) {
      result = "TextGrid";
    } else if (this.dariah) {
      result = "DARIAH-DE";
    }

    return result;
  }

  /**
   * @return
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  private String addPublicationYear() throws ParseException, DatatypeConfigurationException {
    return Integer.toString(OaipmhUtilities
        .getUTCDateAsGregorian(
            OaipmhUtilities.firstEntryFieldLoader(this.jsonObj, this.dateOfObjectCreation))
        .getYear());
  }

  /**
   * <p>
   * Removes the "doi:" from the given DOI, if existing.
   * </p>
   *
   * @param theDOI
   * @return
   */
  private static String omitDoiPrefix(final String theDOI) {
    return omitPrefix(theDOI, RDFConstants.DOI_PREFIX);
  }


  /**
   * <p>
   * Removes the "hdl:" from the given PID, if existing.
   * </p>
   *
   * @param theHDL
   * @return
   */
  private static String omitHdlPrefix(final String theHDL) {
    return omitPrefix(theHDL, RDFConstants.HDL_PREFIX);
  }

  /**
   * <p>
   * Removes a prefix, including the ":" from the beginning of a string.
   * </p>
   *
   * @param theURI
   * @param thePrefix
   * @return
   */
  private static String omitPrefix(final String theURI, final String thePrefix) {

    String result = theURI;

    if (theURI != null && theURI.startsWith(thePrefix + ":")) {
      result = theURI.replaceFirst(thePrefix + ":", "");
    }

    return result;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public String[] getOarTitleFields() {
    return this.oarTitleFields;
  }

  /**
   * @param oarTitleFields
   */
  public void setOarTitleFields(String[] oarTitleFields) {
    this.oarTitleFields = oarTitleFields;
  }

  /**
   * @return
   */
  public String getOarIdentifierField() {
    return this.oarIdentifierField;
  }

  /**
   * @param oarIdentifierField
   */
  public void setOarIdentifierField(String oarIdentifierField) {
    this.oarIdentifierField = oarIdentifierField;
  }

  /**
   * @return
   */
  public String[] getOarDateFields() {
    return this.oarDateFields;
  }

  /**
   * @param oarDateFields
   */
  public void setOarDateFields(String[] oarDateFields) {
    this.oarDateFields = oarDateFields;
  }

  /**
   * @return
   */
  public String[] getOarCreatorFields() {
    return this.oarCreatorFields;
  }

  /**
   * @param oarCreatorFields
   */
  public void setOarCreatorFields(String[] oarCreatorFields) {
    this.oarCreatorFields = oarCreatorFields;
  }

  /**
   * @return
   */
  public String[] getOarContributorFields() {
    return this.oarContributorFields;
  }

  /**
   * @param oarContributorFields
   */
  public void setOarContributorFields(String[] oarContributorFields) {
    this.oarContributorFields = oarContributorFields;
  }

  /**
   * @return
   */
  public String[] getOarLanguageFields() {
    return this.oarLanguageFields;
  }

  /**
   * @param oarLanguageFields
   */
  public void setOarLanguageFields(String[] oarLanguageFields) {
    this.oarLanguageFields = oarLanguageFields;
  }

  /**
   * @return
   */
  public String[] getOarAlternateIdentifierFields() {
    return this.oarAlternateIdentifierFields;
  }

  /**
   * @param oarAlternateIdentifierFields
   */
  public void setOarAlternateIdentifierFields(String[] oarAlternateIdentifierFields) {
    this.oarAlternateIdentifierFields = oarAlternateIdentifierFields;
  }

  /**
   * @return
   */
  public String[] getOarFormatFields() {
    return this.oarFormatFields;
  }

  /**
   * @param oarFormatFields
   */
  public void setOarFormatFields(String[] oarFormatFields) {
    this.oarFormatFields = oarFormatFields;
  }

  /**
   * @return
   */
  public String[] getOarRightsFields() {
    return this.oarRightsFields;
  }

  /**
   * @param oarRightsFields
   */
  public void setOarRightsFields(String[] oarRightsFields) {
    this.oarRightsFields = oarRightsFields;
  }

  /**
   * @return
   */
  public String[] getOarDescriptionFields() {
    return this.oarDescriptionFields;
  }

  /**
   * @param oarDescriptionFields
   */
  public void setOarDescriptionFields(String[] oarDescriptionFields) {
    this.oarDescriptionFields = oarDescriptionFields;
  }

  /**
   * @return
   */
  public String[] getOarRelatedIdentifierFields() {
    return this.oarRelatedIdentifierFields;
  }

  /**
   * @param oarRelatedIdentifierFields
   */
  public void setOarRelatedIdentifierFields(String[] oarRelatedIdentifierFields) {
    this.oarRelatedIdentifierFields = oarRelatedIdentifierFields;
  }

  /**
   * @return
   */
  public String getRelationToWorkObject() {
    return this.relationToWorkObject;
  }

  /**
   * @param relationToWorkObject
   */
  public void setRelationToWorkObject(String relationToWorkObject) {
    this.relationToWorkObject = relationToWorkObject;
  }

  /**
   * @return
   */
  public String[] getOarGeoLocationFields() {
    return this.oarGeoLocationFields;
  }

  /**
   * @param oarGeoLocationFields
   */
  public void setOarGeoLocationFields(String[] oarGeoLocationFields) {
    this.oarGeoLocationFields = oarGeoLocationFields;
  }

  /**
   * @return
   */
  public String getHandle() {
    return this.handle;
  }

  /**
   * @param handle
   */
  public void setHandle(String handle) {
    this.handle = handle;
  }

  /**
   * @return
   */
  public String[] getOarVersionFields() {
    return this.oarVersionFields;
  }

  /**
   * @param oarVersionFields
   */
  public void setOarVersionFields(String[] oarVersionFields) {
    this.oarVersionFields = oarVersionFields;
  }

  /**
   * @return
   */
  public String[] getOarSubjectFields() {
    return this.oarSubjectFields;
  }

  /**
   * @param oarSubjectFields
   */
  public void setOarSubjectFields(String[] oarSubjectFields) {
    this.oarSubjectFields = oarSubjectFields;
  }

  /**
   * @return
   */
  public String getOarSizeField() {
    return this.oarSizeField;
  }

  /**
   * @param oarSize
   */
  public void setOarSizeField(String oarSize) {
    this.oarSizeField = oarSize;
  }

}

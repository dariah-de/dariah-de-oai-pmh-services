/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Logger;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;

/**
 *
 */
public class OaipmhClient {

  private static Logger log = Logger.getLogger(OaipmhClient.class.getName());

  private String identifier;
  private String metadataPrefix;
  private String set;
  private String from;
  private String until;
  private String resumptionToken;
  private OaipmhProducer producer;

  /**
   * @param endpoint
   */
  public OaipmhClient(String endpoint) {

    log.fine("  ##  START CREATING PRODUCER");

    this.producer = JAXRSClientFactory.create(endpoint + "/oai", OaipmhProducer.class);

    log.fine("  ##  END CREATING PRODUCER");
  }

  /**
   * @param verb
   * @return
   * @throws ParseException
   * @throws IOException
   */
  public String request(String verb) throws ParseException, IOException {

    log.fine("  ##  START request");

    String result =
        this.producer.getRequest(verb, this.identifier, this.metadataPrefix, this.set, this.from,
            this.until, this.resumptionToken);

    log.fine("  ##  END request");

    return result;
  }

}

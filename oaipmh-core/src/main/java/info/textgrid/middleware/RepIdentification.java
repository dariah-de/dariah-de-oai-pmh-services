package info.textgrid.middleware;

import info.textgrid.middleware.oaipmh.DeletedRecordType;
import info.textgrid.middleware.oaipmh.GranularityType;
import info.textgrid.middleware.oaipmh.RequestType;

/**
 * <p>
 * Handling the "Identify" request from an OAIPMH client.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class RepIdentification {

  public static final DeletedRecordType DELETE_RECORD = DeletedRecordType.NO;
  public static final GranularityType GRANULARITY = GranularityType.YYYY_MM_DD_THH_MM_SS_Z;
  public static final String PROTOCOL_VERSION = "2.0";

  private String repositoryName;
  private String baseURL;
  private String adminMail;
  private String earliestDatestamp;

  /**
   * @param repositoryName
   * @param baseURL
   * @param adminMail
   * @param earliestDatestamp
   */
  public RepIdentification(String repositoryName, String baseURL, String adminMail,
      String earliestDatestamp) {
    setRepositoryName(repositoryName);
    setBaseUrl(baseURL);
    setAdminMail(adminMail);
    setEarliestDatestamp(earliestDatestamp);
  }

  /**
   * <p>
   * Checking the request if all necessary values are set or not allowed values are not set.
   * </p>
   * 
   * @param request the initially request from the OAIPMG client
   * @return a boolean indicating the correctness of the request
   */
  public boolean requestChecker(RequestType request) {

    boolean noArguments;

    if (request.getFrom() != null || request.getIdentifier() != null
        || request.getMetadataPrefix() != null) {
      noArguments = false;
    } else if (request.getResumptionToken() != null || request.getSet() != null
        || request.getUntil() != null) {
      noArguments = false;
    } else {
      noArguments = true;
    }

    return noArguments;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @param baseURL
   */
  public void setBaseURL(String baseURL) {
    this.baseURL = baseURL;
  }

  /**
   * @return
   */
  public String getRepositoryName() {
    return this.repositoryName;
  }

  /**
   * @param repositoryName
   */
  public void setRepositoryName(String repositoryName) {
    this.repositoryName = repositoryName;
  }

  /**
   * @return
   */
  public String getBaseURL() {
    return this.baseURL;
  }

  /**
   * @param baseURL
   */
  public void setBaseUrl(String baseURL) {
    this.baseURL = baseURL;
  }

  /**
   * @return
   */
  public String getEarliestDatestamp() {
    return this.earliestDatestamp;
  }

  /**
   * @param earliestDatestamp
   */
  public void setEarliestDatestamp(String earliestDatestamp) {
    this.earliestDatestamp = earliestDatestamp;
  }

  /**
   * @return
   */
  public String getAdminMail() {
    return this.adminMail;
  }

  /**
   * @param adminMail
   */
  public void setAdminMail(String adminMail) {
    this.adminMail = adminMail;
  }

}

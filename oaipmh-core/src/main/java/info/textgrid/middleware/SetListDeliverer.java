package info.textgrid.middleware;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.logging.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import info.textgrid.clients.tgauth.AuthClientException;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.oaipmh.ListSetsType;
import info.textgrid.middleware.oaipmh.RequestType;
import info.textgrid.middleware.oaipmh.SetType;

/**
 *
 */
public class SetListDeliverer {

  private static Logger log = Logger.getLogger(SetListDeliverer.class.getName());

  private static final int TG_SIZE = 1000;
  private static final String TG_SANDBOX = "nearlyPublished";
  private static final String TG_FORMAT = "format";
  private static final String TG_PROJECTS = "projects";

  private static final int DH_SIZE = 100000;
  private static final String DH_COLLECTION_FILTER_NAME = "collectionFilter";
  private static final String DH_COLLECTION_FILTER_TERM_NAME = "collectionFilterTerm";
  private static final String DH_FIELD_DIVIDER = "&";

  private String formatField;
  private String relationField;
  private String formatToFilter;
  private String identifierField;
  private String repositoryObjectURIPrefix;
  private String specFieldPrefix;
  private boolean textgrid;
  private boolean dariah;

  private OaipmhElasticSearchClient oaiEsClient;

  /**
   * @param oaiEsClient
   * @param textgrid
   * @param dariah
   */
  public SetListDeliverer(boolean textgrid, boolean dariah) {
    this.textgrid = textgrid;
    this.dariah = dariah;
  }

  /**
   * @return
   * @throws AuthClientException
   * @throws IOException
   */
  public ListSetsType setListBuilder() {

    ListSetsType setList = new ListSetsType();

    SearchRequest request = new SearchRequest(this.oaiEsClient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.size(0);

    // Assemble TG ES request.
    if (this.textgrid) {
      Script mergeProjectIDandProjectName =
          new Script("doc['project.id'].value + '&' + doc['project.value.untouched'].value");

      AggregationBuilder filterPublicProjects = AggregationBuilders.filter("projectsPublic",
          QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(TG_SANDBOX))
              .must(QueryBuilders.matchPhraseQuery(TG_FORMAT, TextGridMimetypes.EDITION)));
      AggregationBuilder projectNameAndID =
          AggregationBuilders.terms(TG_PROJECTS).script(mergeProjectIDandProjectName).size(TG_SIZE);

      filterPublicProjects.subAggregation(projectNameAndID);
      searchSourceBuilder.aggregation(filterPublicProjects);
    }

    // Assemble DH ES request.
    else if (this.dariah) {

      log.fine("formatField:               '" + this.formatField + "'");
      log.fine("relationField:             '" + this.relationField + "'");
      log.fine("formatToFilter:            '" + this.formatToFilter + "'");
      log.fine("identifierField:           '" + this.identifierField + "'");
      log.fine("repositoryObjectURIPrefix: '" + this.repositoryObjectURIPrefix + "'");
      log.fine("specFieldPrefix:           '" + this.specFieldPrefix + "'");

      Script mergeCollectionIDAndTitle = new Script("doc['" + this.identifierField + "'].value + '"
          + DH_FIELD_DIVIDER + "' + doc['descriptiveMetadata.dc:title'].value");

      log.fine("script id/title: " + mergeCollectionIDAndTitle.getIdOrCode());

      // Do filter for all root collections (find formatToFilter in format field AND NOT the HDL
      // prefix in relation field!
      AggregationBuilder collectionFilter = AggregationBuilders.filter(DH_COLLECTION_FILTER_NAME,
          QueryBuilders.boolQuery()
              .must(QueryBuilders.matchPhraseQuery(this.formatField, this.formatToFilter))
              // Do NOT use specFieldPrefix, we must NOT have the ":" in it!
              .mustNot(QueryBuilders.prefixQuery(this.relationField, "hdl")));
      AggregationBuilder collectionFilterTerm = AggregationBuilders
          .terms(DH_COLLECTION_FILTER_TERM_NAME).script(mergeCollectionIDAndTitle).size(DH_SIZE);

      collectionFilter.subAggregation(collectionFilterTerm);
      searchSourceBuilder.aggregation(collectionFilter);
    }

    request.source(searchSourceBuilder);

    // Get items with TG or DH request.
    SearchResponse getRecordListItems = null;
    try {
      getRecordListItems = this.oaiEsClient.getEsClient().search(request, RequestOptions.DEFAULT);

    } catch (IOException e) {
      // TODO Auto-generated catch block
      log.severe(e.getMessage());
      e.printStackTrace();
    }

    // Fill ListSets request...
    if (getRecordListItems != null && getRecordListItems.getAggregations() != null) {

      // ...for TG
      if (this.textgrid) {
        for (Entry<String, Aggregation> entry : getRecordListItems.getAggregations().asMap()
            .entrySet()) {

          String name = entry.getKey();
          if (name.equals("projectsPublic")) {
            Filter trytry = getRecordListItems.getAggregations().get("projectsPublic");
            Terms trytry2 = trytry.getAggregations().get("projects");
            for (Bucket bentry : trytry2.getBuckets()) {
              String projectName = bentry.getKey().toString();
              String[] projectInfos = projectName.split("&");
              SetType setsForTextGrid = new SetType();
              setsForTextGrid.setSetSpec(this.specFieldPrefix + projectInfos[0]);
              setsForTextGrid.setSetName(projectInfos[1]);
              setList.getSet().add(setsForTextGrid);
            }
          }
        }
      }

      // ...for DH
      else if (this.dariah) {

        for (Entry<String, Aggregation> entry : getRecordListItems.getAggregations().asMap()
            .entrySet()) {

          String name = entry.getKey();

          if (name.equals(DH_COLLECTION_FILTER_NAME)) {
            Filter filterCollection =
                getRecordListItems.getAggregations().get(DH_COLLECTION_FILTER_NAME);
            Terms collectionTerms =
                filterCollection.getAggregations().get(DH_COLLECTION_FILTER_TERM_NAME);
            for (Bucket b : collectionTerms.getBuckets()) {
              // Divide ID and title.
              String combined = b.getKeyAsString();

              log.finer("combined: " + combined);

              int indexOf = combined.indexOf(DH_FIELD_DIVIDER);
              // ID already contains the setSpecPrefix!
              String id = combined.substring(0, indexOf);

              String title = combined.substring(indexOf + 1);

              // Set set.
              SetType newSet = new SetType();
              newSet.setSetSpec(id);
              newSet.setSetName(title);
              setList.getSet().add(newSet);
            }
          }
        }
      }
    }

    // Add OpenAIRE data set for OpenAIRE.
    // TODO Document for what this is used or necessary!
    SetType setOpenAire = new SetType();
    setOpenAire.setSetName("OpenAIRE");
    setOpenAire.setSetSpec("openaire_data");
    setList.getSet().add(setOpenAire);

    return setList;
  }

  /**
   * <p>
   * Checking the request if all necessary values are set or not allowed values are not set.
   * </p>
   * 
   * @param request the initially request from the OAIPMG client
   * @return a boolean indicating the correctness of the request
   */
  public boolean requestChecker(RequestType request) {

    boolean noArguments;

    if (request.getFrom() != null ||
        request.getIdentifier() != null ||
        request.getMetadataPrefix() != null) {
      noArguments = false;
    } else if (request.getResumptionToken() != null ||
        request.getSet() != null ||
        request.getUntil() != null) {
      noArguments = false;
    } else {
      noArguments = true;
    }

    return noArguments;
  }

  // **
  // GETTERS and SETTERS
  // **

  /**
   * @return
   */
  public String getFormatField() {
    return this.formatField;
  }

  /**
   * @param formatField
   */
  public void setFormatField(String formatField) {
    this.formatField = formatField;
  }

  /**
   * @return
   */
  public String getRelationField() {
    return this.relationField;
  }

  /**
   * @param relationField
   */
  public void setRelationField(String relationField) {
    this.relationField = relationField;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @return
   */
  public String getIdentifierField() {
    return this.identifierField;
  }

  /**
   * @param identifierField
   */
  public void setIdentifierField(String identifierField) {
    this.identifierField = identifierField;
  }

  /**
   * @return
   */
  public String getRepositoryObjectURIPrefix() {
    return this.repositoryObjectURIPrefix;
  }

  /**
   * @param repositoryObjectURIPrefix
   */
  public void setRepositoryObjectURIPrefix(String repositoryObjectURIPrefix) {
    this.repositoryObjectURIPrefix = repositoryObjectURIPrefix;
  }

  /**
   * @return
   */
  public String getSpecFieldPrefix() {
    return this.specFieldPrefix;
  }

  /**
   * @param specFieldPrefix
   */
  public void setSpecFieldPrefix(String specFieldPrefix) {
    this.specFieldPrefix = specFieldPrefix;
  }

  /**
   * @return
   */
  public boolean isTextgrid() {
    return this.textgrid;
  }

  /**
   * @param textgrid
   */
  public void setTextgrid(boolean textgrid) {
    this.textgrid = textgrid;
  }

  /**
   * @return
   */
  public boolean isDariah() {
    return this.dariah;
  }

  /**
   * @param dariah
   */
  public void setDariah(boolean dariah) {
    this.dariah = dariah;
  }

  /**
   * @return
   */
  public OaipmhElasticSearchClient getOaiEsClient() {
    return this.oaiEsClient;
  }

  /**
   * @param oaiEsClient
   */
  public void setOaiEsClient(OaipmhElasticSearchClient oaiEsClient) {
    this.oaiEsClient = oaiEsClient;
  }

}

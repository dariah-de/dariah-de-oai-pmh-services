package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import info.textgrid.middleware.oaipmh.ListRecordsType;

/**
 *
 */
public interface RecordListDelivererInterface {

  /**
   * @param from
   * @param to
   * @param set
   * @param resumptionToken
   * @return
   * @throws ParseException
   * @throws IOException
   */
  public ListRecordsType getRecords(String from, String to, String set, String resumptionToken)
      throws ParseException, IOException;

}

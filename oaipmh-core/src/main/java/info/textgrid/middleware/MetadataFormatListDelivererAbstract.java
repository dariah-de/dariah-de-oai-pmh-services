package info.textgrid.middleware;

import info.textgrid.middleware.oaipmh.ListMetadataFormatsType;
import info.textgrid.middleware.oaipmh.MetadataFormatType;
import info.textgrid.middleware.oaipmh.RequestType;

/**
 *
 */
public abstract class MetadataFormatListDelivererAbstract
    implements MetadataFormatListDelivererInterface {

  //
  // CLASS
  //

  protected OaipmhElasticSearchClient oaiEsClient;

  /**
   *
   */
  @Override
  public ListMetadataFormatsType setMetadataFormatList() {

    ListMetadataFormatsType result = new ListMetadataFormatsType();

    // Add default metadata format (oai_dc).
    MetadataFormatType mft = new MetadataFormatType();
    mft.setMetadataPrefix(OaipmhConstants.METADATA_OAIDC_PREFIX);
    mft.setMetadataNamespace(OaipmhConstants.OAIDC_NAMESPACE);
    mft.setSchema(OaipmhConstants.OAIDC_SCHEMA_LOCATION);
    result.getMetadataFormat().add(mft);

    // Add metadata format for (oai_datacite).
    MetadataFormatType mftOpenAire = new MetadataFormatType();
    mftOpenAire.setMetadataNamespace(OaipmhConstants.DATACITE_NAMESPACE);
    mftOpenAire.setMetadataPrefix(OaipmhConstants.METADATA_OPENAIRE_PREFIX);
    mftOpenAire.setSchema(OaipmhConstants.DATACITE_SCHEMA_LOCATION);
    result.getMetadataFormat().add(mftOpenAire);

    return result;
  }

  /**
   *
   */
  @Override
  public boolean requestChecker(RequestType request) {

    boolean requestCheck = true;

    if (request.getFrom() != null
        || request.getMetadataPrefix() != null
        || request.getResumptionToken() != null) {
      requestCheck = false;
    } else if (request.getSet() != null
        || request.getUntil() != null) {
      requestCheck = false;
    }

    return requestCheck;
  }

  //
  // GETTERS & SETTERS
  //

  /**
   * @return
   */
  public OaipmhElasticSearchClient getOaiEsClient() {
    return this.oaiEsClient;
  }

  /**
   * @param oaiEsClient
   */
  public void setOaiEsClient(OaipmhElasticSearchClient oaiEsClient) {
    this.oaiEsClient = oaiEsClient;
  }

}

package info.textgrid.middleware;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * <p>
 * Establishing to connection to an ElasticSearch client.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class OaipmhElasticSearchClient {

  private String url;
  private int[] ports;
  private String esIndex;
  private RestHighLevelClient esClient;

  /**
   * <P>
   * Constructor for the connection to an ElasticSearch index.
   * </p>
   * 
   * @param url Address for the ELasticSearch connection
   * @param ports Ports for the ElasticSearch connection
   * @param itemLimit Limit of items returned
   */
  public OaipmhElasticSearchClient(String url, int[] ports) {

    this.setUrl(url);
    this.setPorts(ports);

    List<HttpHost> hosts = new ArrayList<HttpHost>();
    for (int port : ports) {
      hosts.add(new HttpHost(url, port, "http"));
    }

    setEsClient(
        new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[hosts.size()]))));
  }

  /**
   * @return esIndex
   */
  public String getEsIndex() {
    return this.esIndex;
  }

  /**
   * @param esIndex
   */
  public void setEsIndex(String esIndex) {
    this.esIndex = esIndex;
  }

  /**
   * @return url
   */
  public String getUrl() {
    return this.url;
  }

  /**
   * @param url
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * @return port
   */
  public int[] getPorts() {
    return this.ports;
  }

  /**
   * @param port
   */
  public void setPorts(int ports[]) {
    this.ports = ports;
  }

  /**
   * @return
   */
  public RestHighLevelClient getEsClient() {
    return this.esClient;
  }

  /**
   * @param esClient
   */
  public void setEsClient(RestHighLevelClient esClient) {
    this.esClient = esClient;
  }

}

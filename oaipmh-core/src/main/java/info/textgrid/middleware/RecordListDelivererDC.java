package info.textgrid.middleware;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.json.JSONObject;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.ListRecordsType;
import info.textgrid.middleware.oaipmh.MetadataType;
import info.textgrid.middleware.oaipmh.RecordType;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;

/**
 *
 */
public class RecordListDelivererDC extends RecordListDelivererAbstract {

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(RecordListDelivererDC.class.getName());

  protected static Map<String, Integer> cursorCollector = new Hashtable<String, Integer>();

  // **
  // DC-Field Lists
  // **

  private String[] contributorList;
  private String[] coverageList;
  private String[] creatorList;
  private String[] dateList;
  private String[] descriptionList;
  private String[] formatList;
  private String[] identifierList;
  private String[] languageList;
  private String[] publisherList;
  private String[] relationList;
  private String[] relationsForWorkList;
  private String[] rightsList;
  private String[] sourceList;
  private String[] subjectList;
  private String[] titleList;
  private String[] typeList;

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordListDelivererDC(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   * <p>
   * The furtherDCElementes fetches the specified fields from the work object. This function is
   * called from the function fetchFields.
   * </p>
   * 
   * @param id
   * @param client
   * @return
   */
  public GetResponse furtherDCElements(final String id, final RestHighLevelClient client) {

    String identifier = id;

    if (identifier != null) {
      try {
        identifier = URLDecoder.decode(id, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      String[] includes = this.workFields;
      String[] excludes = Strings.EMPTY_ARRAY;
      FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);
      GetRequest getRequest = new GetRequest(this.oaiEsClient.getEsIndex(), identifier)
          .fetchSourceContext(fetchSourceContext);

      GetResponse responseWorkValues = null;
      try {
        responseWorkValues = this.oaiEsClient.getEsClient().get(getRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      return responseWorkValues;
    }

    return null;
  }

  /**
   * <p>
   * The fetchFields function filters all edition and fetch the specified fields.
   * </p>
   * 
   * @param query
   * @param recordList
   * @param resumptionToken
   * @param set
   * @throws UnsupportedEncodingException
   */
  public void fetchFields(QueryBuilder query, ListRecordsType recordList, String resumptionToken,
      String set) throws UnsupportedEncodingException {

    String scrollID = "";
    SearchResponse scrollResp;
    QueryBuilder recordFilter;
    DublinCoreBuilder dublinCoreBuilder = new DublinCoreBuilder();

    // **
    // DARIAH
    // **

    if (this.dariah == true) {
      // All objects!
      recordFilter = query;
    }

    // **
    // TEXTGRID
    // **

    else {
      // No sandbox items.
      QueryBuilder tgFilterSandBox = QueryBuilders.matchPhraseQuery("nearlyPublished", "true");
      // All editions only!
      QueryBuilder tgEditionsOnly =
          QueryBuilders.matchPhraseQuery(this.formatField, this.formatToFilter);

      // Compose query.
      recordFilter =
          QueryBuilders.boolQuery().must(query).must(tgEditionsOnly).mustNot(tgFilterSandBox);
    }

    SearchRequest searchRequest = new SearchRequest(this.oaiEsClient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

    scrollResp = null;

    searchSourceBuilder.query(recordFilter);
    searchSourceBuilder.size(100);
    searchRequest.source(searchSourceBuilder);

    if (resumptionToken != null) {
      SearchScrollRequest scrollRequest = new SearchScrollRequest(resumptionToken);
      scrollRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));

      try {
        scrollResp = this.oaiEsClient.getEsClient().scroll(scrollRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else {
      searchRequest.source(searchSourceBuilder);
      searchRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
      try {
        scrollResp = this.oaiEsClient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    scrollID = scrollResp.getScrollId();

    long completeListSize = scrollResp.getHits().getTotalHits().value;
    setResultSize(completeListSize);

    if (completeListSize > 0) {
      setFoundItems(true);

      for (SearchHit hit : scrollResp.getHits().getHits()) {
        if (hit != null && hit.getFields() != null) {
          JSONObject json = new JSONObject(hit.getSourceAsMap());
          this.modifiedValue = OaipmhUtilities.firstEntryFieldLoader(json, this.modifiedField);

          // **
          // TEXTGRID SEARCH
          // **

          if (this.textgrid) {
            String workUri = "";
            String[] relationFields = new String[] {this.relationToFurtherMetadataObject};

            if (hit.getSourceAsMap().get(this.formatField).toString().equals(this.formatToFilter)) {
              String setSpec = this.specFieldPrefix
                  + DublinCoreFieldLoader.fillList(hit, this.relationList).get(0);
              if (DublinCoreFieldLoader.fillList(hit, relationFields).get(0) != null) {
                workUri = DublinCoreFieldLoader.fillList(hit, relationFields).get(0)
                    .substring(this.repositoryObjectURIPrefix.length());

                if (!workUri.endsWith(".0")) {
                  workUri = workUri.concat(".0");
                }
              }
              try {
                dublinCoreBuilder = putContentIntoDCFieldListsTG(hit,
                    furtherDCElements(workUri, this.oaiEsClient.getEsClient()));

                String identifier = hit.getSourceAsMap().get(this.identifierField).toString();

                HeaderType header = OaipmhUtilities.computeResponseHeader(
                    OaipmhUtilities.getUTCDateAsString(this.modifiedValue).toString(), identifier,
                    setSpec);
                buildRecord(recordList, header, dublinCoreBuilder);

              } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              } catch (DatatypeConfigurationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
              }
            }
          }

          // **
          // DARIAH SEARCH
          // **

          else if (this.dariah) {

            // Get identifier and setSpec field, add setSpec prefix if not already set.
            String identifier = OaipmhUtilities.firstEntryFieldLoader(json, this.identifierField);
            String setSpec = OaipmhUtilities.getSetSpec(set, this.specFieldPrefix, identifier);

            try {
              dublinCoreBuilder = putContentIntoDCFieldListsDH(hit);

              HeaderType header = OaipmhUtilities.computeResponseHeader(
                  OaipmhUtilities.getUTCDateAsString(this.modifiedValue).toString(), identifier,
                  setSpec);
              buildRecord(recordList, header, dublinCoreBuilder);

            } catch (ParseException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            } catch (DatatypeConfigurationException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
        }
      }

      ResumptionTokenType resTokenForResponse = OaipmhUtilities.getResumptionToken(completeListSize,
          resumptionToken, cursorCollector, scrollID, this.searchResponseSize);
      if (resTokenForResponse != null) {
        log.fine("resTokenForResponse: " + resTokenForResponse.getValue());
      }

      recordList.setResumptionToken(resTokenForResponse);

    } else

    {
      setFoundItems(false);
    }
  }

  /**
   *
   */
  @Override
  public ListRecordsType getRecords(String from, String to, String set, String resumptionToken)
      throws ParseException, IOException {

    ListRecordsType recordList = new ListRecordsType();
    QueryBuilder query;
    QueryBuilder rangeQuery = QueryBuilders.rangeQuery(this.rangeField).from(from).to(to);
    QueryBuilder setQuery = null;

    if (set != null && !set.isEmpty()) {
      String queryField = "";
      String valueField = "";

      // **
      // DARIAH
      // **

      if (this.dariah) {
        // TODO Put query field into config!
        queryField = "administrativeMetadata.dcterms:relation";
        valueField = set;

        // Add record with set metadata in it (to also find root collection metadata in Repository
        // Search!)
        // TODO Put query field into config!
        setQuery = QueryBuilders
            .matchQuery("administrativeMetadata.dcterms:identifier", this.specFieldPrefix + set)
            .boost(2);

        log.fine("set query: " + setQuery.getName() + " -> " + setQuery.toString());
      }

      // **
      // TEXTGRID
      // **

      else if (this.textgrid) {
        // Set is not null and not empty, so check set content: if prefix existing and project, do
        // split and use set content.
        if (set.contains(":") && set.startsWith("project:")) {
          String[] setParts = set.split(":");
          if (setParts[0].equals("project")) {
            // TODO Put query field into config!
            queryField = "project.id";
            valueField = setParts[1];
          }
        } else {
          throw new IOException(OaipmhConstants.ERROR_SET_UNKNOWN + ": " + set);
        }
      }

      QueryBuilder matchQuery = QueryBuilders.matchPhraseQuery(queryField, valueField);
      QueryBuilder boolQuery = QueryBuilders.boolQuery().must(rangeQuery).must(matchQuery);
      // "should" means OR here, "must" means AND! So we get: setQuery OR ( rangeQuery AND
      // matchQuery )! The order of should's do matter here!
      if (setQuery != null) {
        boolQuery = QueryBuilders.boolQuery().should(setQuery).should(boolQuery);

        log.fine("bool query: " + boolQuery.getName() + " -> " + boolQuery.toString());
      }

      query = boolQuery;
    }

    // Set == null!
    else {
      query = rangeQuery;
    }

    try {
      fetchFields(query, recordList, resumptionToken, set);
    } catch (UnsupportedEncodingException e) {
      throw new IOException(OaipmhConstants.ERROR_FETCH_FIELDS, e);
    }

    return recordList;
  }

  // **
  // PRIVATE METHIODS
  // **

  /**
   * @param recordList
   * @param header
   * @param dublinCoreBuilder
   */
  private static void buildRecord(ListRecordsType recordList, HeaderType header,
      DublinCoreBuilder dublinCoreBuilder) {

    // Get metadata.
    MetadataType metadata = new MetadataType();
    metadata = dublinCoreBuilder.getDC();

    // Build record, set header and metadata.
    RecordType record = new RecordType();
    record.setHeader(header);
    record.setMetadata(metadata);

    // Add record to the given list.
    recordList.getRecord().add(record);
  }

  /**
   * @param hit
   * @param responseWorkValues
   * @return
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  private DublinCoreBuilder putContentIntoDCFieldListsTG(SearchHit hit,
      GetResponse responseWorkValues) throws ParseException, DatatypeConfigurationException {

    DublinCoreBuilder result = new DublinCoreBuilder();

    result.setContributor(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.contributorList));
    result.setCoverage(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.coverageList));
    result.setCreator(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.creatorList));
    result
        .setDate(DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.dateList));
    result.setDescription(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.descriptionList));
    result.setFormat(DublinCoreFieldLoader.fillList(hit, this.formatList));
    result.setIdentifier(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.identifierList));
    result.setLanguage(DublinCoreFieldLoader.fillList(hit, this.languageList));
    result.setPublisher(DublinCoreFieldLoader.fillList(hit, this.publisherList));
    result.setRelation(DublinCoreFieldLoader.fillList(hit, this.relationList));
    result.setRelation(DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues,
        this.relationsForWorkList));
    result.setRights(DublinCoreFieldLoader.fillList(hit, this.rightsList));
    result.setSource(DublinCoreFieldLoader.fillList(hit, this.sourceList));
    result.setSubject(DublinCoreFieldLoader.fillList(hit, this.subjectList));
    result.setTitle(DublinCoreFieldLoader.fillList(hit, this.titleList));
    result
        .setType(DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.typeList));

    return result;
  }

  /**
   * @param hit
   * @return
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  private DublinCoreBuilder putContentIntoDCFieldListsDH(SearchHit hit)
      throws ParseException, DatatypeConfigurationException {

    DublinCoreBuilder result = new DublinCoreBuilder();

    result.setContributor(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.contributorList));
    result.setCoverage(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.coverageList));
    result.setCreator(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.creatorList));
    result.setDate(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.dateList));
    result.setDescription(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.descriptionList));
    result.setFormat(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.formatList));
    result.setIdentifier(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.identifierList));
    result.setLanguage(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.languageList));
    result.setPublisher(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.publisherList));
    result.setRelation(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.relationList));
    result.setRights(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.rightsList));
    result.setSource(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.sourceList));
    result.setSubject(OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()),
        this.subjectList));
    result.setTitle(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.titleList));
    result.setType(
        OaipmhUtilities.loopListFieldLoader(new JSONObject(hit.getSourceAsMap()), this.typeList));

    return result;
  }

  // **
  // GETTERS AND SETTERS for DublinCore Lists
  // **

  /**
   * @param contributorList
   */
  public void setContributors(String[] contributorList) {
    this.contributorList = contributorList;
  }

  /**
   * @param coveragesList
   */
  public void setCoverages(String[] coveragesList) {
    this.coverageList = coveragesList;
  }

  /**
   * @param creatorsList
   */
  public void setCreators(String[] creatorsList) {
    this.creatorList = creatorsList;
  }

  /**
   * @param datesList
   */
  public void setDates(String[] datesList) {
    this.dateList = datesList;
  }

  /**
   * @param descriptionsList
   */
  public void setDescriptions(String[] descriptionsList) {
    this.descriptionList = descriptionsList;
  }

  /**
   * @param formatsList
   */
  public void setFormats(String[] formatsList) {
    this.formatList = formatsList;
  }

  /**
   * @param identifiersList
   */
  public void setIdentifiers(String[] identifiersList) {
    this.identifierList = identifiersList;
  }

  /**
   * @param languagesList
   */
  public void setLanguages(String[] languagesList) {
    this.languageList = languagesList;
  }

  /**
   * @param publishersList
   */
  public void setPublishers(String[] publishersList) {
    this.publisherList = publishersList;
  }

  /**
   * @param rightsList
   */
  public void setRights(String[] rightsList) {
    this.rightsList = rightsList;
  }

  /**
   * @param sourcesList
   */
  public void setSources(String[] sourcesList) {
    this.sourceList = sourcesList;
  }

  /**
   * @param subjectsList
   */
  public void setSubjects(String[] subjectsList) {
    this.subjectList = subjectsList;
  }

  /**
   * @param titlesList
   */
  public void setTitles(String[] titlesList) {
    this.titleList = titlesList;
  }

  /**
   * @param typesList
   */
  public void setTypes(String[] typesList) {
    this.typeList = typesList;
  }

  /**
   * @param relationsList
   */
  public void setRelations(String[] relationsList) {
    this.relationList = relationsList;
  }

  /**
   * @param relationsForWorkList
   */
  public void setRelationsForWork(String[] relationsForWorkList) {
    this.relationsForWorkList = relationsForWorkList;
  }

}

package info.textgrid.middleware;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.classicmayan.tools.ImageMetsMods;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.MetadataType;
import info.textgrid.middleware.oaipmh.RecordType;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;

/**
 *
 */
public class IdiomImages implements RecordDelivererInterface {

  private static Logger log = Logger.getLogger(IdiomImages.class.getName());

  private String kindID;
  private String perPage;
  private String pageNumber;
  private int totalHits;
  private String idiomRbacSessionID;
  private String idiomTgcrudEndpoint;
  private String from;
  private String until;

  private static Map<String, Integer> cursorCollector = new Hashtable<String, Integer>();
  private static Map<String, String> fromUntilCollector = new Hashtable<String, String>();

  private static CrudClient tgCrudClient;

  /**
   * 
   */
  public IdiomImages() {
    //
  }

  /**
   * @param tgcrudEndpoint
   * @param rbacSessionID
   */
  public IdiomImages(String tgcrudEndpoint, String rbacSessionID) {
    this.idiomTgcrudEndpoint = tgcrudEndpoint;
    this.idiomRbacSessionID = rbacSessionID;
  }

  /**
   *
   */
  @Override
  public GetRecordType getRecordById(final String id)
      throws JSONException, IOException, ParseException {

    GetRecordType singleImageMetsMods = new GetRecordType();
    RecordType conedaKorRecord = new RecordType();
    MetadataType metadataMets = new MetadataType();

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;

    ImageMetsMods metsmodsImage = null;

    String changedID = id;
    if (!id.startsWith("textgrid:")) {
      changedID = "textgrid:" + id;
    }

    log.fine("processing id: " + changedID);
    log.fine("tgcrud endpoint: " + this.idiomTgcrudEndpoint);
    log.fine("tgcrud rbac sid: " + OaipmhUtilities.hideSID(this.idiomRbacSessionID));

    try {
      // Get TG-crud client (if not yet existing)
      // TODO If tgcrud client has got a fix endpoint, should the client better be created
      // elsewhere??
      if (tgCrudClient == null) {
        tgCrudClient = new CrudClient(this.idiomTgcrudEndpoint).enableGzipCompression();
      }
      TextGridObject tgo =
          tgCrudClient.read().setTextgridUri(changedID).setSid(this.idiomRbacSessionID).execute();

      // We want UTC dates for MODS records, too!
      String creationDateUTC = OaipmhUtilities.getUTCDateAsString(
          tgo.getMetadatada().getObject().getGeneric().getGenerated().getCreated().toString());
      String modificationDateUTC = OaipmhUtilities.getUTCDateAsString(
          tgo.getMetadatada().getObject().getGeneric().getGenerated().getLastModified().toString());

      // Get image object.
      metsmodsImage = new ImageMetsMods(tgo, creationDateUTC, modificationDateUTC);

      log.fine("creationDate: " + creationDateUTC);
      log.fine("modificationDate: " + modificationDateUTC);

      metsmodsImage.setCreationDate(creationDateUTC);
      metsmodsImage.setChangeDate(modificationDateUTC);

      log.fine("image id/title: " + metsmodsImage.getID() + " / " + metsmodsImage.getTitle());
      log.fine("metsmods: " + metsmodsImage.getXML());

      builder = factory.newDocumentBuilder();
      Document doc = builder.parse(new InputSource(new StringReader(metsmodsImage.getXML())));
      metadataMets.setAny(doc.getDocumentElement());
      conedaKorRecord.setMetadata(metadataMets);

      // Use base URI for IDIOM image delivery!
      String tgBaseURI = OaipmhUtilities.getTextGridBaseURI(changedID);
      conedaKorRecord.setHeader(buildOaipmhRecordHeader(creationDateUTC, tgBaseURI));

      singleImageMetsMods.setRecord(conedaKorRecord);

    } catch (ParserConfigurationException | SAXException | JSONException | CrudClientException e) {
      String errorMessage = "ERROR getting IDIOM METS record from TG-crud! " + e.getMessage();
      String logMessage = errorMessage;
      if (e instanceof CrudClientException) {
        logMessage += "! Maybe session ID in OAIPMH config needs to be re-newed?";
      }
      log.severe(logMessage);
      throw new IOException(errorMessage);
    }

    return singleImageMetsMods;
  }

  /**
   * @param dateStamp
   * @param Identifier
   * @return
   */
  public HeaderType buildOaipmhRecordHeader(String dateStamp, String Identifier) {

    HeaderType recordHeader = new HeaderType();
    recordHeader.setDatestamp(dateStamp);
    recordHeader.setIdentifier(Identifier);

    return recordHeader;
  }

  /**
   * @param resumptionToken
   * @param completeListSize
   * @param from
   * @param until
   * @return
   */
  public ResumptionTokenType buildResumptionToken(String resumptionToken, int completeListSize,
      String from, String until) {

    ResumptionTokenType resTokenForResponse = new ResumptionTokenType();
    if (resumptionToken != null && cursorCollector.containsKey(resumptionToken)) {
      if (cursorCollector.get(resumptionToken) * 100 > completeListSize) {
        resTokenForResponse.setValue("");
      } else {
        resTokenForResponse.setValue(resumptionToken);
      }
      resTokenForResponse.setCompleteListSize(BigInteger.valueOf(completeListSize));
      resTokenForResponse.setCursor(BigInteger.valueOf(cursorCollector.get(resumptionToken) * 100));
      cursorCollector.put(resumptionToken, cursorCollector.get(resumptionToken) + 1);
    } else {
      UUID uuid = UUID.randomUUID();
      String resTokenValue = uuid.toString();
      resTokenForResponse.setCompleteListSize(BigInteger.valueOf(completeListSize));
      resTokenForResponse.setCursor(BigInteger.valueOf(100));
      resTokenForResponse.setValue(resTokenValue);
      cursorCollector.put(resTokenValue, 2);
      fromUntilCollector.put(resTokenValue, from + ":" + until);
    }

    return resTokenForResponse;
  }

  /**
   * @param doc
   * @return
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  public String printXML(Document doc)
      throws TransformerFactoryConfigurationError, TransformerException {

    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    // initialize StreamResult with File object to save to file
    StreamResult result = new StreamResult(new StringWriter());
    DOMSource source = new DOMSource(doc);
    transformer.transform(source, result);
    String xmlString = result.getWriter().toString();

    return xmlString;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public int getTotalHits() {
    return this.totalHits;
  }

  /**
   * @param totalHits
   */
  public void setTotalHits(int totalHits) {
    this.totalHits = totalHits;
  }

  /**
   * @return
   */
  public String getPageNumber() {
    return this.pageNumber;
  }

  /**
   * @param pageNumber
   */
  public void setPageNumber(String pageNumber) {
    this.pageNumber = pageNumber;
  }

  /**
   * @return
   */
  public String getPerPage() {
    return this.perPage;
  }

  /**
   * @param perPage
   */
  public void setPerPage(String perPage) {
    this.perPage = perPage;
  }

  /**
   * @return
   */
  public String getKindID() {
    return this.kindID;
  }

  /**
   * @param kindID
   */
  public void setKindID(String kindID) {
    this.kindID = kindID;
  }

  /**
   * @return
   */
  public String getFrom() {
    return this.from;
  }

  /**
   * @param from
   */
  public void setFrom(String from) {
    this.from = from;
  }

  /**
   * @return
   */
  public String getUntil() {
    return this.until;
  }

  /**
   * @param until
   */
  public void setUntil(String until) {
    this.until = until;
  }

  /**
   * @return
   */
  public String getIdiomRbacSessionID() {
    return this.idiomRbacSessionID;
  }

  /**
   * @param idiomRbacSessionID
   */
  public void setIdiomRbacSessionID(String idiomRbacSessionID) {
    this.idiomRbacSessionID = idiomRbacSessionID;
  }

  /**
   * @return
   */
  public String getIdiomTgcrudEndpoint() {
    return this.idiomTgcrudEndpoint;
  }

  /**
   * @param idiomTgcrudEndpoint
   */
  public void setIdiomTgcrudEndpoint(String idiomTgcrudEndpoint) {
    this.idiomTgcrudEndpoint = idiomTgcrudEndpoint;
  }

  /**
   * @return
   */
  public static Map<String, Integer> getCursorCollector() {
    return cursorCollector;
  }

  /**
   * @return
   */
  public static Map<String, String> getFromUntilCollector() {
    return fromUntilCollector;
  }

}

package info.textgrid.middleware;

import info.textgrid.middleware.oaipmh.OAIPMHerrorType;
import info.textgrid.middleware.oaipmh.OAIPMHerrorcodeType;

/**
 * Handling the errors for a incorrect request
 * 
 * @author Maximilian Brodhun
 * @version 1.1
 * @since 29.01.2014
 */

public class ErrorHandler {

  private OAIPMHerrorType error = new OAIPMHerrorType();
  private OAIPMHerrorcodeType errorCode;

  /**
   * The function gets an error description and set the responded error code for the OAIPMH error
   * type
   * 
   * @param errorDescription: String with the errorDescrption
   * @return error returns the complete error as OAIPMHerrorType
   */
  public void setError(String errorDescription, String errorValue) {

    if (errorDescription.equals("BadArgument")) {
      this.errorCode = OAIPMHerrorcodeType.BAD_ARGUMENT;
    } else if (errorDescription.equals("badResumptionToken")) {
      this.errorCode = OAIPMHerrorcodeType.BAD_RESUMPTION_TOKEN;
    } else if (errorDescription.equals("VerbError")) {
      this.errorCode = OAIPMHerrorcodeType.BAD_VERB;
      this.error.setValue("Illegal OAI-PMH verb");
    } else if (errorDescription.equals("FormatError")) {
      this.errorCode = OAIPMHerrorcodeType.CANNOT_DISSEMINATE_FORMAT;
    } else if (errorDescription.equals("idDoesNotExist")) {
      this.errorCode = OAIPMHerrorcodeType.ID_DOES_NOT_EXIST;
    } else if (errorDescription.equals("MetadataFormatError")) {
      this.error.setValue("There are no metadata formats available for the specified item.");
    } else if (errorDescription.equals("RecordMatchError")) {
      this.errorCode = OAIPMHerrorcodeType.NO_RECORDS_MATCH;
    } else if (errorDescription.equals("SetHierarchyError")) {
      this.errorCode = OAIPMHerrorcodeType.NO_SET_HIERARCHY;
    } else if (errorDescription.equals("badVerb")) {
        this.errorCode = OAIPMHerrorcodeType.BAD_VERB;
      }

    this.error.setCode(this.errorCode);
    this.error.setValue(errorValue);
  }

  /**
   * @return
   */
  public OAIPMHerrorType getError() {
    return this.error;
  }

}

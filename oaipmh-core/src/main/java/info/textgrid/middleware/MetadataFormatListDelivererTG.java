package info.textgrid.middleware;

import java.io.IOException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import info.textgrid.middleware.oaipmh.ListMetadataFormatsType;
import info.textgrid.middleware.oaipmh.MetadataFormatType;

/**
 *
 */
public class MetadataFormatListDelivererTG extends MetadataFormatListDelivererAbstract {

  // **
  // FINALS
  // **

  public static final String URI = "textgridUri";

  /**
   * <p>
   * Get metadata format list for specific ID.
   * </p>
   */
  @Override
  public ListMetadataFormatsType setMetadataFormatList(String id) {

    ListMetadataFormatsType result = new ListMetadataFormatsType();

    String[] includes = new String[] {URI};
    String[] excludes = Strings.EMPTY_ARRAY;
    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

    GetRequest getRequest =
        new GetRequest(this.oaiEsClient.getEsIndex(), id.replace("textgrid:", ""))
            .fetchSourceContext(fetchSourceContext);
    GetResponse tgObject = null;
    try {
      tgObject = this.oaiEsClient.getEsClient().get(getRequest, RequestOptions.DEFAULT);

      // Check if object is existing in TG.
      if (tgObject.isExists()) {
        result = setMetadataFormatList();

        // TODO Check here, if object is existing in IDIOM?
      }
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return result;
  }

  /**
   *
   */
  @Override
  public ListMetadataFormatsType setMetadataFormatList() {

    // Get things from abstract class.
    ListMetadataFormatsType result = super.setMetadataFormatList();

    // Add metadata format for IDIOM METS.
    MetadataFormatType mftIdiomMets = new MetadataFormatType();
    mftIdiomMets.setMetadataPrefix(OaipmhConstants.METADATA_IDIOM_PREFIX);
    mftIdiomMets.setMetadataNamespace(OaipmhConstants.METS_NAMESPACE);
    mftIdiomMets.setSchema(OaipmhConstants.METS_SCHEMA_LOCATION);

    result.getMetadataFormat().add(mftIdiomMets);

    return result;
  }

}

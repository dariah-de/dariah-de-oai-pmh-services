package info.textgrid.middleware;

import info.textgrid.middleware.oaipmh.ListMetadataFormatsType;

/**
 *
 */
public class MetadataFormatListDelivererDH extends MetadataFormatListDelivererAbstract {

  /**
   *
   */
  @Override
  public ListMetadataFormatsType setMetadataFormatList(String id) {
    // Get things from abstract class, we do not have different metadata formats for different
    // identifiers! Get the two defaults here.
    return super.setMetadataFormatList();
  }

}

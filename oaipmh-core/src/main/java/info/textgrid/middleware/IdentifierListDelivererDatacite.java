package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import info.textgrid.middleware.oaipmh.ListIdentifiersType;

/**
 * <p>
 * Class to build the Element for a ListIdentifiers request.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
public class IdentifierListDelivererDatacite extends IdentifierListDelivererAbstract {

  protected static Map<String, Integer> cursorCollector = new Hashtable<String, Integer>();

  /**
   * @param datestamp taken from the elasticSearch index (created element)
   * @param identifier taken from the elasticSearch index (textgridUri)
   * @return the ListIdentifierElement with the responded header element
   */
  public IdentifierListDelivererDatacite(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   * TODO: Put in IdentifierListDelivererAbstract, due to usage in DC and Datacite class!!
   */
  @Override
  public ListIdentifiersType processIdentifierList(String from, String to, String set,
      String resumptionToken) throws IOException, ParseException {

    /*
     * To get the required values for the ListIdentifiers request this function will ask
     * ElasticSearc for a specific textgridUri the values "created" and "textgridUri".
     */

    /*
     * Since the ListIdentifiers request enables the possibility the limit the request in a specific
     * time interval, it is necessary to perform a range query on the ElasticSearch index.
     */

    ListIdentifiersType lit = new ListIdentifiersType();
    QueryBuilder query = setOrNot(set, from, to);
    SearchResponse listListIdentiferValues;

    String[] includes = this.identifierListFields;
    String[] excludes = Strings.EMPTY_ARRAY;

    // **
    // DARIAH
    // **

    if (this.dariah == true) {
      // All objects!
    }

    // **
    // TEXTGRID
    // **

    else {

      // No sandbox items.
      QueryBuilder tgFilterSandBox = QueryBuilders.matchPhraseQuery("nearlyPublished", "true");
      // All editions only!
      QueryBuilder tgEditionsOnly =
          QueryBuilders.matchPhraseQuery(this.formatField, this.formatToFilter);

      // Compose filtered query.
      query = QueryBuilders.boolQuery().must(query).must(tgEditionsOnly).mustNot(tgFilterSandBox);
    }

    SearchRequest searchRequest =
        new SearchRequest(this.oaiEsClient.getEsIndex()).searchType(SearchType.QUERY_THEN_FETCH);

    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(query);
    searchSourceBuilder.size(this.searchResponseSize);
    searchSourceBuilder.fetchSource(includes, excludes);

    if (resumptionToken != null) {
      SearchScrollRequest scrollRequest = new SearchScrollRequest(resumptionToken);
      scrollRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
      listListIdentiferValues =
          this.oaiEsClient.getEsClient().scroll(scrollRequest, RequestOptions.DEFAULT);
    } else {
      searchRequest.source(searchSourceBuilder);
      searchRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
      listListIdentiferValues =
          this.oaiEsClient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
    }

    listListIdentiferValues = hitHandling(listListIdentiferValues, lit, set,
        listListIdentiferValues.getScrollId(), cursorCollector);

    return lit;
  }

}

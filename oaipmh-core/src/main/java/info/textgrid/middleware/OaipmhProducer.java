/**
 * This software is copyright (c) 2023 by
 * 
 * Göttingen State and University Library
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright Göttingen State and University Library
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware;

import java.io.IOException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import info.textgrid.clients.tgcrud.CrudClientException;

/**
 * <p>
 * Interface to the get Request function
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. funk, SUB Göttingen
 */
public interface OaipmhProducer {

  /**
   * <p>
   * Getting the request values as QueryParam from a REST GET request and proceeds appropriate to
   * them.
   * </p>
   * 
   * @param verb - The parameter verb is a required parameter to all requests. Depending on it the
   *        request will be proceed. Possible values are: GetRecord, Identify, ListIdentifiers,
   *        ListMetadataFormats, ListRecords and ListSets In case of other verbs an error response
   *        will be send back
   * @param identifier - This value indicates if the request will be proceed just for a single
   *        identifier of the repository
   * @param metadataPrefix - To limit the response just for a special metadata format this value can
   *        be used. The prefix "oai_dc" has to be supported by all repositories
   * @param set - To represent the architecture of the repository it is possible to make sets which
   *        work like categories. At the moment textgrid does not support sets
   * @param from - Start value to filter the response for a specific interval
   * @param until - End value to filter the response for a specific interval
   * @param resumptionToken - Indicates how many value will be send back in the response
   * @return OAIPMHType object containing the whole response
   * @throws IOException
   */
  @GET
  @Path("/")
  @Produces(MediaType.TEXT_XML)
  String getRequest(@QueryParam("verb") String verb,
      @QueryParam("identifier") @DefaultValue("") String identifier,
      @QueryParam("metadataPrefix") @DefaultValue("") String metadataPrefix,
      @QueryParam("set") @DefaultValue("") String set,
      @QueryParam("from") @DefaultValue("") String from,
      @QueryParam("until") @DefaultValue("") String until,
      @QueryParam("resumptionToken") @DefaultValue("") String resumptionToken) throws IOException;

  /**
   * <p>
   * Getting the request values as FormParams from a REST POST request and proceeds appropriate to
   * them.
   * </p>
   * 
   * @param verb - The parameter verb is a required parameter to all requests. Depending on it the
   *        request will be proceed. Possible values are: GetRecord, Identify, ListIdentifiers,
   *        ListMetadataFormats, ListRecords and ListSets In case of other verbs an error response
   *        will be send back
   * @param identifier - This value indicates if the request will be proceed just for a single
   *        identifier of the repository
   * @param metadataPrefix - To limit the response just for a special metadata format this value can
   *        be used. The prefix "oai_dc" has to be supported by all repositories
   * @param set - To represent the architecture of the repository it is possible to make sets which
   *        work like categories. At the moment textgrid does not support sets
   * @param from - Start value to filter the response for a specific interval
   * @param until - End value to filter the response for a specific interval
   * @param resumptionToken - Indicates how many value will be send back in the response
   * @return OAIPMHType object containing the whole response
   * @throws IOException
   */
  @POST
  @Path("/")
  @Produces(MediaType.TEXT_XML)
  String postRequest(@FormParam("verb") String verb,
      @FormParam("identifier") @DefaultValue("") String identifier,
      @FormParam("metadataPrefix") @DefaultValue("") String metadataPrefix,
      @FormParam("set") @DefaultValue("") String set,
      @FormParam("from") @DefaultValue("") String from,
      @FormParam("until") @DefaultValue("") String until,
      @FormParam("resumptionToken") @DefaultValue("") String resumptionToken) throws IOException;

  /**
   * @return
   */
  @GET
  @Path("/version")
  @Produces(MediaType.TEXT_PLAIN)
  String getVersion();

  /**
   * @param properties The properties file location.
   * @return
   * @throws IOException
   * @throws CrudClientException
   */
  @POST
  @Path("/triggerConedaKORImageMetadataToTextGridUpdate")
  @Produces(MediaType.TEXT_PLAIN)
  Response triggerConedaKORImageMetadataToTextGridUpdate();

}

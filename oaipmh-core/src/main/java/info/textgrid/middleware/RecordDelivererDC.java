package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.common.Strings;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import info.textgrid.middleware.oaipmh.GetRecordType;
import info.textgrid.middleware.oaipmh.RecordType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
@Component
public class RecordDelivererDC extends RecordDelivererAbstract {

  private static Logger log = Logger.getLogger(RecordDelivererDC.class.getName());

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordDelivererDC(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   * @throws IOException
   */
  @Override
  public GetRecordType getRecordById(String id)
      throws ParseException, DatatypeConfigurationException, IOException {

    GetRecordType result = new GetRecordType();

    RecordType record = new RecordType();
    DublinCoreBuilder dublinCoreBuilder;

    String changedId = id;
    if (id.startsWith(this.repositoryObjectURIPrefix)) {
      // TODO Why do we need changeID? Where is it used?
      changedId = changedId.replace(this.repositoryObjectURIPrefix, "");
    }

    log.fine("id: " + id);
    log.fine("changed id: " + changedId);

    // Get ES response, get record for given ID.
    String[] includes = this.fields;
    String[] excludes = Strings.EMPTY_ARRAY;

    GetResponse esResultObject =
        OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient, id, includes, excludes);

    log.fine("es result id/size: " + (esResultObject != null
        ? esResultObject.getId() + "/" + esResultObject.getFields().size()
        : "null"));

    // Error if no ES result or result empty.
    if (esResultObject == null || !esResultObject.isExists()) {
      String message = "could not fetch result of elasticsearch for id " + id;
      throw new IOException(message);
    }

    String identifier;

    // **
    // DARIAH
    // **

    if (this.dariah == true) {
      dublinCoreBuilder = putContentIntoDCFieldListsDH(esResultObject);
      JSONObject json = new JSONObject(esResultObject.getSourceAsMap());
      identifier = OaipmhUtilities.firstEntryFieldLoader(json, this.identifierField);

      String dateOfCreation = "NO_DATE_SET!";
      if (OaipmhUtilities.firstEntryFieldLoader(json, this.dateOfObjectCreation) != null) {
        dateOfCreation = OaipmhUtilities.getUTCDateAsString(
            OaipmhUtilities.firstEntryFieldLoader(json, this.dateOfObjectCreation).toString());
      }

      String setSpec = OaipmhUtilities.firstEntryFieldLoader(json, this.specField);
      String setSpecValue =
          OaipmhUtilities.getSetSpec(setSpec, this.specFieldPrefix, identifier);

      record.setHeader(
          OaipmhUtilities.computeResponseHeader(dateOfCreation, identifier, setSpecValue));
      record.setMetadata(dublinCoreBuilder.getDC());
    }

    // **
    // TEXTGRID
    // **

    if (this.textgrid == true) {

      // Error: We have got no Edition here!
      if (!esResultObject.getSourceAsMap().get(this.formatField).equals(this.formatToFilter)) {
        String message = ERROR_NO_TG_EDITION + id;
        throw new IOException(message);
      }

      log.fine("---processing tg result---");

      String workUri = DublinCoreFieldLoader.fillListFromTGWorkValues(esResultObject,
          new String[] {this.relationToFurtherMetadataObject}).get(0);

      log.fine("work uri: " + workUri);

      workUri = workUri.substring(this.repositoryObjectURIPrefix.length());
      dublinCoreBuilder =
          putContentIntoDCFieldListsTG(esResultObject, furtherDCElements(workUri));
      identifier = esResultObject.getSourceAsMap().get(this.identifierField).toString();
      String dateOfCreation =
          esResultObject.getSourceAsMap().get(this.dateOfObjectCreation).toString();

      log.fine("date of creation: " + dateOfCreation);

      String setSpec = DublinCoreFieldLoader
          .fillListFromTGWorkValues(esResultObject, this.relationList).get(0);
      String setSpecValue =
          OaipmhUtilities.getSetSpec(setSpec, this.specFieldPrefix, identifier);

      String convertedCreationDate = OaipmhUtilities.getUTCDateAsString(dateOfCreation).toString();
      record.setHeader(
          OaipmhUtilities.computeResponseHeader(convertedCreationDate, identifier, setSpecValue));
      record.setMetadata(dublinCoreBuilder.getDC());
    }

    result.setRecord(record);

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param id
   * @return
   */
  private GetResponse furtherDCElements(String id) {

    // TODO WHY adding .0 here??
    if (!id.endsWith(".0")) {
      id = id.concat(".0");
    }

    GetResponse responseWorkValues = null;
    try {
      String[] includes = this.workFields;
      String[] excludes = Strings.EMPTY_ARRAY;

      responseWorkValues =
          OaipmhUtilities.getRecordByIDFromElasticSearch(this.oaiEsClient, id, includes, excludes);

    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return responseWorkValues;
  }

  /**
   * In case of TextGrid data from two objects are necessary. The edition object and the linked work
   * object. In result for building the DC-Object both objects from the ES-Index are needed
   * 
   * @param responseWorkValues contains the edition object
   * @param relatedWorkObject contains the work object related to edition object
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  private DublinCoreBuilder putContentIntoDCFieldListsTG(GetResponse responseWorkValues,
      GetResponse relatedWorkObject) throws ParseException, DatatypeConfigurationException {

    DublinCoreBuilder result = new DublinCoreBuilder();

    // Set DublinCore lists with content from elastic search results due to configuration.
    result.setContributor(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.contributorList));
    result.setCoverage(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.coverageList));
    result.setCreator(
        DublinCoreFieldLoader.fillListFromTGWorkValues(relatedWorkObject, this.creatorList));
    result
        .setDate(DublinCoreFieldLoader.fillListFromTGWorkValues(relatedWorkObject, this.dateList));
    result.setDescription(
        DublinCoreFieldLoader.fillListFromTGWorkValues(relatedWorkObject, this.descriptionList));
    result.setFormat(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.formatList));
    result.setIdentifier(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.identifierList));
    result.setLanguage(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.languageList));
    result.setPublisher(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.publisherList));
    result.setRelation(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.relationList));
    result.setRights(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.rightsList));
    result.setSource(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.sourceList));
    result.setSubject(
        DublinCoreFieldLoader.fillListFromTGWorkValues(relatedWorkObject, this.subjectList));
    result.setTitle(
        DublinCoreFieldLoader.fillListFromTGWorkValues(responseWorkValues, this.titleList));
    result
        .setType(DublinCoreFieldLoader.fillListFromTGWorkValues(relatedWorkObject, this.typeList));

    return result;
  }

  /**
   * This function with just one object as parameter is for DARIAH
   * 
   * @param responseWorkValues
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  private DublinCoreBuilder putContentIntoDCFieldListsDH(GetResponse responseWorkValues)
      throws ParseException, DatatypeConfigurationException {

    DublinCoreBuilder result = new DublinCoreBuilder();

    // Set DublinCore lists with content from elastic search results due to configuration.
    result.setContributor(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.contributorList));
    result.setCoverage(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.coverageList));
    result.setCreator(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.creatorList));
    result.setDate(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.dateList));
    result.setDescription(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.descriptionList));
    result.setFormat(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.formatList));
    result.setIdentifier(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.identifierList));
    result.setLanguage(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.languageList));
    result.setPublisher(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.publisherList));
    result.setRelation(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.relationList));
    result.setRights(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.rightsList));
    result.setSource(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.sourceList));
    result.setSubject(OaipmhUtilities.loopListFieldLoader(
        new JSONObject(responseWorkValues.getSourceAsMap()), this.subjectList));
    result.setTitle(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.titleList));
    result.setType(OaipmhUtilities
        .loopListFieldLoader(new JSONObject(responseWorkValues.getSourceAsMap()), this.typeList));

    return result;
  }

}

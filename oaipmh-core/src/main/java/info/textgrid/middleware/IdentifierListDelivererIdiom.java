package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONObject;
import info.textgrid.middleware.oaipmh.ListIdentifiersType;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class IdentifierListDelivererIdiom extends IdentifierListDelivererAbstract {

  // **
  // FINALS
  // **

  private static final int IDIOM_RESPONSE_SIZE = 30;

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(IdentifierListDelivererIdiom.class.getName());

  private static Map<String, Integer> cursorCollector = new Hashtable<String, Integer>();

  // **
  // CLASS
  // **

  private RecordDelivererIdiom idiomRecord;

  // Set defaults here, can be changed in oaipmh.properties.
  private String idiomProjectID = "TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318";
  private String idiomArtefactNote = "ARTEFACT";
  private String idiomMediumNote = "ConedaKorMediumData";
  private String idiomInputFormFormat = "text/tg.inputform+rdf+xml";
  private String idiomJSONFormat = "application/json";

  /**
   * @param textgrid
   * @param dariah
   */
  public IdentifierListDelivererIdiom(boolean textgrid, boolean dariah) {
    super(textgrid, dariah);
  }

  /**
   *
   */
  @Override
  public ListIdentifiersType processIdentifierList(final String from, final String to,
      final String set, final String resumptionToken) throws ParseException, IOException {

    ListIdentifiersType identifierList = new ListIdentifiersType();

    log.fine("lastModifiedField: " + OaipmhConstants.IDIOM_MODIFIED_FIELD);
    log.fine("idiomProjectID: " + this.idiomProjectID);
    log.fine("idiomArtefactNote: " + this.idiomArtefactNote);
    log.fine("idiomMediumNote: " + this.idiomMediumNote);
    log.fine("idiomInputFormFormat: " + this.idiomInputFormFormat);
    log.fine("idiomJSONFormat: " + this.idiomJSONFormat);

    // Range query.
    RangeQueryBuilder rangeQuery =
        QueryBuilders.rangeQuery(OaipmhConstants.IDIOM_MODIFIED_FIELD).from(from).to(to);
    // Project query builder.
    BoolQueryBuilder projectQuery = QueryBuilders.boolQuery().must(QueryBuilders
        .matchPhraseQuery(OaipmhConstants.IDIOM_PROJECT_ID_FIELD, this.idiomProjectID));
    // Artefact query builder.
    BoolQueryBuilder artefactQuery = QueryBuilders.boolQuery()
        .must(QueryBuilders.matchPhraseQuery(OaipmhConstants.IDIOM_FORMAT_FIELD,
            this.idiomInputFormFormat))
        .must(QueryBuilders.matchPhraseQuery(OaipmhConstants.IDIOM_NOTES_FIELD,
            this.idiomArtefactNote));
    // Medium query builder.
    BoolQueryBuilder conedakorQuery = QueryBuilders.boolQuery()
        .must(QueryBuilders.matchPhraseQuery(OaipmhConstants.IDIOM_FORMAT_FIELD,
            this.idiomJSONFormat))
        .must(QueryBuilders.matchPhraseQuery(OaipmhConstants.IDIOM_NOTES_FIELD,
            this.idiomMediumNote));
    // Combine artefact and medium query builder.
    BoolQueryBuilder both = QueryBuilders.boolQuery().should(artefactQuery).should(conedakorQuery);

    // Final query builder for classicmayan queries.
    BoolQueryBuilder recordFilterForClassicMayan =
        QueryBuilders.boolQuery().must(rangeQuery).must(projectQuery.filter(both));

    // Build ES request.
    SearchRequest searchRequest = new SearchRequest(this.oaiEsClient.getEsIndex());
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(recordFilterForClassicMayan);
    searchSourceBuilder.size(IDIOM_RESPONSE_SIZE);
    searchRequest.source(searchSourceBuilder);

    log.fine("es elient/index: " + (this.oaiEsClient == null ? "null" : this.oaiEsClient) + "/"
        + (this.oaiEsClient == null ? "null" : this.oaiEsClient.getEsIndex()));

    SearchResponse scrollResp = null;
    if (resumptionToken != null) {
      SearchScrollRequest scrollRequest = new SearchScrollRequest(resumptionToken);
      scrollRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
      try {
        scrollResp = this.oaiEsClient.getEsClient().scroll(scrollRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else {
      // Use given resumption token here.
      searchRequest.source(searchSourceBuilder);
      searchRequest.scroll(TimeValue.timeValueSeconds(this.restokLifetime));
      try {
        scrollResp = this.oaiEsClient.getEsClient().search(searchRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    String scrollID = scrollResp.getScrollId();

    long completeListSize = scrollResp.getHits().getTotalHits().value;

    log.fine("complete list size: " + completeListSize);

    if (completeListSize > 0) {
      setFoundItems(true);

      for (SearchHit hit : scrollResp.getHits().getHits()) {
        String textgridURI = OaipmhUtilities
            .firstEntryFieldLoader(new JSONObject(hit.getSourceAsMap()), this.identifierField);
        String createdDate = OaipmhUtilities.getUTCDateAsString(OaipmhUtilities
            .firstEntryFieldLoader(new JSONObject(hit.getSourceAsMap()), this.rangeField));
        identifierList.getHeader().add(OaipmhUtilities.computeResponseHeader(createdDate,
            OaipmhUtilities.getTextGridBaseURI(textgridURI), ""));
      }

      // Check the need for a resumption token!
      ResumptionTokenType responseToken = OaipmhUtilities.getResumptionToken(completeListSize,
          resumptionToken, cursorCollector, scrollID, IDIOM_RESPONSE_SIZE);
      if (responseToken != null) {
        identifierList.setResumptionToken(responseToken);
      }
    } else {
      setFoundItems(false);
    }

    return identifierList;
  }

  /**
   * @return
   */
  public static Map<String, Integer> getCursorCollector() {
    return cursorCollector;
  }

  /**
   * @return
   */
  public RecordDelivererIdiom getIdiomRecord() {
    return this.idiomRecord;
  }

  /**
   * @param idiomRecord
   */
  public void setIdiomRecord(RecordDelivererIdiom idiomRecord) {
    this.idiomRecord = idiomRecord;
  }

  /**
   * @return
   */
  public String getIdiomProjectID() {
    return this.idiomProjectID;
  }

  /**
   * @param idiomProjectID
   */
  public void setIdiomProjectID(String idiomProjectID) {
    this.idiomProjectID = idiomProjectID;
  }

  /**
   * @return
   */
  public String getIdiomArtefactNote() {
    return this.idiomArtefactNote;
  }

  /**
   * @param idiomArtefactNote
   */
  public void setIdiomArtefactNote(String idiomArtefactNote) {
    this.idiomArtefactNote = idiomArtefactNote;
  }

  /**
   * @return
   */
  public String getIdiomMediumNote() {
    return this.idiomMediumNote;
  }

  /**
   * @param idiomMediumNote
   */
  public void setIdiomMediumNote(String idiomMediumNote) {
    this.idiomMediumNote = idiomMediumNote;
  }

  /**
   * @return
   */
  public String getIdiomInputFormFormat() {
    return this.idiomInputFormFormat;
  }

  /**
   * @param idiomInputFormFormat
   */
  public void setIdiomInputFormFormat(String idiomInputFormFormat) {
    this.idiomInputFormFormat = idiomInputFormFormat;
  }

  /**
   * @return
   */
  public String getIdiomJSONFormat() {
    return this.idiomJSONFormat;
  }

  /**
   * @param idiomJSONFormat
   */
  public void setIdiomJSONFormat(String idiomJSONFormat) {
    this.idiomJSONFormat = idiomJSONFormat;
  }

}

package info.textgrid.middleware;

import info.textgrid.middleware.oaipmh.ListMetadataFormatsType;
import info.textgrid.middleware.oaipmh.RequestType;

/**
 *
 */
public interface MetadataFormatListDelivererInterface {

  /**
   * @param id
   * @return
   */
  public ListMetadataFormatsType setMetadataFormatList(String id);

  /**
   * @return
   */
  public ListMetadataFormatsType setMetadataFormatList();

  /**
   * @param request
   * @return
   */
  public boolean requestChecker(RequestType request);

}

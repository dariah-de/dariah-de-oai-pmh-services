package info.textgrid.middleware;


import java.util.ArrayList;
import java.util.List;
import info.textgrid.middleware.oaipmh.RequestType;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public abstract class RecordListDelivererAbstract implements RecordListDelivererInterface {

  protected OaipmhElasticSearchClient oaiEsClient;

  protected boolean textgrid;
  protected boolean dariah;

  protected String[] fields;
  protected String[] workFields;

  protected String dateOfObjectCreation;
  protected String relationToFurtherMetadataObject;
  protected String repositoryObjectURIPrefix;
  protected String modifiedField;
  protected String identifierField;
  protected String rangeField;
  protected String formatField;
  protected String formatToFilter;
  protected int searchResponseSize;
  protected String specField;
  protected String specFieldPrefix;
  protected String modifiedValue;
  // The scroll parameter (passed to the search request and to every scroll request) tells
  // Elasticsearch how long it should keep the search context alive. Its value (e.g. 1m, see Time
  // units) does not need to be long enough to process all data — it just needs to be long enough to
  // process the previous batch of results. Each scroll request (with the scroll parameter) sets a
  // new expiry time.
  // <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/paginate-search-results.html#scroll-search-context>
  protected int restokLifetime = 600; // Set default here, please change in config.
  protected long resultSize = 100; // Set default here, please change in config.

  private boolean foundItems;

  public ResumptionTokenType resTokenForResponse;

  /**
   * @param textgrid
   * @param dariah
   */
  public RecordListDelivererAbstract(boolean textgrid, boolean dariah) {
    this.textgrid = textgrid;
    this.dariah = dariah;
  }

  /**
   * @param request
   * @return
   */
  public static ErrorHandler requestChecker(RequestType request) {

    ErrorHandler result = new ErrorHandler();

    // Check for metadata prefix AND resumption token.
    if (request.getMetadataPrefix() != null && request.getResumptionToken() != null) {
      result.setError(OaipmhConstants.OAI_BAD_ARGUMENT,
          "The resumptionToken is an exclusive argument, please remove metadataPrefix!");
      return result;
    }

    // Check if metadata prefix is existing and valid.
    if (request.getMetadataPrefix() != null
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OAIDC_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
      result.setError(OaipmhConstants.OAI_METADATA_FORMAT_ERROR,
          "The value of the metadataPrefix " + request.getMetadataPrefix()
              + " is not supported by the item identified by the value of: "
              + request.getIdentifier());
      return result;
    }

    // Check params in general.
    List<String> errorValues = new ArrayList<String>();

    if (request.getResumptionToken() == null && request.getMetadataPrefix() == null) {
      errorValues.add("metadataPrefix");
    }
    if (request.getIdentifier() != null) {
      errorValues.add("identifier");
    }

    // Check from and util.
    if (request.getFrom() != null && !OaipmhUtilities.isParsableDate(request.getFrom())) {
      errorValues.add("from");
    }
    if (request.getUntil() != null && !OaipmhUtilities.isParsableDate(request.getUntil())) {
      errorValues.add("until");
    }

    // Set error messages.
    if (errorValues.size() > 0) {
      result.setError(OaipmhConstants.OAI_BAD_ARGUMENT, "The request includes illegal arguments "
          + "or is missing required arguments: " + errorValues);
    }

    return result;
  }

  // **
  // GETTER AND SETTER
  // **

  /**
   * @return
   */
  public String getRangeField() {
    return this.rangeField;
  }

  /**
   * @param rangeField
   */
  public void setRangeField(String rangeField) {
    this.rangeField = rangeField;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @param formatField
   */
  public void setFormatField(String formatField) {
    this.formatField = formatField;
  }

  /**
   * @return
   */
  public int getSearchResponseSize() {
    return this.searchResponseSize;
  }

  /**
   * @param searchResponseSize
   */
  public void setSearchResponseSize(int searchResponseSize) {
    this.searchResponseSize = searchResponseSize;
  }

  /**
   * @return
   */
  public long getResultSize() {
    return this.resultSize;
  }

  /**
   * @param resultSize
   */
  public void setResultSize(long resultSize) {
    this.resultSize = resultSize;
  }

  /**
   * @return
   */
  public int getRestokLifetime() {
    return this.restokLifetime;
  }

  /**
   * @param restokLifetime
   */
  public void setRestokLifetime(int restokLifetime) {
    this.restokLifetime = restokLifetime;
  }

  /**
   * @return
   */
  public boolean isFoundItems() {
    return this.foundItems;
  }

  /**
   * @param foundItems
   */
  public void setFoundItems(boolean foundItems) {
    this.foundItems = foundItems;
  }

  /**
   * @return
   */
  public OaipmhElasticSearchClient getOaiEsClient() {
    return this.oaiEsClient;
  }

  /**
   * @param oaiEsClient
   */
  public void setOaiEsClient(OaipmhElasticSearchClient oaiEsClient) {
    this.oaiEsClient = oaiEsClient;
  }

  /**
   * @return
   */
  public ResumptionTokenType getResTokenForResponse() {
    return this.resTokenForResponse;
  }

  /**
   * @param resTokenForResponse
   */
  public void setResTokenForResponse(ResumptionTokenType resTokenForResponse) {
    this.resTokenForResponse = resTokenForResponse;
  }

  /**
   * @return
   */
  public String getModifiedField() {
    return this.modifiedField;
  }

  /**
   * @param modifiedField
   */
  public void setModifiedField(String modifiedField) {
    this.modifiedField = modifiedField;
  }

  /**
   * @return
   */
  public String getDateOfObjectCreation() {
    return this.dateOfObjectCreation;
  }

  /**
   * @param dateOfObjectCreation
   */
  public void setDateOfObjectCreation(String dateOfObjectCreation) {
    this.dateOfObjectCreation = dateOfObjectCreation;
  }

  /**
   * @return
   */
  public String getRelationToFurtherMetadataObject() {
    return this.relationToFurtherMetadataObject;
  }

  /**
   * @param relationToFurtherMetadataObject
   */
  public void setRelationToFurtherMetadataObject(String relationToFurtherMetadataObject) {
    this.relationToFurtherMetadataObject = relationToFurtherMetadataObject;
  }

  /**
   * @return
   */
  public String getRepositoryObjectURIPrefix() {
    return this.repositoryObjectURIPrefix;
  }

  /**
   * @param repositoryObjectURIPrefix
   */
  public void setRepositoryObjectURIPrefix(String repositoryObjectURIPrefix) {
    this.repositoryObjectURIPrefix = repositoryObjectURIPrefix;
  }

  /**
   * @param textgrid
   */
  public void setTextgrid(boolean textgrid) {
    this.textgrid = textgrid;
  }

  /**
   * @return
   */
  public boolean isDariah() {
    return this.dariah;
  }

  /**
   * @param dariah
   */
  public void setDariah(boolean dariah) {
    this.dariah = dariah;
  }

  /**
   * @return
   */
  public String getIdentifierField() {
    return this.identifierField;
  }

  /**
   * @param identifierField
   */
  public void setIdentifierField(String identifierField) {
    this.identifierField = identifierField;
  }

  /**
   * @return
   */
  public String getModifiedValue() {
    return this.modifiedValue;
  }

  /**
   * @param modifiedValue
   */
  public void setModifiedValue(String modifiedValue) {
    this.modifiedValue = modifiedValue;
  }

  /**
   * @return
   */
  public String getFormatField() {
    return this.formatField;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @return
   */
  public String[] getWorkFields() {
    return this.workFields;
  }

  /**
   * @param workFields
   */
  public void setWorkFields(String[] workFields) {
    this.workFields = workFields;
  }

  /**
   * @param fields
   */
  public void setFields(String[] fields) {
    this.fields = fields;
  }

  /**
   * @return
   */
  public String[] getFields() {
    return this.fields;
  }

  /**
   * @return
   */
  public String getSpecField() {
    return this.specField;
  }

  /**
   * @param specField
   */
  public void setSpecField(String specField) {
    this.specField = specField;
  }

  /**
   * @return
   */
  public String getSpecFieldPrefix() {
    return this.specFieldPrefix;
  }

  /**
   * @param specFieldPrefix
   */
  public void setSpecFieldPrefix(String specFieldPrefix) {
    this.specFieldPrefix = specFieldPrefix;
  }

}

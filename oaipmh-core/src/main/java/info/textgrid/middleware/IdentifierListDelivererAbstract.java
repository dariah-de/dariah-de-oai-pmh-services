package info.textgrid.middleware;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.json.JSONObject;
import info.textgrid.middleware.oaipmh.HeaderType;
import info.textgrid.middleware.oaipmh.ListIdentifiersType;
import info.textgrid.middleware.oaipmh.RequestType;
import info.textgrid.middleware.oaipmh.ResumptionTokenType;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public abstract class IdentifierListDelivererAbstract implements IdentifierListDelivererInterface {

  // **
  // STATIC
  // **

  protected static Logger log = Logger.getLogger(IdentifierListDelivererAbstract.class.getName());

  // **
  // CLASS
  // **

  protected OaipmhElasticSearchClient oaiEsClient;

  protected boolean textgrid;
  protected boolean dariah;
  protected boolean foundItems;
  protected int searchResponseSize;
  protected ResumptionTokenType resTokenForResponse;
  protected boolean idExist = true;
  protected String[] identifierListFields; // Fields for the elasticsearch request
  protected String rangeField; // Field for the optional range query
  protected String dateOfObjectCreation; // Field for the object creation in the repository
  protected String repositoryObjectURIPrefix;
  protected String identifierField;
  protected String formatField;
  protected String formatToFilter;
  protected String specField;
  protected String specFieldPrefix;
  // The scroll parameter (passed to the search request and to every scroll request) tells
  // Elasticsearch how long it should keep the search context alive. Its value (e.g. 1m, see Time
  // units) does not need to be long enough to process all data — it just needs to be long enough to
  // process the previous batch of results. Each scroll request (with the scroll parameter) sets a
  // new expiry time.
  // <https://www.elastic.co/guide/en/elasticsearch/reference/7.17/paginate-search-results.html#scroll-search-context>
  protected int restokLifetime = 600; // Set default here, please change in config.
  protected long resultSize = 100; // Set default here, please change in config.

  /**
   * @param textgrid
   * @param dariah
   */
  public IdentifierListDelivererAbstract(boolean textgrid, boolean dariah) {
    this.textgrid = textgrid;
    this.dariah = dariah;
  }

  // **
  // PUBLIC METHODS
  // **

  /**
   * @param request
   * @return
   */
  public static ErrorHandler requestChecker(RequestType request) {

    ErrorHandler result = new ErrorHandler();

    // Check for metadata prefix AND resumption token.
    if (request.getMetadataPrefix() != null && request.getResumptionToken() != null) {
      result.setError(OaipmhConstants.OAI_BAD_ARGUMENT,
          "The resumptionToken is an exclusive argument, please remove metadataPrefix!");
      return result;
    }

    // Check if metadata prefix is existing and valid.
    if (request.getMetadataPrefix() != null
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OAIDC_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_IDIOM_PREFIX)
        && !request.getMetadataPrefix().equals(OaipmhConstants.METADATA_OPENAIRE_PREFIX)) {
      result.setError(OaipmhConstants.OAI_METADATA_FORMAT_ERROR,
          "The value of the metadataPrefix " + request.getMetadataPrefix()
              + " is not supported by the item identified by the value of: "
              + request.getIdentifier());
      return result;
    }

    // Check params in general.
    List<String> errorValues = new ArrayList<String>();

    if (request.getResumptionToken() == null && request.getMetadataPrefix() == null) {
      errorValues.add("metadataPrefix");
    }
    if (request.getIdentifier() != null) {
      errorValues.add("identifier");
    }
    if (errorValues.size() > 0) {
      result.setError(OaipmhConstants.OAI_BAD_ARGUMENT,
          "The request includes illegal arguments or is missing required arguments: "
              + errorValues);
    }

    return result;
  }

  /**
   * @param listFurtherValues
   * @param lit
   * @param set
   * @param resumptionToken
   * @param cursorCollector
   * @return
   */
  public SearchResponse hitHandling(SearchResponse listFurtherValues, ListIdentifiersType lit,
      String set, String resumptionToken, Map<String, Integer> cursorCollector) {

    long size = listFurtherValues.getHits().getTotalHits().value;
    setResultSize(size);

    for (SearchHit hit : listFurtherValues.getHits().getHits()) {
      // Handle TextGrid.
      if (this.textgrid) {
        String datestamp = hit.getSourceAsMap().get(this.dateOfObjectCreation).toString();
        datestamp = OaipmhUtilities.getUTCDateAsString(datestamp);
        String identifier = hit.getSourceAsMap().get(this.identifierField).toString();
        lit = setListIdentifierHeader(datestamp, identifier, lit, set);
      }

      // Handle DARIAH.
      else if (this.dariah) {
        JSONObject json = new JSONObject(hit.getSourceAsMap());
        String datestamp = OaipmhUtilities.firstEntryFieldLoader(json, this.dateOfObjectCreation);
        datestamp = OaipmhUtilities.getUTCDateAsString(datestamp);
        String identifier = OaipmhUtilities.firstEntryFieldLoader(json, this.identifierField);
        lit = setListIdentifierHeader(datestamp, identifier, lit, set);
      }
    }

    // Check the need for a resumption token!
    ResumptionTokenType responseToken = OaipmhUtilities.getResumptionToken(
        listFurtherValues.getHits().getTotalHits().value, resumptionToken, cursorCollector,
        listFurtherValues.getScrollId(), this.searchResponseSize);
    if (responseToken != null) {
      lit.setResumptionToken(responseToken);
    }

    return listFurtherValues;
  }

  /**
   * @param datestamp
   * @param identifier
   * @param lit
   * @param set
   * @return
   */
  private ListIdentifiersType setListIdentifierHeader(String datestamp, String identifier,
      ListIdentifiersType lit, String set) {

    HeaderType header = new HeaderType();
    header.setDatestamp(datestamp);
    header.setIdentifier(identifier);

    if (set != null) {
      String setSpecID;
      if (set.startsWith(this.specFieldPrefix)) {
        setSpecID = set;
      } else {
        setSpecID = this.specFieldPrefix + set;
      }
      header.getSetSpec().add(setSpecID);
    }
    lit.getHeader().add(header);

    return lit;
  }

  /**
   * @param set
   * @param from
   * @param until
   * @return
   */
  public QueryBuilder setOrNot(String set, String from, String until) {

    QueryBuilder query;
    QueryBuilder rangeQuery = QueryBuilders.rangeQuery(this.rangeField).from(from).to(until);
    // TODO Make format configurable in OAI-PMH config!
    QueryBuilder formatQuery = QueryBuilders.matchPhrasePrefixQuery("format", this.formatToFilter);
    QueryBuilder setQuery = null;

    if (set != null) {
      String queryField = "";
      String valueField = "";

      // **
      // DARIAH
      // **

      if (this.dariah) {
        // TODO Make format configurable in OAI-PMH config (add relationField as in
        // SetListDeliverer class?)!
        queryField = "administrativeMetadata.dcterms:relation";
        valueField = set;

        // Add record with set metadata in it (to also find root collection metadata in Repository
        // Search!)
        // TODO Put query field into config!
        if (!set.isEmpty()) {
          setQuery = QueryBuilders.matchQuery("administrativeMetadata.dcterms:identifier",
              this.specFieldPrefix + set).boost(2);

          log.fine("set query: " + setQuery.getName() + " -> " + setQuery.toString());
        }
      }

      // **
      // TEXTGRID
      // **

      else if (this.textgrid) {
        String[] setParts = set.split(":");
        if (setParts[0].equals("project")) {
          // TODO Put query field into config!
          queryField = "project.id";
          valueField = setParts[1];
        }
      }

      QueryBuilder matchQuery = QueryBuilders.matchPhraseQuery(queryField, valueField);

      if (this.dariah) {
        query = matchQuery;

        // "should" means OR here, "must" means AND! So we get: setQuery OR ( rangeQuery AND
        // matchQuery )! The order of should's do matter here!
        if (setQuery != null) {
          QueryBuilder boolQuery = QueryBuilders.boolQuery().must(rangeQuery).must(matchQuery);
          query = QueryBuilders.boolQuery().should(setQuery).should(boolQuery);

          log.fine("query: " + boolQuery.getName() + " -> " + boolQuery.toString());
        }
      } else {
        query = QueryBuilders.boolQuery().must(matchQuery).must(formatQuery).must(rangeQuery);
      }
    }

    // Set == null!
    else {
      if (this.dariah) {
        query = rangeQuery;
      } else {
        query = QueryBuilders.boolQuery().must(rangeQuery).must(formatQuery);
      }
    }

    return query;
  }

  // **
  // GETTERS & SETTERS
  // **

  /**
   * @return
   */
  public boolean isFoundItems() {
    return this.foundItems;
  }

  /**
   * @param foundItems
   */
  public void setFoundItems(boolean foundItems) {
    this.foundItems = foundItems;
  }

  /**
   * @return
   */
  public long getResultSize() {
    return this.resultSize;
  }

  /**
   * @param resultSize
   */
  public void setResultSize(long resultSize) {
    this.resultSize = resultSize;
  }

  /**
   * @return
   */
  public int getRestokLifetime() {
    return this.restokLifetime;
  }

  /**
   * @param restokLifetime
   */
  public void setRestokLifetime(int restokLifetime) {
    this.restokLifetime = restokLifetime;
  }

  /**
   * @return
   */
  public ResumptionTokenType getResTokenForResponse() {
    return this.resTokenForResponse;
  }

  /**
   * @param resTokenForResponse
   */
  public void setResTokenForResponse(ResumptionTokenType resTokenForResponse) {
    this.resTokenForResponse = resTokenForResponse;
  }

  /**
   * @return
   */
  public String getFieldForRange() {
    return this.rangeField;
  }

  /**
   * @param fieldForRange
   */
  public void setFieldForRange(String fieldForRange) {
    this.rangeField = fieldForRange;
  }


  /**
   * @param lit
   * @return
   */
  public ListIdentifiersType getLit(ListIdentifiersType lit) {
    return lit;
  }

  /**
   * @return
   */
  public String getDateOfObjectCreation() {
    return this.dateOfObjectCreation;
  }

  /**
   * @param dateOfObjectCreation
   */
  public void setDateOfObjectCreation(String dateOfObjectCreation) {
    this.dateOfObjectCreation = dateOfObjectCreation;
  }

  /**
   * @param repositoryObjectURIPrefix
   */
  public void setRepositoryObjectURIPrefix(String repositoryObjectURIPrefix) {
    this.repositoryObjectURIPrefix = repositoryObjectURIPrefix;
  }

  /**
   * @return
   */
  public String getRepositoryObjectURIPrefix() {
    return this.repositoryObjectURIPrefix;
  }

  /**
   * @param validItem
   */
  public void setIdExist(boolean validItem) {
    this.idExist = validItem;
  }

  /**
   * @return
   */
  public boolean getIdExist() {
    return this.idExist;
  }

  /**
   * @return
   */
  public String[] getIdentifierListFields() {
    return this.identifierListFields;
  }

  /**
   * @param identifierListFields
   */
  public void setIdentifierListFields(String[] identifierListFields) {
    this.identifierListFields = identifierListFields;
  }

  /**
   * @return
   */
  public String getFormatField() {
    return this.formatField;
  }

  /**
   * @param formatField
   */
  public void setFormatField(String formatField) {
    this.formatField = formatField;
  }

  /**
   * @return
   */
  public String getFormatToFilter() {
    return this.formatToFilter;
  }

  /**
   * @param formatToFilter
   */
  public void setFormatToFilter(String formatToFilter) {
    this.formatToFilter = formatToFilter;
  }

  /**
   * @return
   */
  public String getIdentifierField() {
    return this.identifierField;
  }

  /**
   * @param identifierPrefix
   */
  public void setIdentifierField(String identifierPrefix) {
    this.identifierField = identifierPrefix;
  }

  /**
   * @return
   */
  public int getSearchResponseSize() {
    return this.searchResponseSize;
  }

  /**
   * @param searchResponseSize
   */
  public void setSearchResponseSize(String searchResponseSize) {
    this.searchResponseSize = Integer.parseInt(searchResponseSize);
  }

  /**
   * @return
   */
  public String getSpecField() {
    return this.specField;
  }

  /**
   * @param specField
   */
  public void setSpecField(String specField) {
    this.specField = specField;
  }

  /**
   * @return
   */
  public String getSpecFieldPrefix() {
    return this.specFieldPrefix;
  }

  /**
   * @param specFieldPrefix
   */
  public void setSpecFieldPrefix(String specFieldPrefix) {
    this.specFieldPrefix = specFieldPrefix;
  }

  /**
   * @return
   */
  public OaipmhElasticSearchClient getOaiEsClient() {
    return this.oaiEsClient;
  }

  /**
   * @param oaiEsClient
   */
  public void setOaiEsClient(OaipmhElasticSearchClient oaiEsClient) {
    this.oaiEsClient = oaiEsClient;
  }

}

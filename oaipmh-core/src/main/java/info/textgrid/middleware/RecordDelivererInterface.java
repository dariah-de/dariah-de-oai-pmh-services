package info.textgrid.middleware;

import java.io.IOException;
import java.text.ParseException;
import javax.xml.datatype.DatatypeConfigurationException;
import info.textgrid.middleware.oaipmh.GetRecordType;

/**
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-10-21
 * @since 2019-03-07
 */

public interface RecordDelivererInterface {

  /**
   * <p>
   * Query database (whatever) for specific dataset.
   * </p>
   * 
   * @param id identifier within database
   * @return Get the GetRecord response for the request
   * @throws DatatypeConfigurationException
   * @throws ParseException
   * @throws IOException
   */
  GetRecordType getRecordById(String id)
      throws ParseException, DatatypeConfigurationException, IOException;

}

package info.textgrid.middleware;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.search.SearchHit;

/**
 * <p>
 * The Dublin Core Field Loader takes the results of the ElasticSearch request and puts them into a
 * String-List. For each element of Dublin Core exists a separate list.
 * </p>
 * 
 * <p>
 * For each element two functions are necessary because the data type "SeachHit" could contain more
 * then one results and the GetResponse just one.
 * </p>
 * 
 * @author Maximilian Brodhun, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 */
public class DublinCoreFieldLoader {

  // **
  // STATICS
  // **

  private static Logger log = Logger.getLogger(DublinCoreFieldLoader.class.getName());

  /**
   * This function takes the results of the ElasticSearch request and fills the String-Lists with
   * the specific values for the Dublin Core field
   * 
   * @param responseOfGetRequest result of the ElasticSearch request from a getRequest (Single
   *        Result)
   * @param fields String List for the specific Dublin Core field
   * @return String List containing the values for the specific Dublin Core field
   */

  /**
   * @param responseWorkValues contains results of the ElasticSearch request
   * @param fields containing all fields from ElasticSearch related to this Dublin Core field
   * @return all elements mapped from TetxGrid metadata scheme to Dublin Core
   */
  public static List<String> setDate(GetResponse responseWorkValues, String[] fields) {

    List<String> dates = new ArrayList<String>();

    // Transform to OAI-PMH certificated time stamp
    if (responseWorkValues.isExists()) {
      for (String field : fields) {
        // TODO Set values in config file!
        if (responseWorkValues.getField(field) != null) {
          // We need ISO8601 here, take entries as they come from ElasticSearch!
          dates.add(responseWorkValues.getField(field).getValue().toString());
        }
      }
    }

    return dates;
  }

  /**
   * @param hit contains results of the ElasticSearch request
   * @param fields containing all fields from ElasticSearch related to this Dublin Core field
   * @return all elements mapped from TetxGrid metadata scheme to Dublin Core
   */
  public static List<String> setDate(SearchHit hit, String[] fields) {

    List<String> dates = new ArrayList<String>();

    for (String field : fields) {
      if (hit.getFields().get(field) != null) {
        // We need ISO8601 here, take entries as they come from ElasticSearch!
        dates.add(hit.getFields().get(field).getValues().get(0).toString());
      }
    }

    return dates;
  }

  /**
   * @param responseWorkValues
   * @param fields
   * @return
   */
  @SuppressWarnings("unchecked")
  public static List<String> fillListFromTGWorkValues(GetResponse responseWorkValues,
      String[] fields) {

    List<String> list = new ArrayList<String>();

    if (responseWorkValues.isExists()) {
      if (fields != null) {
        for (String field : fields) {
          if (responseWorkValues.getSourceAsMap().get(field) == null) {
            String[] requestedField = field.split(OaipmhUtilities.ES_DIVIDER_REGEXP);
            Map<String, Object> nestedMap = responseWorkValues.getSourceAsMap();
            String valueOfRequestedField = null;
            Map<String, Object> nestedMap2 = null;
            for (int i = 0; i < requestedField.length; i++) {
              if (i < requestedField.length - 1 && nestedMap != null
                  && nestedMap.get(requestedField[i]) != null) {
                try {
                  nestedMap2 = (Map<String, Object>) nestedMap.get(requestedField[i]);
                } catch (ClassCastException e) {
                  e.printStackTrace();
                }
                if (nestedMap.get(requestedField[i]) instanceof Map<?, ?>) {
                  // TODO Comment or remove code!
                }
              }
              if (i == requestedField.length - 1 &&
                  nestedMap2 != null && nestedMap2.get(requestedField[i]) != null) {
                valueOfRequestedField = nestedMap2.get(requestedField[i]).toString();
                if (valueOfRequestedField.length() > 0) {
                  list.add(valueOfRequestedField);
                }
              }
              nestedMap = nestedMap2;
            }
          } else {
            list.add(responseWorkValues.getSourceAsMap().get(field).toString());
          }
        }
      }
    }

    return list;
  }

  /**
   * @param hit
   * @param fields
   * @return
   */
  @SuppressWarnings("unchecked")
  public static List<String> fillList(SearchHit hit, String[] fields) {

    List<String> list = new ArrayList<String>();

    if (fields != null) {
      for (String field : fields) {
        if (hit.getSourceAsMap().get(field) == null) {
          String[] requestedField = field.split(OaipmhUtilities.ES_DIVIDER_REGEXP);
          Map<String, Object> nestedMap = hit.getSourceAsMap();
          String valueOfRequestedField = null;
          Map<String, Object> nestedMap2 = null;
          for (int i = 0; i < requestedField.length; i++) {
            if (i < requestedField.length - 1 && nestedMap != null
                && nestedMap.get(requestedField[i]) != null) {
              try {
                nestedMap2 = (Map<String, Object>) nestedMap.get(requestedField[i]);
              } catch (ClassCastException e) {
                e.printStackTrace();
              }
            }
            if (i == requestedField.length - 1 &&
                nestedMap2 != null && nestedMap2.get(requestedField[i]) != null) {
              valueOfRequestedField = nestedMap2.get(requestedField[i]).toString();
              if (valueOfRequestedField.length() > 0) {
                list.add(valueOfRequestedField);
              }
            }
            nestedMap = nestedMap2;
          }
        } else {
          list.add(hit.getSourceAsMap().get(field).toString());
        }
      }
    }

    return list;
  }

}
